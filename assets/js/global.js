$(document).ready(function() {

    // Init menu
    initMenu();

    // Move spot on menu
	if ($(window).width() > 747 && $('.nav li.current .spot').is(":visible")) {
		$('.nav li').hover(function (){
			moveSpot($(this), false);
		}, function() {
			moveSpot($('.nav li.current'), false);
		});
	}

	// Move spot on other pages page
	if ($('.nav-list-heading li.current .spot').is(":visible")) {
		$('.nav-list-heading li').hover(function (){
			moveSpot($(this), false, '.nav-list-heading', 5, 10);
		}, function() {
			moveSpot($('.nav-list-heading li.current'), false, '.nav-list-heading', 5, 10);
		});

		moveSpot($('.nav-list-heading li.current'), false, '.nav-list-heading', 5, 10);
	}

	if ($(".services").length) {
		initSlick("services-doctor-list");

		$("img.youtube-video").click(function() {
			youtubeImageReplace(this);
		}); 
	}
	
	if ($('body.home').length) {
		initSlick();

		// Call it once on pageload
		setEducationImagesSize();

		// Then attach the event
		$(window).resize(function() {
			setEducationImagesSize();
		});	

		initHomeCards();

		initContactForm("home");
	}

	if ($('body.contact').length) {
		initContactForm("contact");
	}

	if($('body.cazuri').length) {
		// Init bootstrap tab
		initTabs();
		initTabNavigation();
	}

	$('[data-fancybox="gallery"]').fancybox({
		loop: true,
		buttons: ["close"],
		lang: "en",
		i18n: {
			en: {
				CLOSE: "Închide",
				NEXT: "Următor",
				PREV: "Precedent",
				ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
				PLAY_START: "Start slideshow",
				PLAY_STOP: "Pause slideshow",
				FULL_SCREEN: "Full screen",
				THUMBS: "Pictograme",
				DOWNLOAD: "Descarcă",
				SHARE: "Share",
				ZOOM: "Mărește"
			}
		}
	});

	setLanguagesOrder();

	// When clicking CTA open form
	$(document).on("click", ".js-open-form-modal", function(e) {
		e.preventDefault();
		
		initContactModal();
	});

	AOS.init({
		mirror: true,
	});
});