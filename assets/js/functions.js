function initMenu() {
    
    if ($('.nav li.current .spot').is(":visible")) {
        moveSpot($('.nav li.current'), true);
	}
	
	$('.js-open-menu').click(function() {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('.nav-mobile').stop().slideUp();
		} else {
			$(this).addClass('open');
			$('.nav-mobile').stop().slideDown();
		}
	});

	$(".js-open-submenu").on('click', function(e) {
		if (!$(this).parent().children(".submenu").is(":visible")) {
			e.preventDefault();
		}

		$(this).parent("li").addClass("is-expanded");
		$(this).parent().children(".submenu").slideDown();
	})
}

function setEducationImagesSize() {
	$(".js-open-card img").each(function(item) {
		$(this).css({
			'width' : $(this).parent().innerWidth(),
			'height': $(this).parent().innerHeight()
		});
	});
}

function setLanguagesOrder() {
    $('.language-list li:eq(0)').before($('.language-list li.current'));
}

function youtubeImageReplace(element) {
	video = '<iframe allow="autoplay=1;" width="100%" height="500" src="'+ $(element).attr('data-video') +'"></iframe>';
    $(element).replaceWith(video);
}

function moveSpot(e, init, parent=".nav", margin=-15, additional = 0) {
	
	var spot = $(parent + ' li.current .spot');

	if ($(e.parent()[0]).hasClass("submenu")){
		var spanwidth = e.width() + additional;
		var offset = e.offset().left - $(parent + ' li.current').offset().left;

		spot.stop().animate({
			left: offset + 40,
			width: 56
		}, 300);

		return false;
	} 

	var offset = e.offset().left - $(parent + ' li.current').offset().left;
	// var margin = -10;
	// var additional = 15;

	// if tablet 768 - 1024
	if ($(window).width() < 1007 && $(window).width() > 747) {
		margin = 0;
		additional = 0;
	}
	spanwidth = e.width() + additional;
	//console.log("Width: " + $(window).width() + "\n offset: " + offset + "\n margin: "  + margin + "\n additional: " + additional + "\nspanwidth: " + spanwidth);
	

	spot.stop().animate({
		left: offset - margin,
		width: spanwidth
	}, 300);
}

function initSlick(key = "home") {
	if (key == "home") {
		$('.testimonials .container').slick({
			infinite: true,

			slidesToShow: 1,
			slidesToScroll: 1,

			dots: true,

			prevArrow: '<button type="button" class="slick-prev"></button>',
			nextArrow: '<button type="button" class="slick-next"></button>'
		});

		initHomeTestimonials();
	} else if (key == "services-doctor-list") {
		$('.services .doctors-list').slick({
			infinite: true,

			slidesToShow: 1,
			slidesToScroll: 1,

			autoplay: true,
			autoplaySpeed: 2000,

			dots: false,
			arrows: false,
		});
	}
}

function initHomeCards() {
	$('.js-open-card').click(function() {
		var cardNumber = $(this).attr('data-card-number');
		
		if (window.innerWidth > 767) {
			// desktop logic
			$('.js-open-card').removeClass('open');
			$('.card-content').stop().slideUp('300', function() {
				$('.card-content[data-card-number="'+ cardNumber +'"]').stop().slideDown('300');
				$('.js-open-card[data-card-number="'+ cardNumber +'"]').addClass('open');
			});
		} else {
			// mobile logic
			$('.js-open-card').removeClass('open');

			var currentCardContent = $(".mobile-card-contents[data-card-number='"+ cardNumber +"']");

			$('.mobile-card-contents').stop().slideUp('300', function() {
				currentCardContent.stop().slideDown('300');
			});

			$(this).addClass('open')
		}

		
	});
}

function initMap() {
	var opts = {
		center: {
			lat: 45.758109,
			lng: 21.222949,
		},
		zoom: 15,
		styles: [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"saturation":"0"},{"color":"#0c8160"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}],
		maxZoom: 20,
		minZoom: 0,
		mapTypeId: 'roadmap',
	};

	
	opts.clickableIcons = true;
	opts.disableDoubleClickZoom = false;
	opts.draggable = false;
	opts.keyboardShortcuts = true;
	opts.scrollwheel = true;
	opts.mapTypeControl = false;
	// opts.streetViewControl = false;
	
	map = new google.maps.Map(document.getElementById('map'), opts);

	(function(){
		var markerOptions = {
			map: map,
			position: {
				lat: 45.758109,
				lng: 21.222949,
			}
		};
		
		markerOptions.icon = {
			path: 'M11 2c-3.9 0-7 3.1-7 7 0 5.3 7 13 7 13 0 0 7-7.7 7-13 0-3.9-3.1-7-7-7Zm0 9.5c-1.4 0-2.5-1.1-2.5-2.5 0-1.4 1.1-2.5 2.5-2.5 1.4 0 2.5 1.1 2.5 2.5 0 1.4-1.1 2.5-2.5 2.5Z',
			scale: 1.6363636363636363636363636364,
			anchor: new google.maps.Point(11, 22),
			fillOpacity: 1,
			fillColor: '#f44336',
			strokeOpacity: 0
		};
		
		var marker = new google.maps.Marker(markerOptions);

	})();
}

function initContactModal() {
	// No scroll allowed
	$("body").css("overflow", "hidden");

	$(".contact-form-ajax").fadeIn("300", function() {
		$(".form-wrapper").slideDown();
	});

	// Attach event to close modal
	$(document).stop().on("click", '.js-close-form-modal' , function() {
		$(".form-wrapper").slideUp("300", function() {
			$(".contact-form-ajax").fadeOut();
		});

		$("body").css("overflow", "initial");
	});

	// Init ajax form
	initContactForm("modal");

}

function initContactForm(location) {
	$("input, textarea").on("keydown", function() {
		$(this).siblings(".error").remove();
	});

	$("." + location + " form").off("submit").submit(function (e) {
		e.preventDefault();
		
		var fullName = (location == "modal" ? $("#full_name_modal") : $("#full_name"));
		var phone = (location == "modal" ? $("#phone_modal") : $("#phone"));
		var email = (location == "modal" ? $("#email_modal") : $("#email"));
		var message = (location == "modal" ? $("#message_modal") : $("#message"));

		$.ajax({
			url: 'ajax-contact/' + location,
			type: 'POST',
			data: {full_name: fullName.val(), phone: phone.val(), email: email.val(), message:message.val()},
			error: function() {
			   alert('Something is wrong');
			},
			success: function(data) {
				var response = JSON.parse(data);
				$(".error").remove();

				if (response.error) {
					response.error.forEach(function (value, key) {
						var errorHtml = "<div class='error'>* "+ value.message +"</div>"
						$("#" + value.key + (location == "modal" ? "_modal" : "")).after(errorHtml);
					});
				}

				if(response.success == 1) {
					alert(response.message);
					fullName.val("");
					phone.val("");
					email.val("");
					message.val("");

					if (location == "modal") {
						$(".form-wrapper").slideUp("300", function() {
							$(".contact-form-ajax").fadeOut();
						});
				
						$("body").css("overflow", "initial");
					}
				}
			}
		});
	});
}

function initHomeTestimonials() {
	var textColumns = $('.testimonial .column-text .content p');
	var maxCharCount = 900;

	var helpers = {
		trimByWord: function(string, maxLength) {
			var trimmedString = string.substr(0, maxLength);

			return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + " ...";
		}
	};

	$.each(textColumns, function(index, item) {

		var testimonialText = $(item).text().trim();

		// Cut the text if there is more than maxCharCount characters
		if (testimonialText.length > maxCharCount) {
			// Save full text in hidden div
			$(item).parent().append('<div class="d-none full-text">'+ $(item).html() +'</div>');
			
			// Display only 450 chars
			$(item).text(helpers.trimByWord(testimonialText, maxCharCount));

			// Add read more button
			$(item).parent().append('<a href="#" class="js-read-more float-right btn">Vezi mai mult <i class="fas fa-chevron-down"></i></a>')
		}
	});

	$(document).on('click', '.js-read-more', function(e) {
		e.preventDefault();
		var thisButton = $(this);

		// Replace trimmed text with full text
		var fullText = $(this).siblings('.full-text').html();
		$(this).siblings('p').html(fullText);

		thisButton.fadeOut();
		// Expand testimonial text box
		$(this).parent().parent().animate({
			height: $(this).parent().height() + 140,
		});
		
	});
}

// [Workaround]
// Custom function to programatically trigger AOS animations
function AOSReanimate(parent, speed = 300) {

	var elements = parent.find('.aos-init');

	$.each(elements, function(index, element) {
		$(element).removeClass('aos-init aos-animate');
		setTimeout(function() {
			$(element).addClass('aos-init aos-animate');
		}, speed);
	});
	
}

// [Workaround]
// Custom function to force AOS to hide elements with set animation
function AOSHide(parent) {
	elements = parent.find('.aos-init');

	$.each(elements, function (index, element) {
		$(element).removeClass('aos-animate');
	});
}

function initTabs() {
	$("a.tab-link").on('click', function(e){
		e.preventDefault();

		var thisLink = $(this);

		$([document.documentElement, document.body]).animate({
			scrollTop: $(".tab-content").offset().top
		}, 300);

		// Change classes 
		$("a.nav-link").removeClass('active');
		$(".tab-list-item").removeClass('current');

		$(this).addClass("active");
		$(this).parent().addClass('current');

		AOSHide($(".tab-pane"));

		setTimeout(function() {
			// hide() all panels, so we don't have any visible one
			$(".tab-pane").hide();
			
			AOSReanimate($(thisLink.attr("href")));
			$(thisLink.attr("href")).fadeIn(300);
		}, 100);
	});
}

function initTabNavigation() {
	let casesLength = 9;
	let cases = {
		1: "alexandra",
		2: "madalina",
		3: "olimpia",
		4: "marius",
		5: "loredana",
		6: "andreea",
		7: "erika",
		8: "dan",
		9: "iva"
	};
	let lastImage = $(".js-last-image img");
	let nextImage = $(".js-next-image img");

	$(".js-last-case").click(function(e) {
		setTimeout(() => { 
			let lastSrc = lastImage.attr("src");
			let currentLastSrc = lastSrc.slice(lastSrc.length - 5).charAt(0);
			let intendedLastSrc;
			let intendedNextSrc;
	
			// Make it infinite
			if (currentLastSrc == "1") {
				intendedLastSrc = casesLength;
				intendedNextSrc = 1;
			} else if (currentLastSrc == casesLength) {
				intendedLastSrc = parseInt(currentLastSrc) - 1;
				intendedNextSrc = 1;
			} else {
				intendedLastSrc = parseInt(currentLastSrc) - 1;
				intendedNextSrc = parseInt(currentLastSrc) + 1;
			}
			
			lastImage.attr("src", lastSrc.replace(currentLastSrc, intendedLastSrc));
			$(this).attr("href", "#" + cases[intendedLastSrc]);
	
			nextImage.attr("src", lastSrc.replace(currentLastSrc, intendedNextSrc));
			$(".js-next-case").attr("href", "#" + cases[intendedNextSrc]);
		}, 100);
	});

	$(".js-next-case").click(function(e) {
		setTimeout(() => { 
			let oldUrl = $(this).attr("href");
			let nextSrc = nextImage.attr("src");
			let currentNextSrc = nextSrc.slice(nextSrc.length - 5).charAt(0);
			let intendedNextSrc;
			let intendedLastSrc;
	
			// Make it infinite
			if (currentNextSrc == casesLength) {
				intendedNextSrc = 1;
				intendedLastSrc = casesLength;
			} else if (currentNextSrc == 1) {
				intendedNextSrc = parseInt(currentNextSrc) + 1;
				intendedLastSrc = casesLength;
			} else {
				intendedNextSrc = parseInt(currentNextSrc) + 1;
				intendedLastSrc = parseInt(currentNextSrc) - 1;
			}
			
			nextImage.attr("src", nextSrc.replace(currentNextSrc, intendedNextSrc));
			$(this).attr("href", "#" + cases[intendedNextSrc]);

			lastImage.attr("src", nextSrc.replace(currentNextSrc, intendedLastSrc));
			$(".js-last-case").attr("href", "#" + cases[intendedLastSrc]);
		}, 100);
	});
}