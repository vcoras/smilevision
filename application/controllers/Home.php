<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $languages;
    public $language;
	
    public function __construct()
    {
        parent::__construct();

        $this->languages = $this->Language_model->get_languages(1);
        $this->language  = $this->Language_model->get_current_language($this->languages, $this->uri->segment(1));

        foreach (array('site', 'form_validation') as $file)
        {
            $this->config->set_item('language', $this->language['package']);
            $this->lang->load($file, $this->language['package']);
        }
    }

    public function index() 
    {
        // TODO: Listen for POST request (fallback no javascript)
        $data['main'] = $this->load->view('home', NULL, TRUE);
        
        $data['page_title'] = "Clinică Stomatologică Timișoara | Smile Vision";
        $data['meta_data'] = [
            'keywords' => "clinica stomatologica, stomatologie, stomatologie timisoara, clinica stomatologica timisoara, clinica dentara, clinica dentara timisoara",
            'description' => "La clinica stomatologică Smile Vision îți oferim un întreg spectru de servicii profesionale. Beneficiem de propriul laborator digital și folosim cele mai noi tehnologii.",

        ];
        $this->load->view('page', $data);
    }
    
    public function ajax_contact($source = "home") 
    {
        $response = [
            "success" => 1,
            "error" => [],
            "message" => "Mesajul a fost trimis cu succes."
        ];
        
        if (empty($this->input->post("full_name"))) {
            $response["success"] = 0;
            $response["error"][] = [
                "key" => "full_name",
                "message" => "Te rugăm să introduci un nume"
            ];

            unset($response["message"]);
        }

        if (empty($this->input->post("phone"))) {
            $response["success"] = 0;
            $response["error"][] = [
                "key" => "phone",
                "message" => "Te rugăm să introduci un număr de telefon"
            ];

            unset($response["message"]);
        }

        if (empty($this->input->post("email"))) {
            $response["success"] = 0;
            $response["error"][] = [
                "key" => "email",
                "message" => "Te rugăm să introduci o adresă de e-mail"
            ];

            unset($response["message"]);
        }

        if (empty($this->input->post("message"))) {
            $response["success"] = 0;
            $response["error"][] = [
                "key" => "message",
                "message" => "Te rugăm să introduci un mesaj"
            ];

            unset($response["message"]);
        }
        
        // Send e-mail
        
        if ( function_exists( 'mail' ) && $response["success"] == 1)
        {
            $sources = [
                "home" => "Homepage",
                "contact" => "Contact",
                "modal" => "Formular modal"
            ];

            $location = isset($sources["source"]) ? $sources["source"] : "Homepage";
            $to = "vlad.coras96@gmail.com";
            $subject = "Mesaj de pe formularul de contact | ". $location. " Smile Vision";
            
            $message = $this->load->view('email/contact.php', [
                'full_name' => $this->input->post("full_name"),
                'phone' => $this->input->post("phone"),
                'email' => $this->input->post("email"),
                'message' => $this->input->post("message"),
                'location' => $location
            ], TRUE);

            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            $headers[] = 'From: Formular contact <contact@smilevision.ro>';
            
            
            mail($to, $subject, $message, implode("\r\n", $headers));
        }

        echo json_encode($response);
    }
}