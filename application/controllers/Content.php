<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Content extends CI_Controller
{
    public $languages;
    public $language;

    public function __construct()
    {
        parent::__construct();

        $this->languages = $this->Language_model->get_languages(1);
        $this->language = $this->Language_model->get_current_language($this->languages, $this->uri->segment(1));

        foreach (['site', 'form_validation'] as $file) {
            $this->config->set_item('language', $this->language['package']);
            $this->lang->load($file, $this->language['package']);
        }
    }

    public function view($link_rewrite = null)
    {
        $data['main'] = $this->load->view('content/' . $link_rewrite, null, true);
        
        $titles = [
            'clinica' => 'Despre Noi | Clinica Stomatologică Smile Vision',
            'cazuri' => 'Cazuri Stomatologice | Clinica Stomatologică Smile Vision',
            'stomatologie-digitala' => 'Stomatologie digitală în Timișoara | Smile Vision',
            'laborator' => 'Laborator de tehnică dentară în Timișoara | Smile Vision',
            'radiologie' => 'Radiologie dentară digitală în Timișoara | Smile Vision',
            'preturi' => 'Prețuri Servicii Stomatologice | Smile Vision Timișoara',
        ];

        $meta_data = [
            'clinica' => 'Pasiunea pentru detaliu ne definește! La clinica stomatologică Smile Vision beneficiezi de servicii personalizate pe baza nevoilor tale. Află mai multe despre noi!',
            'cazuri' => 'Pasiunea pentru detaliu ne definește! Citește mai multe despre cazurile stomatologice tratate în clinica Smile Vision și află părerea clienților noștri.',
            'stomatologie-digitala' => 'La clinica Smile Vision toate procesele au devenit digitale, reunite în cadrul conceptului denumit Stomatologie Digitală.',
            'laborator' => 'Laboratorul de tehnică dentară Smile Vision din Timișoara oferă servicii complete și are o echipă de profesioniști bine pregătiți, cu vastă experiență.',
            'radiologie' => 'Radiologie digitală de înaltă performanță. Aparate de ultima generație, cu doze mici de radiații și rezultate net superioare filmului radiologic clasic.',
            'preturi' => 'Ne pasă de sănătatea dinților tăi. De aceea, îți oferim o gamă completă de servicii stomatologice. Consultă lista noastră de prețuri și programează-te acum!',
        ];

        $data['page_title'] = $titles[$link_rewrite];
        $data['meta_data'] = [
            'keywords' => "clinica stomatologica, stomatologie, stomatologie timisoara, clinica stomatologica timisoara, clinica dentara, clinica dentara timisoara",
            'description' => $meta_data[$link_rewrite],
        ];
        $this->load->view('page', $data);
    }

    public function gdpr()
    {
        $data['main'] = $this->load->view('content/gdpr', null, true);
        
        $data['meta_data'] = [
            'keywords' => "clinica stomatologica, stomatologie, stomatologie timisoara, clinica stomatologica timisoara, clinica dentara, clinica dentara timisoara",
            'description' => "Politica de confidențialitate și respectarea regulamentelor europene (GDPR)",
        ];
        $this->load->view('page', $data);
    }
}
