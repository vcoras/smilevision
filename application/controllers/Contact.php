<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public $languages;
    public $language;
	
    public function __construct()
    {
        parent::__construct();

        $this->languages = $this->Language_model->get_languages(1);
        $this->language  = $this->Language_model->get_current_language($this->languages, $this->uri->segment(1));

        foreach (array('site', 'form_validation') as $file)
        {
            $this->config->set_item('language', $this->language['package']);
            $this->lang->load($file, $this->language['package']);
        }
    }

    public function index() 
    {  
        $data['main'] = $this->load->view('contact', NULL, TRUE);
        
        $data['page_title'] = "Contact | Clinica Stomatologică Smile Vision";
        $data['meta_data'] = [
            'keywords' => "contact clinica stomatologica, contact stomatologie, contact stomatologie timisoara, contact clinica stomatologica timisoara, contact clinica dentara, clinica dentara timisoara",
            'description' => "Când vine vorba de sănătatea dinților tăi, suntem aici pentru tine. Contactează-ne acum! Fă primul pas către un zâmbet strălucitor și plin de viață! ",
        ];
        $this->load->view('page', $data);
    }
}