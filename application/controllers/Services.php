<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Services extends CI_Controller
{
    public $languages;
    public $language;

    public function __construct()
    {
        parent::__construct();

        $this->languages = $this->Language_model->get_languages(1);
        $this->language = $this->Language_model->get_current_language($this->languages, $this->uri->segment(1));

        foreach (['site', 'form_validation'] as $file) {
            $this->config->set_item('language', $this->language['package']);
            $this->lang->load($file, $this->language['package']);
        }
    }

    public function view($link_rewrite = null)
    {
        if ($link_rewrite === null) {
            show_404();
        }

        $template_pages = [
            'chirurgie', 'fatete-dentare', 'endodontie', 'estetica-dentara',
            'implantologie', 'odontologie', 'ortodontie', 'parodontologie',
            'pedodontie', 'profilaxie', 'protetica', 'radiologie'
        ];

        $rest_of_pages = ['landing', 'indicatii'];

        if (in_array($link_rewrite, $template_pages)) {
            $data['body_class'] = 's-template';
        } elseif (!in_array($link_rewrite, $rest_of_pages)) {
            show_404();
        }

        $data['main'] = $this->load->view('services/' . $link_rewrite, null, true);

        $titles = [
            'chirurgie' => 'Chirurgie Dentară: Intervenții și prețuri | Smile Vision',
            'fatete-dentare' =>'Fațete dentare în câteva ore | Smile Vision Timișoara',
            'endodontie' => 'Endodonție: Tratament obturație de canal | Smile Vision',
            'estetica-dentara' =>'Albire Dentară: Preț și etape | Estetică Dentară | Smile Vision',
            'implantologie' => 'Implant Dentar: Prețuri, tipuri și etape | Smile Vision',
            'odontologie' => 'Odontologie: Tratarea cariilor și leziunilor dinților | Smile Vision',
            'ortodontie' => 'Ortodonție: Aparate dentare fixe, mobile sau de contenție | Smile Vision',
            'parodontologie' => 'Tratament Parodontoză | Parodontologie | Smile Vision',
            'pedodontie' => 'Pedodonție: Stomatologie pentru copii | Smile Vision',
            'profilaxie' => 'Profilaxie Dentară: Detartraj, fluorizare și igienizare | Smile Vision',
            'protetica' => 'Protetică: Încrustații, punți și coroane dentare | Smile Vision',
            'radiologie' => 'Radiografie și Imagistică Dentară | Smile Vision',
            'indicatii' => 'Indicații Tratamente Dentare | Smile Vision',
            'landing' => 'Servicii Stomatologice | Smile Vision Timișoara',
        ];

        $meta_data = [
            'chirurgie' => 'Clinica stomatologică Smile Vision îți pune la dispoziție servicii complete de chirurgie dentară. Ne pasă de sănătatea zâmbetului tău!',
            'fatete-dentare' => 'Fațetele dentare sunt soluția ideală pentru un zâmbet de invidiat! Deoarece avem propriul laborator dentar, îți putem oferi fațetele în câteva ore.',
            'endodontie' => 'Apelează la serviciile de endodonție oferite de clinica stomatologică Smile Vision și beneficiază de un tratament de canal de succes. Programează-te acum!',
            'estetica-dentara' => 'Ai nevoie de o albire dentară sau de o remodelare a zâmbetului? Clinica stomatologică Smile Vision îți oferă servicii profesionale de estetică dentară.',
            'implantologie' => 'Clinica stomatologică Smile Vision îți pune la dispoziție servicii complete de implantologie dentară. Descoperă acum soluțiile noastre de implant dentar!',
            'odontologie' => 'Clinica stomatologică Smile Vision îți pune la dispoziție servicii profesionale de odontologie. Folosim materiale de reconstituire de ultimă generație.',
            'ortodontie' => 'Clinica stomatologică Smile Vision oferă servicii profesionale de ortodonție. Programează-te acum, iar noi îți vom recomanda un aparat dentar potrivit nevoilor tale.',
            'parodontologie' => 'Clinica Stomatologică Smile Vision îți oferă servicii de parodontologie profesionale. Programează-te acum pentru un tratament specializat împotriva parodontozei.',
            'pedodontie' => 'Clinica Smile Vision îți pune la dispoziție servicii de pedodonție și stomatologie pediatrică. Ai grijă de sănătatea orală a copilului tău. Programează-te acum!',
            'profilaxie' => 'Profilaxia dentară previne imbolnavirea dinților. Smile Vision îți oferă servicii de îndepărtare a plăcii bacteriene, fluorizare, detartraj și igienizare.',
            'protetica' => 'Ai nevoie de inlocuirea unui dinte lipsă? La Smile Vision folosim aparatură de ultimă generație pentru a-ți oferi servicii profesionale de protetică dentară.',
            'radiologie' => 'Prin intermediul radiografiei dentare putem evalua sănătatea dentară și identifica tumorile cavității bucale. Află mai multe despre procedurile utilizate de noi!',
            'indicatii' => 'Tu știi ce trebuie să faci înainte și după efectuarea unui tratament dentar? Ți-am pus la dispoziție o listă cu indicații pentru cele mai populare intervenții stomatologice.',
            'landing' => 'Descoperă serviciile profesionale de stomatologie oferite de clinica Smile Vision. Fă primul pas către un zâmbet strălucitor și programează-te acum!',
        ];


        $data['page_title'] = $titles[$link_rewrite];
        $data['meta_data'] = [
            'keywords' => "clinica stomatologica, stomatologie, stomatologie timisoara, clinica stomatologica timisoara, clinica dentara, clinica dentara timisoara",
            'description' => $meta_data[$link_rewrite],
        ];
        $this->load->view('page', $data);
    }
}
