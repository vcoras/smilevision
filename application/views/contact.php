<section class="section-1">
    <div data-aos="fade-down" data-aos-anchor-placement="top-bottom" id="map"></div>
</section>
<section class="section-2">
    <div data-aos="fade-down" class="container container-960">
        <div class="row">
            <h1>Contact</h1>
        </div>
        <div class="row no-gutter">
            <div class="col-md-6">
                <div class="row no-gutter phone">
                    <h2 class="col-md-12">Telefon</h2>
                    <p class="col-md-12 text"><a href="tel:0724005832">0724 005 832</a></p>
                </div>
                <div class="row no-gutter email">
                    <h2 class="col-md-12">E-mail</h2>
                    <p class="col-md-12 text"><?php echo safe_mailto('office@smilevision.ro'); ?></p>
                </div>
                <div class="row no-gutter address">
                    <h2 class="col-md-12">Adresă</h2>
                    <p class="col-md-12 text">Str. Gheorghe Lazăr nr. 9, corp H, et. 2,<br>Timișoara</p>
                </div>
            </div>
            <div class="col-md-6">
                <form action="<?php echo current_url(); ?>" method="post">
                    <div data-aos-delay="0" data-aos="fade-right" class="form-group">
                        <label for="full_name">Nume</label>
                        <input type="text" class="form-control" id="full_name" name="full_name">
                    </div>
                    <div data-aos-delay="100" data-aos="fade-right" class="form-group">
                        <label for="phone">Telefon</label>
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>

                    <div data-aos-delay="200" data-aos="fade-right" class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>

                    <div data-aos-delay="300" data-aos="fade-right" class="form-group">
                        <label for="message">Mesaj</label>
                        <textarea class="form-control" id="message" name="message"></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" id="submit" name="submit" value="Trimite">Trimite</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>