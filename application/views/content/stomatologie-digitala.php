<section class="section-1">
    <img src="<?php echo media_url("stomatologie-digitala/header.jpg"); ?>" alt="Heading image">
    <div class="text-wrapper">
        <h1 data-aos="fade-right" data-aos="fade-right">
            Stomatologie <br> 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Digitală</h1>
        <p data-aos="fade-left" data-aos="fade-left">Ce este și care sunt avantajele?</p>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <h2 data-aos="fade-up">Ce este stomatologia digitală?</h2>
        <p data-aos="fade-up">Stomatologia Digitală se referă la utilizarea tehnologiilor sau dispozitivelor dentare care încorporează componente digitale sau controlate de calculator pentru a efectua și a ne ajuta în timpul tratamentelor stomatologice, permițând tratamente noi și revoluționare.</p>
        <div data-aos="fade-up" class="row video">
            <video controls poster="<?php echo media_url("stomatologie-digitala/video.png"); ?>">
                <source src="<?php echo media_url("stomatologie-digitala/stomatologie-digitala-". $this->language["url_key"] .".mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
        <br>
        <h2 data-aos="fade-up">Avantajele tehnologiei digitale în stomatologie</h2>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Tratament eficient și precis cu un nivel de predictibilitate mult mai ridicat al rezultatului final</li>
            <li data-aos-delay="100" data-aos="fade-left">Zâmbet perfect integrat în fizionomia feței, configurat pe calculator</li>
            <li data-aos-delay="200" data-aos="fade-left">Radiografii cu un nivel mult mai scăzut de radiații</li>
            <li data-aos-delay="300" data-aos="fade-left">Reconstrucții dentare realizate în timp record, precise, care se integrează natural</li>
        </ul>
    </div>
</section>
<div class="title-ribbon" data-aos-delay="0" data-aos="zoom-in">
    <div class="container">
        <h2 data-aos-delay="200" data-aos="fade-right">Tehnologii în stomatologia digitală</h2>
    </div>
</div>
<section class="section-3">
    <div class="container">
        <h2 data-aos="fade-up">Scanner-ul intraoral</h2>
        <p data-aos="fade-right"class="label">RAPIDITATE, PRECIZIE și CONFORT</p>
        <p  data-aos="fade-up">Trecerea de la stomatologia analogică la cea digitală înseamnă că sărim amprentarea convențională și toate tratamentele să le începem cu o scanare intraorală.</p>
        <p  data-aos="fade-up">Realizând 3.000 de fotografii tridimensionale pe secundă, folosirea unui scaner intraoral realizează o reprezentare 3D a întregii cavități orale.</p>
        <p  data-aos="fade-up">Amprenta optică elimină din start micile modificări ce pot apărea în realizarea amprentei clasice a arcadelor dentare (date de factorul de contracție al materialelor folosite în realizarea amprentei clasice) obținându-se astfel clona digitală fidelă în proportie de 100%.</p>
        <br>
        <p data-aos="fade-up"><strong>Când se folosește scannerul intraoral?</strong></p>
        <p data-aos="fade-up">Fiecare tratament simplu sau complex poate începe cu o scanare digitală 3D intraorală a pacientului. </p>
        <p data-aos="fade-up">Reprezentarea digitală 3D a cavității orale este prezentată pacienților, oferindu-le posibilitatea de a înțelege exact ce se întâmplă cu dantura lor, făcând posibil să urmărim cu ușurință evoluția stării dentare și să monitorizăm progresul tratamentului.</p>
        <p data-aos="fade-up">În cazul restaurărilor protetice cu un singur click, se va trimite reprezentarea digitală către laboratorul dentar unde cu ajutorul tehnologiilor CAD/CAM se va realiza o lucrare protetică cu un grad de estetică superior, precis, în termen de câteva ore.</p>
        <br>
        <p data-aos="fade-up"><strong>Beneficiile scanării intraorale</strong></p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Precizie extrem de ridicată cu riscul erorilor minim</li>
            <li data-aos-delay="100" data-aos="fade-left">Determinarea exactă a culorii dinților folosind 3ShapeTrios</li>
            <li data-aos-delay="200" data-aos="fade-left">Confortul pacientului în timpul procesului de amprentare este net superior față de amprentarea clasică</li>
            <li data-aos-delay="300" data-aos="fade-left">Număr de ședințe și timp de lucru redus</li>
            <li data-aos-delay="400" data-aos="fade-left">Reproductibilitatea lucrărilor protetice (fără o altă amprentă)</li>
        </ul>
        <br>
        <div class="row photos">
            <div class="col-md-5 p-0">
                <img data-aos="fade-right" src="<?php echo media_url("stomatologie-digitala/scanare-1.jpg"); ?>" alt="3shape" />
            </div>
            <div class="col-md-7 p-0">
                <img data-aos="fade-left" src="<?php echo media_url("stomatologie-digitala/scanare-2.jpg"); ?>" alt="Stomatologie digitala" />
            </div>
        </div>
        <p data-aos="fade-up" class="extra-text"><strong>3Shape Trios</strong> este cel mai performant scanner intraoral din lume, care ne permite efectuarea amprentelor digitale de înaltă precizie într-un timp foarte scurt.</p>
    </div>
</section>
<section class="section-4">
    <div class="container">
        <h2 data-aos="fade-up">Tratamente asistate de microscop</h2>
        <p data-aos="fade-right" class="label">RAPID, FĂRĂ RISCURI, 100% PRECIS</p>
        <p data-aos="fade-up">Majoritatea procedurilor stomatologice se efectuează în locuri întunecate și restrânse, iar fracțiunile de milimetri pot decide rezultatul tratamentului.</p>
        <p data-aos="fade-up">Microscopul dentar a devenit indispensabil atât pentru diagnostic cât și pentru terapie în toate procedurile dentare, îndeosebi în <a href="<?php echo base_url($this->language['url_key'] . '/servicii/endodontie'); ?>">endodontie (tratamentul de canal)</a>.</p>
        <br>
        <p data-aos="fade-up"><strong>Ce este un tratament endodontic asistat microscopic? </strong></p>
        <p data-aos="fade-up">Un tratament endodontic asistat microscopic este un tratament al canalului radicular al dintelui folosind microscopul endodontic.</p>
        <p data-aos="fade-up">Tratamentul radicular este necesar atunci când: dintele este infiltrat de leziuni carioase mari, în cazul infecțiilor dentare, în scop protetic etc.</p>
        <p data-aos="fade-up">Datorită performanțelor optice remarcabile și a iluminării coaxiale, medicul are posibilitatea de a vizualiza canale radiculare suplimentare, variații anatomice, calcificări ale canalelor, instrumente fracturate care nu pot fi observate cu ochiul liber în interiorul rădăcinii dintelui.</p>
        <br>
        <p data-aos="fade-up"><strong>Beneficiile tratamentelor asistate microscopic</strong></p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Salvarea dinților care au nevoie de tratamente endodontice complexe</li>
            <li data-aos-delay="100" data-aos="fade-left">Detectarea precoce a cariilor în forma incipientă și a fisurilor dentare</li>
            <li data-aos-delay="200" data-aos="fade-left">Posibilitatea de a trata cele mai dificile cazuri endodontice</li>
            <li data-aos-delay="300" data-aos="fade-left">Tratamente minim invazive ,imposibil de realizat cu tehnicile clasice</li>
            <li data-aos-delay="400" data-aos="fade-left">Prognostic mai bun al tratamentelor efectuate pe termen lung datorită gradului ridicat de precizie</li>
            <li data-aos-delay="500" data-aos="fade-left">Vizionarea “live” a tratamentului efectuat de către medic</li>
        </ul>
        <br>
        <div class="row photos">
            <div class="col-md-9 p-0">
                <img data-aos="fade-right" src="<?php echo media_url("stomatologie-digitala/microscop-1.jpg"); ?>" alt="Microscop 1">
            </div>
            <div class="col-md-3 p-0">
                <img data-aos="fade-left" src="<?php echo media_url("stomatologie-digitala/microscop-2.jpg"); ?>" alt="Microscop 2">
            </div>
        </div>
        <p class="extra-text"><strong>Microscopul dentar Leica</strong> mărește de până la 40 de ori și luminează zona care va fi tratată, vizualizând orice complicație, oferind pacientului un tratament fără riscuri și o intervenție 100% precisă, imposibil de realizat cu tehnicile clasice.</p>
    </div>
</section>
<section class="section-5">
    <div class="container">
        <h2 data-aos="fade-up">Radiologie digitală</h2>
        <p data-aos="fade-right" class="label">RADIAȚII MINIME, SCANARE COMPLETĂ, CLARITATE</p>
        <p data-aos="fade-up"><a href="<?php echo base_url($this->language['url_key'] . '/radiologie-dentara-digitala'); ?>">Radiografiile dentare</a> sunt imagini ale dinților pe care le folosim pentru a evalua sănătatea orală a pacienților și pentru a putea stabili un diagnostic corect și un plan de tratament complet. În funcție de scopul pentru care sunt efectuate, există mai multe feluri de radiografii dentare.</p>
        <br>
        <p data-aos="fade-up"><strong>Ce este radiologia digitală?</strong></p>
        <p data-aos="fade-up">Spre deosebire de imagistica convențională, imagistica digitală înseamnă nivel de radiații mai scăzut pentru pacienți și reducerea timpului de expunere a pacientului.</p>
        <br>
        <p data-aos="fade-up"><strong>Beneficiile radiologiei digitale</strong></p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Metodă de diagnosticare a afecțiunilor dentare</li>
            <li data-aos-delay="100" data-aos="fade-left">Verificarea tratamentelor în timp real</li>
            <li data-aos-delay="200" data-aos="fade-left">Doze de radiații minime</li>
            <li data-aos-delay="300" data-aos="fade-left">Obținerea imediată a imaginilor radiologice</li>
            <li data-aos-delay="400" data-aos="fade-left">Precizie în obținerea diagnosticelor (mai ales la 3D)</li>
            <li data-aos-delay="500" data-aos="fade-left">Colaborarea cu pacientul în stabilirea planului de tratament</li>
        </ul>
        <br>
        <div class="row photos">
            <div class="col-md-5 p-0">
                <img data-aos="fade-right" src="<?php echo media_url("stomatologie-digitala/radiologie-1.jpg"); ?>" alt="Radiologie 1">
            </div>
            <div class="col-md-7 p-0">
                <img data-aos="fade-left" src="<?php echo media_url("stomatologie-digitala/radiologie-2.jpg"); ?>" alt="Radiologie 2">
            </div>
        </div>
        <p data-aos="fade-up" class="extra-text"><strong>Planmeca ProX</strong> și <strong>Satelec</strong>, aparatele noastre pentru radiografiile retro-alveolare ne oferă o poziționare ușoară și precisă, un proces imagistic simplu și imagini de înaltă calitate în rezoluție înaltă.</p>
        <p data-aos="fade-up" class="extra-text">Radiologie digitală panoramică <strong>Morita Veraviewepocs 3D R100</strong> ne permite scanarea completă a maxilarului și / sau a mandibulei obținând o calitate strălucitoare a imaginii cu reducerea eficientă a dozei de radiație.</p>
    </div>
</section>
<section class="section-6">
    <div class="container">
        <h2 data-aos="fade-up">Chirurgie ghidată digital în implantologie</h2>
        <p data-aos="fade-right" class="label">TEHNLOGIE, EXPERIENȚĂ, ESTETICĂ</p>
        <p data-aos="fade-up">În implantologia modernă, cele mai frecvente complicații apar datorită poziției greșite sau imperfecte a implanturilor. Inserarea lor, care în multe situații trebuie să fie perfectă (marjă de eroare de sub 1 mm de poziție și sub 3 grade în angulație) este foarte greu de obținut intraoperator de medicul implantolog.</p>
        <br>
        <p data-aos="fade-up"><strong>Ce este chirurgia ghidată?</strong></p>
        <p data-aos="fade-up">Chirurgia ghidată este întâlnită atunci când stomatologul folosește echipamente avansate și tehnologie imagistică pentru a „ghida” inserarea <a href="<?php  echo base_url($this->language['url_key'] . '/servicii/implantologie');?>">implanturilor dentare</a> mai eficient și mai precis, reducând timpul unei intervenții și oferind pacientului o experiență mai ușoară și mai plăcută pe durata intervenției.</p>
        <br>
        <p data-aos="fade-up"><strong>Beneficiile tehnologiei digitale în chirugia stomatologică</strong></p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Rezultatele tratamentului apropiate cu cele din simulare</li>
            <li data-aos-delay="100" data-aos="fade-left">Grad ridicat de precizie a inserării implanturilor în cazuri limită</li>
            <li data-aos-delay="200" data-aos="fade-left">Posibilitatea realizării coroanelor provizorii pe implanturi imediat</li>
            <li data-aos-delay="300" data-aos="fade-left">Estetică îmbunătățită a rezultatului final</li>
            <li data-aos-delay="400" data-aos="fade-left">Prognostic bun pe termen lung</li>
            <li data-aos-delay="500" data-aos="fade-left">Timp redus al intervenției chirurgicale</li>
            <li data-aos-delay="600" data-aos="fade-left">Recuperare rapidă a pacientului</li>
        </ul>
        <br>
        <div class="row photos">
            <div class="col-md-4 p-0">
                <img data-aos="fade-right" src="<?php echo media_url("stomatologie-digitala/chirurgie-1.jpg"); ?>" alt="chirurgie 1">
            </div>
            <div class="col-md-8 p-0">
                <img data-aos="fade-left" src="<?php echo media_url("stomatologie-digitala/chirurgie-2.jpg"); ?>" alt="chirurgie 2">
            </div>
        </div>
        <p data-aos="fade-up" class="extra-text">Tehnologia folosită în chirurgia ghidată include: imagini radiologice 3D (CBCT), scanări intraorale, software CAD, mașini de frezat CAM și imprimanta 3D.</p>
        <p data-aos="fade-up" class="extra-text">Un <strong>“digital workflow”</strong> complet - de la scanarea intraorală până la plasarea implantului cu ajutorul șablonului ghidat și, în final, frezarea imediată a restaurării protetice.</p>
    </div>
</section>
<section class="section-7">
    <div class="container">
        <h2 data-aos="fade-up">Laborator dentar digital</h2>
        <p data-aos="fade-right" class="label">TEHNOLOGIE CAD/CAM, IMPRIMARE 3D</p>
        <p data-aos="fade-up">Schimbările tehnologice revoluționează cu adevărat modul în care se practică stomatologia și modul în care laboratoarele dentare fabrică restaurări protetice.</p>
        <p data-aos="fade-up">Apariția CAD / CAM a permis medicilor stomatologi și laboratoarelor să valorifice puterea computerelor pentru a proiecta și fabrica restaurări protetice, estetice și durabile.</p>
        <div class="row no-gutter">
            <div data-aos="fade-up" class="col-md-12">
                <img src="<?php echo media_url("stomatologie-digitala/laborator-1.jpg"); ?>" alt="Laborator 1">
            </div>
        </div>
        <br><br>
        <p data-aos="fade-up"><strong>Lucrări protetice realizate în laboratorul dentar digital</strong></p>
        <div class="row photos photos-4">
            <div class="col-md-3 p-0">
                <img data-aos-delay="0" data-aos="fade-down" src="<?php echo media_url("stomatologie-digitala/laborator-2.jpg"); ?>" alt="Laborator 2">
            </div>
            <div class="col-md-3 p-0">
                <img data-aos-delay="100" data-aos="fade-down" src="<?php echo media_url("stomatologie-digitala/laborator-3.jpg"); ?>" alt="Laborator 2">
            </div>
            <div class="col-md-3 p-0">
                <img data-aos-delay="200" data-aos="fade-down" src="<?php echo media_url("stomatologie-digitala/laborator-4.jpg"); ?>" alt="Laborator 3">
            </div>
            <div class="col-md-3 p-0">
                <img data-aos-delay="300" data-aos="fade-down" src="<?php echo media_url("stomatologie-digitala/laborator-5.jpg"); ?>" alt="Laborator 4">
            </div>
        </div>
        <br>
        <p data-aos="fade-up">Cu ajutorul scanării digitale, amprenta optică a  danturii tale ajunge instant în laboratorul de tehnică dentară, unde cu ajutorul mijloacelor de proiectare CAD-CAM, a unităților de frezare și a imprimantei 3D se obțin: coroane dentare, <a href="<?php echo base_url($this->language['url_key'] . "/servicii/fatete-dentare"); ?>">fațete</a>, ghiduri chirurgicale, wax-up, modele de studiu, gutiere de albire, gutiere de bruxism, inlay, onlay în termen de câteva ore.</p>
        <br>
        <p data-aos="fade-up"><strong>Beneficiile utilizării propriului laboratorului digital</strong></p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Modificările protetice necesare (culoare, nuanță, formă) sunt imediat corectate cât timp pacientul este în scaunul stomatologic</li>
            <li data-aos-delay="100" data-aos="fade-left">Eficiență și timp: lucrări protetice direct in clinică</li>
            <li data-aos-delay="200" data-aos="fade-left">Confort și flexibilitate pentru pacienți</li>
        </ul>
        <br>
        <p data-aos="fade-up" class="extra-text">Tehnica digitală din cadrul unui laborator dentar grăbește proiectarea și producerea restaurărilor, combinând scanarea 3D, proiectarea CAD și frezarea cu cameră sau imprimarea 3D.</p>
    </div>
</section>
<section class="section-8">
    <div class="container">
        <h2 data-aos="fade-up">Fotografia în stomatologie</h2>
        <p data-aos="fade-right" class="label">TEHNLOGIE, EXPERIENȚĂ, ESTETICĂ</p>
        <p data-aos="fade-up">Fotografia (imaginea), reprezintă unul din cele mai importante mijloace de comunicare.</p>
        <br>
        <p data-aos="fade-up"><strong>Ce este fotografia dentară?</strong></p>
        <p data-aos="fade-up">Fotografia dentară este cel mai la îndemână instrument care permite pacientului să-și vizualizeze zâmbetul și starea orală, și să înțeleagă rațiunea tratamentului recomandat.</p>
        <br>
        <div class="row no-gutter">
            <div data-aos="fade-up" class="col-md-12">
                <img src="<?php echo media_url("stomatologie-digitala/fotografie-0.jpg"); ?>" alt="fotografie 0">
            </div>
        </div>
        <br><br>
        <p data-aos="fade-up">Pentru macrofotografia intraorală de înaltă calitate sunt implicate:</p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">un aparat de fotografiat DSRL cu obiectiv macro (85–105 mm) și un bliț inelar extern montat în fața obiectivului.</li>
            <li data-aos-delay="100" data-aos="fade-left">retractorii obrajilor, accesorii esențiale folosite pentru a retrage buzele, mucoasa labială și bucală din câmpul vizual.</li>
            <li data-aos-delay="200" data-aos="fade-left">oglinzile intraorale sunt neprețuite atunci când se realizează imagini cu vedere ocluzală și bucală, deoarece unghiul fotografic nu permite de obicei captarea acestor fotografii dintr-o vedere directă.</li>
        </ul>
        <div class="row photos">
            <div class="col-md-4 p-0">
                <img data-aos="fade-down" class="flex-fill " src="<?php echo media_url("stomatologie-digitala/fotografie-1.jpg"); ?>" alt="fotografie 1">
            </div>
            <div class="col-md-5 p-0">
                <img data-aos="fade-down" class="flex-fill " src="<?php echo media_url("stomatologie-digitala/fotografie-2.jpg"); ?>" alt="fotografie 2">
            </div>
            <div class="col-md-3 p-0">
                <img data-aos="fade-down" class="flex-fill" src="<?php echo media_url("stomatologie-digitala/fotografie-3.jpg"); ?>" alt="fotografie 3">
            </div>
        </div>
        <br><br>
        <p data-aos="fade-up"><strong>Avantajele folosirii fotografiei dentare</strong></p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Diagnostic și comunicare în cadrul echipei medicale</li>
            <li data-aos-delay="100" data-aos="fade-left">Auto-evaluarea rezultatelor obținute</li>
            <li data-aos-delay="200" data-aos="fade-left">Comunicarea cu pacientul și laboratorul de tehnică dentară</li>
            <li data-aos-delay="300" data-aos="fade-left">Realizarea planificării digitale a unor soluții terapeutice</li>
            <li data-aos-delay="400" data-aos="fade-left">Studiu medical și evolutia tratamentelor alese în timp</li>
            <li data-aos-delay="500" data-aos="fade-left">Urmărirea în timp a tratamentelor efectuate</li>
        </ul>
    </div>
</section>
<section class="section-9">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9"><p>Vino la o consultație și află mai multe despre sănătatea dinților tăi!</p></div>
            <div class="col-md-3">
                <a class="js-open-form-modal" href="<?php echo base_url($this->language['url_key'] . '/contact'); ?>">Vreau o programare</a>
            </div>
        </div>
    </div>
</section>