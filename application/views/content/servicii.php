<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
        <div class="nav-list-heading">
            <ul class="d-flex" role="tablist">
                <li data-aos="fade-left" data-aos-delay="0" class="tab-list-item list-inline-item current">
                    <a class="tab-link" href="#chirurgie">Chirurgie</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="50" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#endodontie">Endodonție</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="100" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#estetica">Estetică dentară</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="150" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#implantologie">Implantologie</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="200" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#odontologie">Odontologie</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="250" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#ortodontie">Ortodonție</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="300" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#parodontologie">Parodontologie</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="350" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#pedodontie">Pedodonție</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="400" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#profilaxie">Profilaxie</a>
                    <span class="spot">&nbsp;</span>
                </li>
                <li data-aos="fade-left" data-aos-delay="450" class="tab-list-item list-inline-item">
                    <a class="tab-link" href="#protetica">Protetică</a>
                    <span class="spot">&nbsp;</span>
                </li>
            </ul>
        </div>
        <div class="fluid-gallery"></div>
    </div>
</section>
<section class="section-2 tabs">
    <div class="container">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="chirurgie" role="tabpanel">
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Date generale despre tratamentele de chirurgie orală</h2>
                <p data-aos="zoom-in">Tratamentele de chirurgie orală sunt necesare atunci când trebuie salvați sau îndepărtați dinții afectați de complicații severe. Acest tip de tratamente pot de asemenea pregăti terenul pentru refacerile protetice ulterioare sau pentru inserția implanturilor.</p>
                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de chirurgie orală pe care le utilizăm</h2>
                <p data-aos="zoom-in">Chirurgia orală implică proceduri complexe, fiind un domeniu vast al stomatologiei. În clinica noastră vei putea beneficia de toate intervențiile de chirurgie orală, printre care:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">extracții dentare</li>
                    <li data-aos="fade-left" data-aos-delay="50">descoperiri chirurgicale ale dinților incluși</li>
                    <li data-aos="fade-left" data-aos-delay="100">chirurgie parodontală – gingivectomii: planarea radiculară, chiuretajul subgingival în câmp închis / deschis</li>
                    <li data-aos="fade-left" data-aos-delay="150">chirurgie endodontică – rezecții apicale: îndepărtarea vârfului rădăcinii și a țesutului afectat din jurul acestuia</li>
                    <li data-aos="fade-left" data-aos-delay="200">chistectomii – extirparea chisturilor rămase intra-osos</li>
                    <li data-aos="fade-left" data-aos-delay="250">chirurgie preprotetică – regularizarea crestelor edentate în vederea protezării</li>
                    <li data-aos="fade-left" data-aos-delay="300">sinus lift – manopera chirurgicală cu un grad de dificultate ridicat, pentru mărirea ofertei osoase la maxilar, viitorul suport pentru implant</li>
                    <li data-aos="fade-left" data-aos-delay="350">implantologie</li>
                </ul>
                <p data-aos="zoom-in">Toate tratamentele sunt efectuate cu ajutorul magnificarii optice, prin lupă sau la microscop, pentru a garanta o mare precizie.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Când spunem chirurgie orală, probabil că ne gândim la procese complexe și dureroase. Din fericire, în practica noastră chirurgicală durerea este aproape inexistentă și ne place să nu complicăm lucrurile când nu este cazul. De aceea folosim materiale și tehnici care ajută la vindecarea rapidă și sănătoasă a plăgilor obținute în urma manoperelor chirurgicale orale și anumite substanțe care împiedică apariția durerii, umflarea și colorarea tegumentelor postchirurgical.</p>
                <p data-aos="zoom-in">Noi vom face tot posibilul ca pentru tine să pară simplu, indiferent de gradul de dificultate al cazului:</p>
                <ol type="a" class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">îți vom explica întotdeauna care este tratamentul indicat și de ce îl recomandăm</li>
                    <li data-aos="fade-left" data-aos-delay="50">îți vom prezenta toate argumentele pro și contra, iar tu vei avea cuvântul decisiv.</li>
                </ol>

                <p data-aos="zoom-in">La noi nu se aplică sintagma “no pain, no game.”, de aceea te așteptăm în cabinetul nostru stomatologic pentru a-ți dovedi asta. Vei simți că lucrăm, dar fără urme de durere.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</p>
            </div>

            <div class="tab-pane fade show" id="endodontie" role="tabpanel">
                <p data-aos="zoom-in">Endodonția este ramura stomatologiei care se ocupă cu tratamentul rădăcinii dintelui, partea care nu este vizibilă în cavitatea bucală. Tratamentul de endodonție al unui dinte este o etapă din cadrul unui proces complex. Salvarea și păstrarea dintelui în cavitatea bucală este misiunea noastră prin această procedură stomatologică.</p>
                <p data-aos="zoom-in">Fiecare dinte are o anatomie specifică. Molarii de minte sunt întotdeauna atipici și nu se știe câte canale pot avea sau dacă sunt operabile în totalitate. Rar se poate aplica tratamentul endodontic molarilor de minte iar teoretic ei sunt ”neendodontici”.</p>
                
                <p data-aos="zoom-in">Tratamentul de endodonție are numeroase indicații, printre care:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0"><span>îndepărtarea unui nerv mort sau infectatm</span></li>
                    <li data-aos="fade-left" data-aos-delay="50"><span>îndepărtarea nervului în scop protetic</span></li>
                    <li data-aos="fade-left" data-aos-delay="100"><span>vindecarea unui abces de la vârful dintelui</span></li>
                </ul>
                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de endodonție pe care le utilizăm</h2>
                <p data-aos="zoom-in">În tratamentul endodontic folosim sistemul de izolare diga, sistem care împiedică trecerea substanțelor utilizate pentru dezinfectarea rădăcinii dintelui în cavitatea bucală și care păstrează un câmp de lucru curat. Infectarea cu alte bacterii devine practic imposibilă.</p>
                <p data-aos="zoom-in">Diga este singura bariera care protejează, în timpul tratamentului de endodonție, infectarea dintelui cu microorganismele din cavitatea bucală. În acest fel ești protejat de contactul cu substanțele folosite pentru curățarea canalelor, substanțe care pe lângă că au un gust neplăcut, mai pot produce și iritații.</p>
                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Succesul unui tratament de endodonție constă în dezinfecția foarte riguroasă a canalelor dintelui și obturarea acestora în mod tridimensional iar echipa noastră este specializată înatenție și răbdare.</p>
                <p data-aos="zoom-in">Un alt aspect căruia noi îi acordăm un interes deosebit, este tipul de materiale utilizate. În acest fel tratamentul de endodonție are cele mai bune rezultate vizibile în timp. Folosim materiale 100% biologice, care nu dau colorații în timp dintelui și pe care le combinăm cu cele mai bune tehnici moderne în prepararea canalului.</p>
                <p data-aos="zoom-in">Alături de acestea, sistemul pe care îl utilizăm în obturarea canalelor este sistemul de obturare tridimensional, recunoscut pentru sigilarea mult mai eficientă în comparație cu sistemele clasice.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>

            </div>

            <div class="tab-pane fade show" id="estetica" role="tabpanel">
                <p data-aos="zoom-in">Știm cu toții că estetică dentară și facială are un impact puternic în relaționarea cu noi înșine și cu ceilalți. Oriunde am fi pe glob, printr-un zâmbet sănătos ne scriem cartea de vizită. Printr-un zâmbet estetic transmiți faptul că îți pasă de tine și de ceilalți, te îngrijești și îți acorzi atenție. Estetica dentară s-a născut tocmai din această dorință a oamenilor de a transmite frumosul. Această specializare se adresează pacienților care își doresc dinți sănătoși, armonizați cu fizionomia lor și cât mai albi. Pentru aceasta trebuie acordat un interes sporit în principal: curățeniei dentare, simetriei naturale a zâmbetului și feței și culorii dinților.</p>
                <p data-aos="zoom-in">Alături de estetică dentară, estetica facială contribuie la modul în care ești perceput. Aceasta presupune diverse tehnici prin care se creează un zâmbet frumos, definit de buze pline, o piele fină și lipsită de imperfecțiuni (diminuarea ridurilor, corecția conturului facial, remodelarea buzelor).</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Tehnicile folosite de noi în ceea ce privește contribuția la estetică dentară și facială</h2>
                <p data-aos="zoom-in">Modificarea zâmbetului prin transformarea celebrului “gummy smile”, care presupune descoperirea completă a dinților și a unei părți din gingie. Pentru această procedură a esteticii dentare utilizăm toxina botulinică, prin care construim, fără a fi invazivi, un zâmbet cu aspect natural.</p>
                <p data-aos="zoom-in">Albirea dentară o realizăm prin intermediul a două tehnici: albirea endodontică și albirea tuturor dinților, în funcție de necesitățile pacientului. Albirea dentară se efectuează în două etape principale: realizarea gutierelor necesare albirii și albirea propriu-zisă cu substanța destinată acestui scop. Pacientul primește gutierele și substanța de albit, pentru a căror utilizare corespunzătoare la domiciliu i se face un instructaj. Datorită faptului că substanța va fi dozată în funcție de necesitatea și preferințele pacientului, nu vor apărea sensibilități dentare.</p>
                <p data-aos="zoom-in">Estomparea ridurilor faciale și modificarea volumului buzelor prin diminuarea ridurilor, corecția conturului facial, remodelarea buzelor. Bineînțeles, și în această privință tehnicile pe care le utilizăm diferă în funcție de necesitățile și preferințele pacientului.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Considerăm că estetica dentară și facială este disciplina medicinii dentare dedicată artei de a reda zâmbetul natural, disciplina prin care un medic dentist își demonstreza în cel mai vizibil mod măiestria. La fel ca în cazul tuturor procedurilor utilizate în cadrul cabinetului nostru, și în ceea ce privește estetică dentară și facială acordăm o atenție sporită materialelor și aparaturii folosite.</p>
                <p data-aos="zoom-in">Prin utilizarea conceptului “Digital Smile Design” ne permite vizualizarea viitorului zâmbet prin aplicarea lucrărilor protetice și realizarea modificărilor dorite de pacient înaintea realizării lucrării propriu-zise. Altfel spus, cu ajutorul pozelor făcute în cabinetul nostru, putem realiza o simulare a viitorului zâmbet, asupra căreia pacientul trebuie să își dea acordul. Astfel că, prin această procedură, realizăm împreună zâmbetul dorit, original și care să te reprezinte.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>
            </div>

            <div class="tab-pane fade show" id="implantologie" role="tabpanel">

                <p data-aos="zoom-in">Implantul dentar este cea mai bună soluție pentru înlocuirea unui dinte lipsă. Actualele tehnici permit medicilor dentiști să implanteze un dinte artificial indiferent de situația care a condus la pierderea celui natural.</p>
                <p data-aos="zoom-in">Implantul dentar este un dispozitiv artificial de titan confecționat pentru a înlocui rădăcina dintelui care lipsește și se folosește ca suport pentru coroane, proteze fixe și mobilizabile. Un implant dentar are aceeași funcție ca rădăcina unui dinte natural, oferind sprijin solid pentru un dinte nou. Astfel poți redobândi un zâmbet funcțional și sănătos, fără a resimți neplăcerile cauzate de un corp artificial.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de implantologie dentară pe care le utilizăm</h2>
                <p data-aos="zoom-in">La fel ca în cadrul oricărui serviciu oferit de cabinetul nostru, toate etapele de lucru și procedurile de implantologie dentară sunt detaliate în urma unui consult amănunțit. Astfel vei ști în permanență care sunt etapele pe care va trebui să le parcurgi și ce presupun acestea.</p>
                <p data-aos="zoom-in">Recomandăm implanturile ca cele mai bune opțiuni pentru înlocuirea dinților pierduți, deoarece:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">oferă o estetică și o funcționalitate naturală</li>
                    <li data-aos="fade-left" data-aos-delay="50">nu necesită modificarea anatomică a dinților vecini, ca în cazul protezării clasice</li>
                    <li data-aos="fade-left" data-aos-delay="100">prin implantologie dentară, osului nu i se permite să se resoarbă</li>
                </ul>


                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce noi?</h2>
                <p data-aos="zoom-in">Cu ajutorul tehnicilor pe care le utilizăm în toate procedurile de implantologie dentară, inserarea unui implant este ușoară, iar recuperarea rapidă. În funcție de anumiți parametri ai consistenței sosului dumneavoastră, veți putea pleca după procedurile de implantologie dentară cu dinți provizorii pe implanturi, dinți cu care puteți zâmbi, mânca și vorbi.</p>                
                <p data-aos="zoom-in">Modalitățile prin care realizăm implantul ne ajută să redăm aspectul natural al dinților, nu doar la nivel estetic, ci și legat de funcționalitatea acestora.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>

            </div>

            <div class="tab-pane fade show" id="odontologie" role="tabpanel">

                <p data-aos="zoom-in">Odontologia este specialitatea stomatologică care tratează lezunile carioase și cele necarioase (fracturi, distrofii, leziuni de uzură dentară). Această terapie de restaurare dentară este o latură a proteticii care restabilește integritatea dinților naturali. Restaurările protetice pot fi directe (obturațiile) sau indirecte (unidentare și pluridentare ca incrustațiile, coroanele, punțile).</p>
                <p data-aos="zoom-in">În mod frecvent, dinții sunt distruși de carii. Cu toții am învățat de mici cum acestea penetrează structura dintelui și duc la modificări anatomice la suprafața lui. Modelarea suprafețelor dinților este importantă, atât din punct de vedere funcțional cât și estetic. Dacă nu se realizează reconstituirea anatomică a dinților, apar deficiențe în ocluzia pacientului și îți va fi dificil să mesteci, ceea ce poate duce chiar la o digestive dificilă.</p>
                <p data-aos="zoom-in">Cu cât leziunea dintelui este descoperită mai devreme, cu atât cresc șansele ca dintele să rămână cât mai aproape de forma inițială, fără intervenții de odontologie majore. Nu deageaba se recomandă controalele periodice pentru depistarea schimbărilor, prevenirea și tratarea cariilor în faza incipientă.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de restaurare odontologică pe care le utilizăm</h2>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">Obturații metalice</li>
                    <li data-aos="fade-left" data-aos-delay="50">Obturații fizionomice</li>
                    <li data-aos="fade-left" data-aos-delay="100">Incrustații sau obturații indirecte – Inlay și Onlay (coroană parțială)</li>
                    <li data-aos="fade-left" data-aos-delay="150">Fațetări</li>
                    <li data-aos="fade-left" data-aos-delay="200">Coroane și punți dentare</li>
                </ul>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">„Modelarea unui dinte frontal este teza de doctorat a fiecărui stomatolog”. Reușita intervenției se reflectă în obturarea dinților frontali și aceasta poate fi considerată o carte de vizită pentru noi. Acest proces trebuie realizat în așa fel încât să nu se observe faptul că pacientul este posesorul unei obturații pe dintele respectiv. De aceea, în cadrul intervenției de odontologie folosim materiale de reconstituire de ultimă generație, cu o rezistență crescută în timp. Rezultatul nu întârzie să apară: prin folosirea acestor materiale și datorită tehnicii pe care o utilizăm, estetica dintelui, în urma intervenției de odontologie, rămâne identică cu cea a dintelui natural. Trebuie menționat faptul că în rezultatele pe care le avem, o contribuție majoră o au și cursurile de perfecționare a tehnicilor de realizare a obturațiilor, la care echipa noastră participă constant.</p>      
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>

            </div>

            <div class="tab-pane fade show" id="ortodontie" role="tabpanel">

                <p data-aos="zoom-in">Ortodonția este partea stomatologiei care se ocupă cu prevenirea și tratamentul pozițiilor defectuase ale dinților, prin așezarea în poziție normală a dinților pe arcadele dentare. Altfel spus, prin procedurile de ortodonție, dintii strâmbi sau aliniați incorect sunt așezați în poziție corespunzătoare.</p>                
                <p data-aos="zoom-in">Încă din perioada copilăriei, prin vizitele regulate la medicul stomatolog, este indicat să se intercepteze anomaliile dentare. În caz contrar, odată ajunși la maturitate, procedurile de ortodonție devin mai complexe.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de ortodonție pe care le utilizăm</h2>
                <p data-aos="zoom-in">Tratamentul ortodontic se realizează prin diverse tipuri de aparate. În urma unei analize amănunțite a tipului de anomalie, recomandăm tipul de aparat corespunzător: fix sau mobil, pentru realinierea dinților, reantrenarea mușchilor și influențarea creșterii maxilarelor.</p>
                <p data-aos="zoom-in">Durata tratamentelor ortodontice depinde de gravitatea anomaliei și de vârsta pacientului. În funcție de aceste două criterii principale, stabilim și personalizam procedurile care trebuie urmate, plus tipul de aparat potrivit pacientului.</p>
                <p data-aos="zoom-in">Plecând de la recomandările noastre, ajungem la preferințele pacientului, care poate alege tipul de aparat dorit, precum și personalizarea acestuia.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">La fel ca în cazul tuturor serviciilor pe care le oferim, și în ceea ce privește tratamentele de ortodonție folosim tehnologiile și materialele de ultima generație. Și nu ne rezumăm la asta.</p>
                <p data-aos="zoom-in">Pentru că medicina dentară este într-o continuă evoluție, medicul nostru specializat în ortodonție se afla într-o continuă perfecționare. Pentru că zâmbetul tău merită cele mai bune și performante tehnici și aparaturi.</p>
                <p data-aos="zoom-in">La fel cum fiecare persoană are trăsături proprii, fiecare zâmbet este diferit. Pentru noi, ortodonția nu înseamnă a aduce zâmbetul la un anumit standard de frumusețe, ci a oferi fiecărei persoane zâmbetul care să îi dea încredere în sine. Prin tratamentele de ortodonție pe care le efectuăm, nu ne rezumăm doar la alinierea dinților, ci investigăm amănunțit tipul de anomalie, pentru care recomandăm aparatul potrivit.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>

            </div>

            <div class="tab-pane fade show" id="parodontologie" role="tabpanel">
                <p data-aos="zoom-in">Odată cu modernizarea umanității, se complica și afecțiunile omului. S-a observat faptul că din ce în ce mai mulți pacienți prezintă afecțiuni ale parodonțiului marginal, implicit ale dinților.</p>
                <p data-aos="zoom-in">Cele mai frecvente cauze ale bolii parodontale sunt reprezentate de:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">tartrul dentar</li>
                    <li data-aos="fade-left" data-aos-delay="50">absența dinților de pe arcade</li>
                    <li data-aos="fade-left" data-aos-delay="100">restaurările protetice maladaptate</li>
                    <li data-aos="fade-left" data-aos-delay="150">obiceiuri vicioase (fumatul)</li>
                    <li data-aos="fade-left" data-aos-delay="200">afecțiuni generale și chiar unele medicamente</li>
                </ul>

                <p data-aos="zoom-in">Boala parodontală este sistematizată în mai multe etape, evoluția ei fiind de la simplă la complicată. Cu cât este mai devreme descoperită, cu atât este mai simplu de tratat. Principalele semne clinice care ar trebui să vă dea de gândit sunt reprezentate de:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">sângerarea gingiilor la periaj</li>
                    <li data-aos="fade-left" data-aos-delay="50">schimbarea culorii gingiilor de la roz spre roșu</li>
                    <li data-aos="fade-left" data-aos-delay="100">mărirea volumului gingiilor</li>
                    <li data-aos="fade-left" data-aos-delay="150">apariția aspectului de coajă de portocală</li>
                    <li data-aos="fade-left" data-aos-delay="200">retragerea gingiilor și mobilitatea dinților</li>
                </ul>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile pe care le utilizăm în tratamentul parodontologic</h2>
                <p data-aos="zoom-in">Tratametul parodontitelor este relativ simplu, dar necesită răbdare și consecvență din partea dumneavoastră. Prima etapă constă în eliminarea factorilor cauzali, urmată de etapa întreținerii și apoi vindecarea propriu-zisă cu refacerea structurilor afectate.</p>
                <p data-aos="zoom-in">Pe parcursul tratamentului de parodontologie, monitorizăm cu ajutorul microscopului electronic evoluția germenilor din pungile parodontale. Acest lucru ne poate confirma evoluția procesului de vindecare. La fiecare vizită stomatologica a dumneavoastră în cadrul terapiei bolii parodontale, monitorizăm schimbările sănătății orale, pentru a verifica în permanență evoluția către vindecare.</p>
                <p data-aos="zoom-in">Alături de fișele în care se notează evoluția bolii dumneavoastră, se vor găsi fotografii cu situația inițială pe parcursul tratamentului și la final. Astfel, datorită evoluției stomatologiei digitale, oricând vă putem pune la dispoziție, cu exactitate, istoricul bolii dumneavoastră.</p>
                

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Considerăm că tratamentele de parodontologie trebuie adaptate fiecărui pacient în parte. De aceea, analizăm fiecare caz în parte în privința depistării germenilor (există mai multe specii ale bacteriei prezente în boală parodontală) și aplicăm un tratament corespunzător (nu toate bacteriile sunt sensibile la un anumit tratament, motiv pentru care tratamentul aplicat trebuie să fie specific).</p>
                <p data-aos="zoom-in">Pentru eficientizarea tratamentului și o experiență cât mai ușoară pe scaunul nostru, am elaborat și menținem un program sistematizat în etape pentru tratarea bolii parodontale. Pornim de la analizele speciale pentru detectarea parodontozei, făcute în Germania, continuăm cu elaborarea schemei personalizate de tratament și urmărim în timp parametrii prin controale și analize periodice.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>

            </div>

            <div class="tab-pane fade show" id="pedodontie" role="tabpanel">
                <p data-aos="zoom-in">Pedodonția înseamnă stomatologie pediatrică, adică acea parte a medicinei dentare care se adresează copiilor. Tratamentele de pedodonție sunt proceduri importante în viața oricărui copil, începând de la cele mai mici vârste, deoarece pierderea precoce a dinților “de lapte” poate provoca diverse complicații de erupție ale dinților definitivi, care va influența viața adultă a danturii micilor pacienți.</p>
                <p data-aos="zoom-in">Stomatologia copilului este mult diferită de stomatologia adultului. Încă din această perioadă se pot preveni anumite modificări dentare, motiv pentru care monitorizarea sănătății orale a copilului este esențială. Vizitele regulate la stomatolog, încă de la apariția primilor dinți, influențează în bine zâmbetul și sănătatea orală a viitorului adult.</p> 

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de pedodonție utilizate în cabinetul nostru</h2>
                <p data-aos="zoom-in">Considerăm că tratamentul copiilor necesită o atenție deosebită în privința comunicării dintre pacient și medicul stomatolog, deoarece primele vizite ale micuților la cabinet le influențează încrederea în sistemul stomatologic. De aceea, înainte de a porni orice tratament de pedodonție, încercam să găsim, alături de părinți, calea de comunicare potrivită fiecărui mic pacient în parte.</p>
                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Principalele tratamente de pedodonție pe care le efectuăm:</h2>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">Fluorizări realizate în cabinet</li>
                    <li data-aos="fade-left" data-aos-delay="50">Sigilarea dinților permanenți</li>
                    <li data-aos="fade-left" data-aos-delay="100">Interceptarea anomaliilor dentare și rezolvarea acestora încă de la primele semne,
pentru a împiedica malpozitionarea dinților</li>
                </ul>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Știm că rolul nostru nu constă doar în elaborarea și aplicarea tratamentelor de pedodonție, ci se extinde în educarea copiilor legat de îngrijirea corecte a dinților. Treptat, copiii trebuie să dobândească tehnicile necesare pentru a-și îngriji singuri și într-un mod corect dinții, iar în acest proces, alături de părinți, noi, medicii dentiști, avem un rol esențial. De aceea, echipa noastră nu se rezumă la efectuarea tratamentelor de pedodonție, ci se străduiește să găsească întotdeauna calea pentru a-i educa legat de:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">Profilaxia anomaliilor dento-maxilare</li>
                    <li data-aos="fade-left" data-aos-delay="50">Prevenirea cariilor și afecțiunilor gingivale</li>
                    <li data-aos="fade-left" data-aos-delay="100">Controlul plăcii bacteriene</li>
                    <li data-aos="fade-left" data-aos-delay="150">Indicații pentru periajul dentar și igiena bucală corectă</li>
                </ul>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>
            </div>

            <div class="tab-pane fade show" id="profilaxie" role="tabpanel">
                
                <p data-aos="zoom-in">Cu toate că dinții sunt unele dintre cele mai puternice componente ale organismului, ei pot fi atacați și distruși de bacterii. Profilaxia dentară este modalitatea de prevenire a îmbolnăvirii dentare și a complicațiilor, în cazul în care afecțiunile există deja.</p>
                <p data-aos="zoom-in">Majoritatea problemelor dinților pot fi prevenite, iar prin prevenție poți evita multe complicații stomatologice. Sănătatea ta primează, așa că cea mai importantă măsura luată în acest sens este realizarea unei igienizări riguroase și sănătoase.</p>
                <p data-aos="zoom-in">Respectând vizitele regulate la stomatolog reduceți semnificativ costurile alocate recăpătării sănătății dinților.</p>
                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de profilaxie utilizate în cabinetul nostru</h2>
                <p data-aos="zoom-in">Profilaxia este un proces sistematic, pe durata căruia vom urmări și identifică starea sănătății orale. Prin această procedură pot fi depistate problemele și dezvoltarea unui tratament personalizat de igienizare și profilaxie.</p>
                <p data-aos="zoom-in">Procedurile pe care le efectuăm sunt:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">Identificarea și îndepărtarea plăcii bacteriene</li>
                    <li data-aos="fade-left" data-aos-delay="50">Fluorizări</li>
                    <li data-aos="fade-left" data-aos-delay="100">Sigilarea șanțurilor și fosetelor</li>
                    <li data-aos="fade-left" data-aos-delay="150">Detartraj și periaj profesional</li>
                    <li data-aos="fade-left" data-aos-delay="200">Control periodic</li>
                </ul>
                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Considerăm că, pentru sănătatea orală a fiecărui pacient, este necesară acordarea unei atenții deosebite comunicării dintre pacient și echipa medicală. De aceea, echipa noastră nu se rezuma la tratarea afecțiunilor deja existente, ci este pregătită să acorde sfaturile necesare pentru menținerea sănătății orale.</p>
                <p data-aos="zoom-in">La finalul ședințelor de igienizare (procedura principală în profilaxie), tratam dinții cu pastă care remineralizează structura dentară, lucru care va avea efecte pozitive vizibile în timp asupra danturii.</p>
                <p data-aos="zoom-in">Cabinetul nostru funcționează pe baza programului de recall. Aceasta înseamnă că nu trebuie să țineți evidenta ultimei vizite la stomatolog, fiindcă, datorită acestui program, veți fi anunțat când au trecut 6 luni de la data ultimei vizite la noi. Considerăm acest program foarte util, atât pentru dumneavoastră cât și pentru noi. Pentru dumneavoastră, deoarece vă scutește de încă o grijă pe care trebuie să o purtați, iar pe noi ne ajută în asumarea răspunderii referitoare la sănătatea orală a a fiecărui pacient în parte.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>
            </div>

            <div class="tab-pane fade show" id="protetica" role="tabpanel">
                <p data-aos="zoom-in">Protetica este ramura stomatologiei care se ocupă cu: înlocuirea dinților pierduți sau cu ajutorul coroanelor protetice, menținerea dinților tratați endodontic. Odată cu tratamentul endodontic aplicat unui dinte, rezistența lui este scăzută și devine casant. Din această cauză, este recomandată aplicarea unei coroane protetice peste coroană clinică a dintelui. În cazul în care lipsesc dinți, se realizează punți dentare alcătuite din coroanele dinților absenți și coroanele care vor fi cimentate pe dinții stâlpi (dinții naturali șlefuiți).</p>
                <p data-aos="zoom-in">O altă subramura stomatologică ce aparține de protetica este reprezentată de realizarea coroanelor pe implanturi. După vindecarea completă a implantului propriu-zis (aproximativ 6 luni), se realizează coroana definitivă. Aceasta coroană se realizează în așa fel încât să fie identică cu coroana dintelui natural, atât din punct de vedere estetic, cât și funcțional.</p> 
                                
                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Procedurile de protetică utilizate în cabinetul nostru</h2>
                <p data-aos="zoom-in">Întotdeauna preferăm protetica pe implanturi în detrimentul proteticii clasice, în care se șlefuiesc dinții naturali. Șlefuirea dinților naturali nu este singurul argument contra proteticii clasice, ci și faptul că dinții absenți înlocuiți în protezarea clasică au sprijin pe gingie sau în aer. Cu timpul, osul se retrage și sprijinul va rămâne în aer.</p>

                <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De ce la noi?</h2>
                <p data-aos="zoom-in">Suntem de părere că într-un cabinet stomatologic tehnicile de protetica utilizate trebuie să reflecte iscusința și imaginația medicului stomatolog. Motiv pentru care, în privința fiecărei lucrări în parte, încercam să găsim soluțiile personalizate.</p>
                <p data-aos="zoom-in">De asemenea, prin anumite tehnici digitale (Digital Smile Design) ne permitem ca, în urma analizei preferințelor și necesităților pacientului, să realizăm simularea viitoarei lucrări protetice.</p>
                <p data-aos="zoom-in">Astfel, veți vedea, înainte de a porni procedurile de protetica, dacă viitoarele rezultate se pliază pe cerințele dumneavoastră.</p>
                <p data-aos="zoom-in">Pe lângă acestea, în realizarea lucrărilor de protetica, ne recomandă:</p>
                <ul class="pl-3">
                    <li data-aos="fade-left" data-aos-delay="0">realizarea lucrărilor protetice provizorii în cabinet (pacientul nu va pleca niciodată cu dinții neacoperiți, neprotejați);</li>
                    <li data-aos="fade-left" data-aos-delay="50">realizarea unor coroane care imită în detaliu anatomia și morfologia dintelui natural, în colaborare cu tehnicianul și cu ajutorul materialelor de ultimă generație pe care le utilizăm;</li>
                    <li data-aos="fade-left" data-aos-delay="100">coroanele realizate pe implant de către noi sunt asemănătoare cu situația naturală.</li>
                </ul>
                <p data-aos="zoom-in">Senzația este ca dintele “iese din gingie”, o senzație asemănătoare cu cea naturală.</p>
                <p data-aos="zoom-in">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, accesează secțiunea cu indicații utile pentru pacienți.</p>
            </div>
        </div>
    </div>
</section>
