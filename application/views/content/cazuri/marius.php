<div class="tab-pane fade has-img-left show" id="marius" role="tabpanel">
    <div class="person">
        <div class="container">
            <div class="row no-gutter align-items-end">
                <div class="col-lg-5">
                    <img class="person-image" data-aos="fade-right" data-aos-delay="0" src="<?php echo media_url('cazuri/marius/marius.jpg') ?>" alt="Marius">
                </div>
                <div class="col-lg-7">
                    <div class="text">
                        <h2 data-aos="fade-down" data-aos-delay="50">Marius</h2>
                        <p data-aos="fade-down" data-aos-delay="100">
                            Am venit la clinica Smile Vision pentru lucrări mici. După multe experiențe neplăcute care au dus și la pierderea a două măsele, am venit reticent și rezervat. În schimb aici am văzut cum se lucrează, cum decurg lucrurile și era altceva. Am prins încredere și curaj. M-am lăsat pe mâna lor pentru intervențiile de implant dentar necesare și totul a decurs simplu și rapid. 
                            <br><br>
                            La un consult, domnul doctor Paul Zaharia m-a întrebat dacă nu vreau să fac ceva și cu cei doi incisivi. Îi aveam de când mă știu și nu mi-am pus niciodată problema lor. 
                            <br><br>
                            Mi-a făcut o simulare, am testat-o câteva zile și am fost extrem de încântat. Cât de mult contează un zâmbet aliniat, armonios, frumos... mă simțeam tare bine. Nu am mai stat pe gânduri și am mers pe mâna lor cu un tratament definitiv: ajustarea gingiei și aplicarea de fațete... cea mai bună alegere.
                            <br><br>
                            Sincer mă simt împlinit, satisfăcut și zâmbesc non-stop… câteodată mă întreb dacă nu exagerez :) Mă bucur că v-am găsit și am dat curs propunerilor voastre.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row gallery">
            <div class="main-image"><img data-aos="fade-down" data-aos-delay="0" src="<?php echo media_url('cazuri/marius/1.jpg') ?>" alt="" class="main-image"></div>
            <div class="row no-gutter">
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/marius/2.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/marius/3.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="150" src="<?php echo media_url('cazuri/marius/4.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="200" src="<?php echo media_url('cazuri/marius/5.jpg') ?>" alt=""></div>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Marius și-a dorit ca cei doi incisivi laterali să aibă o formă normală, bine armonizată.</p>
                <p>Dinții erau foarte mici și aveau o poziție ectopică.</p>
                <p>În zona extracțiilor osul gingival era foarte retras.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Simularea zâmbetului folosind tehnica WAX-UP realizată de tehnician și MOCK-UP realizată în clinică, pentru experiența reală a rezultatului final înainte de începerea tratamentului.</li>
                    <li>Aplicare fațetelor cu o preparare minimă a dinților (la un dinte nefiind deloc necesară)</li>
                    <li>În zona extracțiilor s-a efectuat: inserare implant + grefă de țesut conjunctiv + capă vindecare</li>
                    <li>S-a abordat o tehnică mai simplă și foarte predictibilă care are ca rezultat un timp de așteptare mai mic (decât dacă am fi făcut adiții osoase) cu recuperare rapidă și disconfort minim</li>
                </ul>
                <p>Numărul de intervenții a fost minim, deoarece a fost posibil să aplicăm capa de vindecare în timpul operației și la 2 luni s-au aplicat dinții lipsă.</p>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-md-9">Paul Zaharia</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sorina-copaci.jpg') ?>" alt="Sorina Copaci"></div>
                    <div class="col-md-9">Sorina Copaci</div>
                </div>
            </div>
        </div>
        <!-- <div class="row impressions">
            <h2>Impresii dupa interventie</h2>
        </div> -->
    </div>
</div>