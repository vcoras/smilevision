<div class="tab-pane fade show has-cover-img" id="iva" role="tabpanel">
    <div class="person">
        <div class="container container-960">
            <div class="row">
                <h2 data-aos="fade-down" data-aos-delay="50">Iva</h2>
                <p>Știi momentul acela în care ai o idee, dar nu știi de unde s-o apuci și cum să o finalizezi? Cam așa am ajuns eu la Dr. Paul Zaharia, unde spre marea mea surpriză am avut parte de clarificare și de pași concreți asupra unei scheme de tratamente complexe și complete la finalul căreia am obținut ceea ce mi-am dorit: o dantură sănătoasă și frumoasă.</p>
                <p>Am descoperit o echipă de medici dedicați, implicați și profesioniști. M-am bucurat de fiecare pas (chiar dacă se spune că la stomatolog nu ai cum să mergi cu plăcere) și pot spune că fiecare vizită la Clinica Smile Vision este ca o mică reuniune de familie. RECOMAND cu 10 stele!!!</p>
                <p>Mulțumesc Dr. Paul Zaharia! Mulțumesc Smile Vision!</p>
                <img src="<?php echo media_url("cazuri/iva/1.jpg"); ?>" alt="Iva si Dr. Paul Zaharia">
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row no-gutter">
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/iva/2.jpg') ?>" alt="Inainte">
                <p>Înainte</p>
            </div>
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/iva/3.jpg') ?>" alt="Dupa">
                <p>După</p>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Provocare estetică - persoană publică / interacțiune socială frecventă.</p>
                <p>Probleme la nivelul gingiilor / parodonțiului de susținere a dinților.</p>
                <p>Lipsa unei funcționalități corespunzătoare masticatorii.</p>
                <p>S-a început cu stabilizarea parodonțiului/gingiilor astfel încât tratamentele ulterioare să fie realizate pe o bază stabilă și sănătoasă. Extracția dinților cu probleme și inserarea imediată a implanturilor împreună cu coroane provizorii pentru a restaura masticația și pentru un efect estetic/funcțional desăvârșit. În timpul intervenției s-au realizat atât augmentare osoasă cât și grefe de gingie pentru a compensa resorbția osoasă fiziologică, ce apare inevitabil în urma oricăror extracții.</p>
                <p>Realizarea unei simulări cu dinții finali.</p>
                <p>Realizarea fațetelor integral din ceramică și a coroanelor finale.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Igienizare - testare pentru germeni parodontali - tratament parodontal.</li>
                    <li>Extracțiile molarilor și inserarea implanturilor cu adiție de os, grefă de țesut conjunctiv, coroane provizorii.</li>
                    <li>Wax-up / Mock-up - simularea noului zâmbet și “purtarea” lui câteva zile astfel încât să avem o confirmare că totul este bine dpdv estetic și funcțional.</li>
                    <li>Prepararea dinților în vederea realizării și cimentării fațetelor.</li>
                    <li>Executarea unei gutiere de bruxism pentru a minimiza efectul bruxismului de-a lungul timpului = uzura rapidă a dinților și posibil decimentarea fațetelor.</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-md-9">Paul Zaharia</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sergiu-buzatu.jpg') ?>" alt="Sergiu Buzatu"></div>
                    <div class="col-md-9">Sergiu Buzatu</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/mario-chilom.jpg') ?>" alt="Mario Chilom"></div>
                    <div class="col-md-9">Mario Chilom</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sorina-copaci.jpg') ?>" alt="Sorina Copaci"></div>
                    <div class="col-md-9">Sorina Copaci</div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="impressions">
        <div class="container container-960">
            <video class="clearfix" controls poster="<?php echo media_url('cazuri/iva/video.jpg'); ?>">
                <source src="<?php echo media_url("cazuri/iva/interviu.mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
    </div>
</div>