<div class="tab-pane fade show has-cover-img" id="loredana" role="tabpanel">
    <div class="person">
        <div class="container container-960">
            <div class="row">
                <h2 data-aos="fade-down" data-aos-delay="50">Loredana</h2>
                <p>Am mers întotdeauna cu groază la dentist. Mi-a fost mereu frică și teamă de medicii stomatologi. Asta a fost cea mai mare temere a mea. Aici l-am întâlnit pe Dr. Paul Zaharia, un om de milioane și de o finețe de nedescris. Am uitat ce înseamnă durerea de dinți și am uitat ce înseamnă frica de stomatolog. Acum mă simt împlinită, pot zâmbi liniștită.</p>
                <p>M-am uitat în oglindă, am zâmbit și mi-am spus:</p>
                <p>“În sfârșit pot să zâmbesc”.</p>
                <img src="<?php echo media_url("cazuri/loredana/1.jpg"); ?>" alt="Poza Loredana">
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row no-gutter">
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/loredana/2.jpg') ?>" alt="Inainte">
                <p>Înainte</p>
            </div>
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/loredana/3.jpg') ?>" alt="Dupa">
                <p>După</p>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Lucrările anterioare au avut un impact negativ emoțional și social. Loredana nu zâmbea, se ferea chiar să vorbească și avea dureri atunci când a venit la noi.</p>
                <p>După rezolvarea urgențelor i-am prezentat posibilitatea de a avea o avea o dantură sănătoasă, fără probleme pe termen lung. Libertatea de a zâmbi în voie a inspirat-o și astfel a ales să nu mai amâne începerea tratamentelor.</p>
                <p>După îndepărtarea lucrării vechi a fost necesară extragerea incisivul central înlocuindu-l cu un implant. Inserarea implantului nu s-a putut realiza în aceeași ședință cu extracția ceea ce a făcut cazul o nouă provocare estetică.</p>
                <p>O altă provocare a fost faptul că lucrarea inițială nu respecta ghidajele estetice/funcționale și că anatomia pacientei era o provocare în sine - lipsa simetriei faciale și a simetriei poziționării maxilarelor în planul feței.</p>
                <p>A fost nevoie de mai multe rânduri de lucrări provizorii pentru a testa funcționalitatea și adaptarea la noua poziție.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Tratarea dinților ce nu erau în lucrările protetice.</li>
                    <li>Îndepărtarea lucrărilor protetice existente necorespunzătoare și realizarea extracțiilor pentru dinții ce nu se puteau salva. Pentru eliminarea oricărui impact funcțional și social s-au executate lucrări provizorii pe toată durata intervențiilor.</li>
                    <li>Tratarea tuturor dinților care necesită intervenții iar după două luni de la extracție s-a inserat un implant în locul centralului superior împreună cu o coroană provizorie imediată.</li>
                    <li>După osteointegrarea implantului s-a efectuat toată lucrarea protetică superioară din coroane și punți de Zirconia Monolithic.</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-md-9">Paul Zaharia</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sergiu-buzatu.jpg') ?>" alt="Sergiu Buzatu"></div>
                    <div class="col-md-9">Sergiu Buzatu</div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="impressions">
        <div class="container container-960">
            <video class="clearfix" controls poster="<?php echo media_url('cazuri/loredana/video.jpg'); ?>">
                <source src="<?php echo media_url("cazuri/loredana/interviu.mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
    </div>
</div>