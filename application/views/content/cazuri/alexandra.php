<div class="tab-pane fade has-img-left active show" id="alexandra" role="tabpanel">
    <div class="person">
        <div class="container">
            <div class="row no-gutter align-items-end">
                <div class="col-lg-5">
                    <img class="person-image" data-aos="fade-right" data-aos-delay="0" src="<?php echo media_url('cazuri/alexandra/alexandra.jpg') ?>" alt="Alexandra">
                </div>
                <div class="col-lg-7">
                    <div class="text">
                        <h2 data-aos="fade-down" data-aos-delay="50">Alexandra</h2>
                        <p data-aos="fade-down" data-aos-delay="100">Mereu am ținut la dantura mea și am avut grijă de ea mergând la dentist regulat și am ținut cont de toate sfaturile lui. Mi-aș fi dorit ca eforturile mele să nu fi fost în zadar. Însă cu toate astea, cu mulți ani în urmă am avut parte de tratamente care au scăzut rezistența dinților.
                        <br><br>
                        De data asta mi-am pus toată încrederea în echipa Smile Vision și totul a ieșit foarte bine. Nu sunt happy că am implant si coroane... dar măcar arată ok, arată natural, ăsta e singurul lucru care mă liniștește. Cât despre experiența mea, ce pot să spun? Dacă nu aveam încredere nu alegeam să fac pasul ăsta. Sori este și prietena mea și dentistul meu, iar prin ea l-am cunoscut și pe domnul doctor Zaharia. Toti sunt oameni faini, prietenoși, glumeți și cel mai important, sunt profi. I-am recomandat și până acum și i-aș recomanda oricând :)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row gallery">
            <div class="main-image"><img data-aos="fade-down" data-aos-delay="0" src="<?php echo media_url('cazuri/alexandra/1.jpg') ?>" alt="" class="main-image"></div>
            <div class="row no-gutter">
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/alexandra/2.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/alexandra/3.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="150" src="<?php echo media_url('cazuri/alexandra/4.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="200" src="<?php echo media_url('cazuri/alexandra/5.jpg') ?>" alt=""></div>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 pr-5 case">
                <h3>Particularitatea cazului</h3>
                <p>În încercarea de a restaura incisivul central, după îndepărtarea coroanei vechi necorespunzătoare din punct de vedere estetic, s-a constatat că dintele era distrus până la nivelul gingiei. În acest caz orice încercare de restaurare nu are un prognostic bun pe termen lung. Este recomandată extracția iar după efectuarea ei s-a inserat imediat un implant dentar urmat de o coroană provizorie.</p>
                <p>Dificultatea cazului a constat în păstrarea aspectului estetic al incisivului central, dinte care are vizibilitatea cea mai mare și care domină expresia zâmbetului și a feței.</p>
                <p>Responsabilitatea este foarte mare, pentru că cea mai mică greșeală produce un dezechilibru care are un impact major asupra esteticii.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 pr-5 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Tratament canal, Reconstituire</li>
                    <li>Ablație coroană, Extracție, Inserare Implant, Coroană provizorie</li>
                </ul>
                <p>La două luni după intervenție s-a realizat lucrarea finală (3 coroane integral ceramice, 3 fațete) pe un teren sănătos și stabil.</p>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-9">Paul Zaharia</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-3"><img src="<?php echo media_url('cazuri/medici/sorina-copaci.jpg') ?>" alt="Sorina Copaci"></div>
                    <div class="col-9">Sorina Copaci</div>
                </div>
            </div>
        </div>
        <!-- <div class="row impressions">
            <h2>Impresii dupa interventie</h2>
        </div> -->
    </div>
</div>