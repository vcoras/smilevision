<div class="tab-pane fade show has-cover-img" id="erika" role="tabpanel">
    <div class="person">
        <div class="container container-960">
            <div class="row">
                <h2 data-aos="fade-down" data-aos-delay="50">Erika</h2>
                <p>Dacă ar fi să mă descriu în trei cuvinte aș spune că sunt o ființă pozitivă, comunicativă și creativă. Din aceste motive, cred, am început de câțiva ani să creez content digital, visul meu fiind realizarea unui canal de youtube. Dar datorită unor pete închise la culoare și formei dinților mei, simțeam mereu că nu pot zâmbi cu adevărat în fața camerei și nu pot prezenta publicului versiunea mea cea mai autentică.</p>
                <p>La Smile Vision nu am descoperit doar o echipă de profesioniști, ci o familie care m-a întâmpinat cu zâmbete și empatie, gata să mă asculte și să mă încurajeze să cred din nou în visul meu. Comunicarea și colaborarea dintre mine și întreaga echipă a fost una excelentă, iar acest lucru a rezultat în găsirea formei și culorii perfecte pentru dinții mei cu ajutorul fațetelor dentare. Rezultatul a fost unul fabulos! Acum primesc mereu complimente pentru dinții perfecți și mă așez cu încredere și entuziasm în fața camerei.</p>
                <p>Echipa Smile Vision a contribuit la crearea celei mai bune versiuni a mea de până acum!</p>
                <p>Hugs,<br>Erika</p>
                <img src="<?php echo media_url("cazuri/erika/1.jpg"); ?>" alt="Portret Erika">
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row no-gutter">
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/erika/2.jpg') ?>" alt="Inainte">
                <p>Înainte</p>
            </div>
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/erika/3.jpg') ?>" alt="Dupa">
                <p>După</p>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Erika este o ființă plină de viață, cu zâmbetul mereu pe buze. Atunci când zâmbește, îi zâmbesc și ochii. Din cauza incisivilor centrali colorați, zâmbetul larg era de fapt cu buzele apropiate, și doar ochii mai “povesteau” zâmbetul.</p>
                <p>Incisivii centrali prezentau colorații datorită unor tratamente endodontice realizate cu mulți ani în urmă, incisivii laterali erau afectați de procese carioase, prezentând obturații, iar premolarul din dreapta sus a suferit tratamente la nivelul rădăcinii, colorându-se și el.</p>
                <p>Împreună cu tehnicianul nostru, ținând cont de dorințele pacientei, am realizat cea mai estetică și durabilă soluție. Prima etapă a fost retratarea rădăcinilor și proceselor carioase secundare, realizarea a două coronițe pe incisivii centrali, două fațete pe incisivii laterali și o coroniță pe primul premolar, toate elementele protetice urmând să fie realizate individual din ceramică.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Tratarea cariilor secundare de pe incisivii laterali.</li>
                    <li>Re-tratamentele endodontice cu înlocuirea obturațiilor coronare a incisivilor și a primului premolar.</li>
                    <li>Realizarea unei simulări (realizarea coroanelor și fațetelor provizorii) cu ajutorul căreia am hotărât formele și dimensiunile potrivite.</li>
                    <li>Realizarea și cimentarea coronițelor și a fațetelor finale.</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sorina-copaci.jpg') ?>" alt="Sorina Copaci"></div>
                    <div class="col-md-9">Sorina Copaci</div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="impressions">
        <div class="container container-960">
            <video class="clearfix" controls poster="<?php echo media_url('cazuri/erika/video.jpg'); ?>">
                <source src="<?php echo media_url("cazuri/erika/interviu.mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
    </div>
</div>