<div class="tab-pane fade show has-cover-img" id="dan" role="tabpanel">
    <div class="person">
        <div class="container container-960">
            <div class="row">
                <h2 class="mb-0" data-aos="fade-down" data-aos-delay="50">Dan</h2>
                <!-- <p>Replace this text</p> -->
                <img src="<?php echo media_url("cazuri/dan/1.jpg"); ?>" alt="Dan si Dr. Paul Zaharia">
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row no-gutter">
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/dan/2.jpg') ?>" alt="Inainte">
                <p>Înainte</p>
            </div>
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/dan/3.jpg') ?>" alt="Dupa">
                <p>După</p>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Dan a venit adresând probleme minore legate de sensibilitatea dinților care erau foarte abrazati și probleme parodontale cronice localizate. Abraziunea era din cauza unui bruxism intens iar problemele parodontale au apărut din cauza lipsei mai multor dinți.</p>
                <p>La consultația initiala a fost prezentată situatia generala nefavorabila si riscurile neinceperii tratamentelor: pierderea rapida a suportului osos si gingival la dinți care mai exista.</p>
                <p>Pacientul nu a fost convins de necesitate intervenției imediate si a preferat sa mai “astepte”. După o perioada de pauza de cateva luni s-a reîntors în clinică datorită sensibilității apărute la dinții frontali - aceștia aveau un grad mare de uzură, încât s-a ajuns la nerv și erau sensibili la cele mai mici diferențe de temperatură.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Rezolvarea imediată a cariilor, a sensibilităților apărute și stabilizarea parodontală.</li>
                    <li>Tratarea dinților restanți (tratamente endodontice, reconstituiri cu pivoți din fibră de sticlă), realizarea de lucrări protetice provizorii pentru a testa funcțional și estetic noul raport dintre mandibulă și maxilar prin aplicarea lucrărilor provizorii.</li>
                    <li>Realizarea lucrărilor finale de Zirconia Monolithic (efect estetic satisfăcător, compatibilitate 100%, rezistență crescută).</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sergiu-buzatu.jpg') ?>" alt="Sergiu Buzatu"></div>
                    <div class="col-md-9">Sergiu Buzatu</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-md-9">Paul Zaharia</div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="impressions">
        <div class="container container-960">
            <video class="clearfix" controls poster="<?php echo media_url('cazuri/dan/video.jpg'); ?>">
                <source src="<?php echo media_url("cazuri/dan/interviu.mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
    </div>
</div>