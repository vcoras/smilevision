<div class="tab-pane fade has-img-left show" id="olimpia" role="tabpanel">
    <div class="person">
        <div class="container">
            <div class="row no-gutter align-items-end">
                <div class="col-lg-5">
                    <img class="person-image" data-aos="fade-right" data-aos-delay="0" src="<?php echo media_url('cazuri/olimpia/olimpia.jpg') ?>" alt="Olimpia">
                </div>
                <div class="col-lg-7">
                    <div class="text">
                        <h2 data-aos="fade-down" data-aos-delay="50">Olimpia</h2>
                        <p data-aos="fade-down" data-aos-delay="100">
                            Îmi place să mă bucur de viață, să râd și să iau tot ce vine cu optimism. Sunt un om vesel.
                            <br><br>
                            Din cauza unui tratament necorespunzător realizat cu mult timp în urmă și care nu evolua bine, nu mă simțeam deloc confortabil cu mine, cu dantura mea și eram îngrijorată. Am avut curajul să verific, să cer o părere, să nu ignor problemele și disconfortul ce îl aveam. În sfârșit cineva care are curajul de a găsi o soluție, sigur pe ce spune, rapid... M-am bucurat că pot să rezolv imediat. Mi-au explicat toate riscurile. Primul implant, prima experiență... încredere totală. Am știut că sunt pe mâini bune și că pot să am încredere că se face ce trebuie. 
                            <br><br>
                            S-a vindecat totul foarte frumos, fără durere, umflături sau alte probleme.</p>
                            <br><br>
                            Vin la voi pentru că am încredere. Sănătatea e totul pentru mine. Sunteți implicați, dedicați, sunteți profesioniști și vă faceți meseria cu drag... este altceva... mă simt în siguranță.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row gallery">
            <div class="main-image"><img data-aos="fade-down" data-aos-delay="0" src="<?php echo media_url('cazuri/olimpia/1.jpg') ?>" alt="" class="main-image"></div>
            <div class="row no-gutter">
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/olimpia/2.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/olimpia/3.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="150" src="<?php echo media_url('cazuri/olimpia/4.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="200" src="<?php echo media_url('cazuri/olimpia/5.jpg') ?>" alt=""></div>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Din punct de vedere estetic Olimpia nu era mulțumită în totalitate de dantura ei. Pe dintele lateral era aplicată o coroană ce se dorea înlocuită. La îndepărtarea coroanei vechi, dintele era foarte distrus și nu se mai putea salva, era necesară extragerea dintelui și inserarea unui implant.</p>
                <p>Pentru cineva vesel care se bucură și zâmbește mult, provocarea a constat în păstrarea danturii intacte (fara dinți lipsă) pe durata vindecării și a osointegrării implantului.</p>
                <p>Într-o singură ședință s-a realizat intervenția de inserare a implantului și aplicarea unei lucrări provizorii. La 2 luni s-a aplicat lucrarea finală.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Simularea zâmbetului folosind tehnica MOCK-UP realizată în clinică, pentru experiența reală a rezultatului final.</li>
                    <li>Ablație coroană, extracție, inserare implant, grefă țesut conjunctiv, coroană provizorie</li>
                    <li>Aplicarea lucrării finale la 2 luni constând din o coroană ceramică pe bont individual de zirconiu</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/mario-chilom.jpg') ?>" alt="Mario Chilom"></div>
                    <div class="col-md-9">Mario Chilom</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-md-9">Paul Zaharia</div>
                </div>
            </div>
        </div>
        <!-- <div class="row impressions">
            <h2>Impresii dupa interventie</h2>
        </div> -->
    </div>
</div>