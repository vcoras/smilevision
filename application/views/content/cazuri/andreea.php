<div class="tab-pane fade show has-img-right" id="andreea" role="tabpanel">
    <div class="person">
        <div class="container container-960">
            <div class="row no-gutter align-items-end">
                <div class="col-lg-5">
                    <div class="text">
                        <h2 data-aos="fade-down" data-aos-delay="50">Andreea</h2>
                        <p data-aos="fade-down" data-aos-delay="100">
                            Toate complexele pe care le aveam înainte din cauză că dinții mei nu erau drepți, au dispărut.
                            <br><br>
                            Nu îmi mai fac probleme în momentul în care trebuie să zâmbesc, mă uit cu încredere în oglindă și mă bucur să văd că am zâmbetul la care am visat dintotdeauna.
                            <br><br>
                            Medicii de aici și experiența avută pe parcursul celor 5 ani de zile este cu totul diferită.
                            <br><br>
                            A fost foarte multă deschidere, sinceritate și încredere care m-au determinat să vin aici și să îmi doresc să vin în continuare.
                        </p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <img class="person-image" data-aos="fade-right" data-aos-delay="0" src="<?php echo media_url('cazuri/andreea/1.jpg') ?>" alt="Andreea">
                </div>
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row no-gutter">
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/andreea/2.jpg') ?>" alt="Inainte">
                <p>Înainte</p>
            </div>
            <div class="col-md-6">
                <img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/andreea/3.jpg') ?>" alt="Dupa">
                <p>După</p>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Din dorința de a avea zâmbetul mult dorit, Andreea a purtat o perioadă îndelungată aparat dentar. Pe durata purtării aparatului dentar s-a realizat re-tratatrea dinților afectați cu tratamente endodontice și s-au înlocuit obturațiile vechi necorespunzătoare dpdv estetic.</p>
                <p>După îndepărtarea aparatului dentar, Andreea nu era 100% mulțumită de zâmbetul ei.</p>
                <p>Aplicarea de fațete dentare era singura opțiune pentru a avea zâmbetul mult dorit, la care visase mereu.</p>
                <p>Am analizat împreună ce și-ar dori și cum va arăta viitorul ei zâmbet.</p>
                <p>Pornind pe un fond sănătos, după tratamentul gingiilor pentru stoparea evoluției bolii paradontale, s-a realizat o simulare (mock-up) cu dinții finali.</p>
                <p>După o perioadă de câteva săptămâni s-au realizat și aplicat fațetele finale, integral din ceramică.</p>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Igienizare - testare pentru germeni parodontali - tratament parodontal</li>
                    <li>Retratarea dintilor și înlocuirea obturațiilor</li>
                    <li>Îndepărtarea aparatului ortodontic</li>
                    <li>Tratarea gingiilor pentru stoparea evoluției bolii parodontale apărută în urma tratamentului ortodontic</li>
                    <li>Analiza și simularea zâmbetului folosind tehnica WAX-UP realizată de tehnician și MOCK-UP realizată în clinică, pentru experiența reală a rezultatului final</li>
                    <li>Prepararea dinților în vederea realizării și cimentării fațetelor de ceramică</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/mario-chilom.jpg') ?>" alt="Mario Chilom"></div>
                    <div class="col-md-9">Mario Chilom</div>
                </div>
            </div>
        </div>
    </div>
    <div class="impressions">
        <div class="container container-960">
            <video class="clearfix" controls poster="<?php echo media_url('cazuri/andreea/video.jpg'); ?>">
                <source src="<?php echo media_url("cazuri/andreea/interviu.mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
    </div>
</div>