<div class="tab-pane fade has-img-left show" id="madalina" role="tabpanel">
    <div class="person">
        <div class="container">
            <div class="row no-gutter align-items-end">
                <div class="col-lg-5">
                    <img class="person-image" data-aos="fade-right" data-aos-delay="0" src="<?php echo media_url('cazuri/madalina/madalina.jpg') ?>" alt="Madalina">
                </div>
                <div class="col-lg-7">
                    <div class="text">
                        <h2 data-aos="fade-down" data-aos-delay="50">Mădălina</h2>
                        <p data-aos="fade-down" data-aos-delay="100">
                            Sunt Mădălina, am 26 de ani, iar ca orice femeie m-am concentrat și pe aspectul fizic, dantura fiind parte importantă pentru mine, chiar o carte de vizită.
                            <br><br>
                            Nu aș putea să spun că am avut o dantură groaznică, mai degrabă problematică, suficient cât să mă facă să nu zâmbesc natural.
                            <br><br>
                            De aici am acumulat anumite frustrări, indiferent de cât de sigură eram pe mine, mereu simțeam că mai e nevoie de ceva.
                            <br><br>
                            Așa am ajuns la Smile Vision, și chiar pot să spun că viața mea s-a schimbat. Paul și Sergiu au înțeles perfect ce îmi doresc, iar în 7 luni zâmbetul meu a devenit liber.
                            <br><br>
                            Dacă până de curând l-am ascuns, acum abia mă oprești din a zâmbi.
                            <br><br>
                            Vă mulțumesc oameni frumoși!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info container">
        <div class="row gallery">
            <div class="main-image"><img data-aos="fade-down" data-aos-delay="0" src="<?php echo media_url('cazuri/madalina/1.jpg') ?>" alt="" class="main-image"></div>
            <div class="row no-gutter">
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="50" src="<?php echo media_url('cazuri/madalina/2.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="100" src="<?php echo media_url('cazuri/madalina/3.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="150" src="<?php echo media_url('cazuri/madalina/4.jpg') ?>" alt=""></div>
                <div class="col-md-3"><img data-aos="fade-right" data-aos-delay="200" src="<?php echo media_url('cazuri/madalina/5.jpg') ?>" alt=""></div>
            </div>
        </div>
        <div class="row no-gutter details">
            <div data-aos="fade-right" data-aos-delay="0" class="col-lg-4 case">
                <h3>Particularitatea cazului</h3>
                <p>Mădalina își dorea îmbunătățirea radicală a zâmbetului, un zâmbet estetic, frumos, fără diastemă (strungă), mai alb, dar în același timp având o culoare și un aspect natural. </p>
                <p>Prima etapă a constat în simularea viitoriilor dinți astfel încât să existe experiența rezultatului final înainte de începerea tratamentelor.</p>
                <p>Provocările acestui caz au constat în:</p>
                <ul>
                    <li>inserarea implantului într-un spațiu limitat datorită faptului că dintele extras (incisivul lateral stâng) era un dinte fără os în jurul lui</li>
                    <li>implementarea a 3 soluții protetice diferite (fațetă/coroană integral ceramică dinte/implant) pe 3 dinți diferiți (dinte natural integru/dinte natural colorat/implant).</li>
                </ul>
            </div>
            <div data-aos="fade-up" data-aos-delay="0" class="col-lg-4 approach">
                <h3>Abordare</h3>
                <h4>Etapele</h4>
                <ul>
                    <li>Simularea zâmbetului folosind tehnica WAX-UP realizată de tehnician și MOCK-UP realizată în clinică, pentru experiența reală a rezultatului final.</li>
                    <li>Ablație coroană, extracție, inserare implant, grefă țesut conjunctiv, coroană provizorie</li>
                    <li>Aplicarea lucrării finale la 2 luni constând în 4 fațete și 2 coroane.</li>
                </ul>
            </div>
            <div data-aos="fade-left" data-aos-delay="0" class="col-lg-4 team">
                <h3>Echipa</h3>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/sergiu-buzatu.jpg') ?>" alt="Sergiu Buzatu"></div>
                    <div class="col-md-9">Sergiu Buzatu</div>
                </div>
                <div class="row no-gutter align-items-end">
                    <div class="col-md-3"><img src="<?php echo media_url('cazuri/medici/paul-zaharia.jpg') ?>" alt="Paul Zaharia"></div>
                    <div class="col-md-9">Paul Zaharia</div>
                </div>
            </div>
        </div>
        <!-- <div class="row impressions">
            <h2>Impresii dupa interventie</h2>
        </div> -->
    </div>
</div>