<section class="section-1">
    <img src="<?php echo media_url("laborator/header.jpg"); ?>" alt="Heading image">
    <div class="text-wrapper">
        <h1 data-aos="fade-right" data-aos-anchor-placement="top-bottom">
            Laborator <br> 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de tehnică dentară</h1>
        <p data-aos="fade-left">în Timișoara</p>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <p data-aos="fade-up"><strong>Aveți nevoie de <a href="<?php echo base_url($this->language['url_key'] . "/servicii/implantologie"); ?>">implanturi dentare</a>, coroane, proteze sau punți?</strong></p>
        <p data-aos="fade-up">Clinica stomatologică Smile Vision are propriul laborator de tehnică dentară. Prin urmare, controlăm întregul tratament, lucru care se reflectă în eficientizarea costurilor pentru pacienți și a timpului execuției procedurilor. </p>
        <p data-aos="fade-up">Având laboratorul dentar chiar în interiorul clinicii, suntem bucuroși să vă oferim intervenții confortabile, rapide și soluții estetice eficiente.</p>
        <div data-aos="fade-up" class="row video">
            <video controls poster="<?php echo media_url('laborator/video.png'); ?>">
                <source src="<?php echo media_url("laborator/laborator-". $this->language["url_key"] .".mp4") ?>" type="video/mp4">
                Nu putem reda acest video in browser-ul dumneavoastra.
            </video>
        </div>
        <br><br>
        <h2 data-aos="fade-up">Avantajele unui laborator de tehnică dentară propriu</h2>
        <p data-aos="fade-up"><strong>Planificarea unei restaurări protetice, executarea designului și realizarea propriu-zisă a lucrării au loc toate în clinica Smile Vision</strong> confortul fiind maxim, costurile reduse, iar durata intervenției minimă.</p>
        <p data-aos="fade-up">Pentru a obține lucrări protetice cu aspect plăcut, natural, utilizăm tehnologii și tehnici moderne, ce asigură calitate ridicată și maximă precizie la orice intervenție. În plus, colectivul nostru de tehnicieni dentari <strong>execută toate tipurile de lucrări, de la cele mai simple până la cele mai complexe.</strong></p>
        <p data-aos="fade-up">Fiecare pacient beneficiază de o experiență personalizată, completă, alegând cele mai bune soluții de tratament în funcție de nevoile și dorințele acestuia. Rezultatele mult visate se obțin în cel mai scurt timp și, atât timp cât pacientul îngrijește lucrarea conform recomandărilor specialiștilor, aceasta va trece cu brio testul timpului.</p>
    </div>
</section>
<div class="title-ribbon" data-aos-delay="0" data-aos="zoom-in">
    <div class="container">
        <h2 data-aos-delay="200" data-aos="fade-right">Facilități oferite de laboratorul de tehnică dentară Smile Vision</h2>
    </div>
</div>
<section class="section-3">
    <div class="container">
        <h2 data-aos="fade-up">Tehnicieni dentari bine pregătiți</h2>
        <p data-aos="fade-up">Echipa noastră de tehnicieni dentari a fost atent aleasă dintre cei mai buni experți și beneficiază de specializare și formare profesională continuă. Tehnicienii lucrează cu medicii stomatologi, identificând împreună soluțiile potrivite pentru pacienți, atât la nivel medical, cât și estetic.</p>
        <br>
        <div class="row photos no-gutter">
            <div class="col d-lg-flex justify-content-around">
                <img data-aos="fade-right" class="flex-fill " src="<?php echo media_url("laborator/tehnicieni-1.jpg"); ?>" alt="Tehnicieni 1" />
                <img data-aos="fade-left" class="flex-fill" src="<?php echo media_url("laborator/tehnicieni-2.jpg"); ?>" alt="Tehnicieni 2" />
            </div>
        </div>
        <br><br>
        <h2 data-aos="fade-up">Sistemul CAD/CAM</h2>
        <p data-aos="fade-up">Tehnologia CAD/CAM este folosită în tehnica dentară pentru realizarea pieselor protetice conform cerințelor medicului stomatolog și pacientului. Sistemul este compus din:</p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">scanner intra-oral</li>
            <li data-aos-delay="100" data-aos="fade-left">calculator cu soft de imagine 3D</li>
            <li data-aos-delay="200" data-aos="fade-left">unitate de frezare cu freze diamantate pentru a transforma rapid și precis datele în produsul finit</li>
        </ul>
        <br>
        <div class="row photos d-flex no-gutter clearfix">
            <div class="col d-lg-flex justify-content-around">
                <img data-aos="fade-right" class="flex-fill " src="<?php echo media_url("laborator/CAD-1.jpg"); ?>" alt="CAD 1" />
                <img data-aos="fade-left" class="flex-fill" src="<?php echo media_url("laborator/CAD-2.jpg"); ?>" alt="CAD 2" />
            </div>
        </div>
        <br>
        <p data-aos="fade-up">Principalele avantaje ale acestei tehnologii sunt timpul scurt în care restaurarea poate fi realizată, precum și rezultatul estetic. Prin urmare, sistemul CAD/CAM crește eficiența tratamentului stomatologic și este de mare ajutor în standardizarea restaurărilor protetice, dar și în dezvoltarea unor noi concepte de tratament și a unor noi grupe de materiale.</p>
        <br>
        <h2 data-aos="fade-up">Imprimantă 3D</h2>
        <p data-aos="fade-up">Pentru fabricarea restaurărilor dentare folosim imprimanta 3D cu un grad crescut de precizie și eficiență. Piesele construite cu ajutorul acestui aparat sunt de înaltă acuratețe și de o calitate excelentă. Mai mult, versatilitatea imprimantei permite procesarea unei game variate de materiale și aplicații.</p>
        <br>
        <div class="row photos no-gutter">
            <div class="col d-lg-flex justify-content-around">
                <img data-aos="fade-right" class="flex-fill " src="<?php echo media_url("laborator/3D-1.jpg"); ?>" alt="3D 1" />
                <img data-aos="fade-left"  class="flex-fill" src="<?php echo media_url("laborator/3D-2.jpg"); ?>" alt="3D 2" />
            </div>
        </div>
    </div>
</section>
<section class="section-4">
    <div class="container">
        <h2 data-aos="fade-up">Lucrări protetice extrem de precise realizate în laboratorul de tehnică dentară Smile Vision</h2>
        <p data-aos="fade-up">Lucrările de <a href="<?php echo base_url($this->language['url_key'] . "/servicii/protetica"); ?>">protetică dentară</a> au rolul de a restabili integritatea danturii afectate de degradare sau leziuni. Aceste intervenții se realizează cu ajutorul coroanelor, protezelor, punților dentare, <a href="<?php echo base_url($this->language['url_key'] . "/servicii/fatete-dentare"); ?>">fațetelor</a> sau a încrustațiilor dentare.</p>
        <p data-aos="fade-up">Dacă ai întrebări referitoare la serviciile oferite de clinica stomatologică Smile Vision și a laboratorului nostru de tehnică dentară din Timișoara, contactează-ne, iar echipa noastră te va ajuta cu drag.</p>
        <img data-aos="fade-up" class="d-block mx-auto" src="<?php echo media_url("laborator/lucrari.jpg"); ?>" alt="Lucrari protetice">
    </div>
</section>
<section class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9"><p>Vino la o consultație și află mai multe despre sănătatea dinților tăi!</p></div>
            <div class="col-md-3">
                <a class="js-open-form-modal" href="<?php echo base_url($this->language['url_key'] . '/contact'); ?>">Vreau o programare</a>
            </div>
        </div>
    </div>
</section>