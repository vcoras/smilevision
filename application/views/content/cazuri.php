<section class="section-1">
    <div class="container container-960">
        <h1>Cazuri</h1>
        <p>Descoperă povestea din spatele zâmbetelor pacienților noștri și află mai multe despre tratamentele realizate și experiența lor.</p>
        <p>O călătorie personală de la grijă, emoție, la sănătate, încredere, expresie de sine și frumos. Le mulțumim pentru deschiderea și curajul de a face disponibilă povestea lor și ȚIE.</p>
        <div class="nav-list-heading">
            <div class="row no-gutter" role="tablist">
                <div data-aos="fade-down" data-aos-delay="0" class="tab-list-item col-12 col-sm-6 col-md-4 current">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#alexandra">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-1.jpg") ?>" alt="Alexandra">
                        </div>
                        <p>
                            Alexandra
                            <span class="spot">&nbsp;</span>
                        </p>
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="50" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#madalina">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-2.jpg") ?>" alt="Mădălina">
                        </div>
                        <p>
                            Mădălina
                            <span class="spot">&nbsp;</span>
                        </p>    
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="100" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#olimpia">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-3.jpg") ?>" alt="Olimpia">
                        </div>
                        <p>
                            Olimpia
                            <span class="spot">&nbsp;</span>
                        </p>
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="150" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#marius">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-4.jpg") ?>" alt="Marius">
                        </div>
                        <p>
                            Marius
                            <span class="spot">&nbsp;</span>

                        </p>
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="200" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#loredana">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-5.jpg") ?>" alt="Loredana">
                        </div>
                        <p>
                            Loredana
                            <span class="spot">&nbsp;</span>
                        </p>
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="250" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#andreea">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-6.jpg") ?>" alt="Andreea">
                        </div>
                        <p>
                            Andreea
                            <span class="spot">&nbsp;</span>
                        </p>
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="300" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#erika">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-7.jpg") ?>" alt="Erika">
                        </div>
                        <p>
                            Erika
                            <span class="spot">&nbsp;</span>
                        </p>    
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="350" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#dan">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-8.jpg") ?>" alt="Dan">
                        </div>
                        <p>
                            Dan
                            <span class="spot">&nbsp;</span>
                        </p>
                    </a>
                </div>
                <div data-aos="fade-down" data-aos-delay="400" class="tab-list-item col-12 col-sm-6 col-md-4">
                    <a class="tab-link" data-toggle="tab" role="tab" href="#iva">
                        <div class="image">
                            <img src="<?php echo media_url("cazuri/thumb-caz-9.jpg") ?>" alt="Iva">
                        </div>
                        <p>
                            Iva
                            <span class="spot">&nbsp;</span>
                        </p>
                    </a>
                </div>
            </ul>
        </div>
    </div>
</section>
<section class="section-2 tabs">
    <div class="tab-content">
        <?php $this->load->view("content/cazuri/alexandra"); ?>
        <?php $this->load->view("content/cazuri/madalina"); ?>
        <?php $this->load->view("content/cazuri/olimpia"); ?>
        <?php $this->load->view("content/cazuri/marius"); ?>
        <?php $this->load->view("content/cazuri/loredana"); ?>
        <?php $this->load->view("content/cazuri/andreea"); ?>
        <?php $this->load->view("content/cazuri/erika"); ?>
        <?php $this->load->view("content/cazuri/dan"); ?>
        <?php $this->load->view("content/cazuri/iva"); ?>
    </div>
</section>
<section class="section-3 controls">
    <div class="container container-960">
        <div class="row no-gutter align-items-center" role="tablist">
            <div class="col-md-2 image js-last-image"><img src="<?php echo media_url("cazuri/thumb-caz-9.jpg") ?>" alt="Iva"></div>
            <div class="col-md-4">
                <a href="#iva" data-toggle="tab" role="tab" class="tab-link js-last-case">Cazul anterior</a>
            </div>
            <div class="col-md-4 text-right">
                <a href="#madalina" data-toggle="tab" role="tab" class="tab-link js-next-case">Cazul următor</a>
            </div>
            <div class="col-md-2 image js-next-image"><img src="<?php echo media_url("cazuri/thumb-caz-2.jpg") ?>" alt="Mădălina"></div>
        </div>
    </div>
</section>