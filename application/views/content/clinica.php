<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Clinica</h1>
        
        <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Vino să ne cunoști</h2>
        <p data-aos="fade-left">Pasiunea pentru detaliu este ce ne definește. Așa că nu facem rabat la nimic. Suntem mereu prezenți la conferințe, cursuri și specializări în domeniu, investim constant în pregătirea noastră și ne punem la punct cu ultimele descoperiri în domeniu pentru a oferii pacienților noștri tehnici noi în tratamentele pe care le realizăm și tehnologii care ne oferă rigurozitate și calitate.</p>
        <p data-aos="fade-left">Îți oferim un întreg spectru de servicii stomatologice: stomatologie generală, reabilitări orale complexe, implantologie, estetică dentară, ortodonție, endodonție asistată microscopic, stomatologie pentru copii.</p>
        <p data-aos="fade-left">Folosim cele mai noi tehnologii: DSD, scanner intraoral 3DShape, microscop Leica, așa că îți putem arăta etapele tratamentelor realizate, și chiar rezultatul final încă înainte de a începe, pentru ca în orice moment tu să știi ce urmează. Astfel și tu participi la alegerea celei mai potrivite soluții pentru tine.</p>
        <p data-aos="fade-left">Beneficiem de propriul laborator digital, ceea ce ne oferă flexibilitate, vizite mai puține și mai scurte pentru tine, timp redus pentru lucrările protetice, totul datorită designului și a frezării asistate de computer. De asemenea toate investigațiile radiologice sunt digitale, cu radiații minime și se realizează direct în clinică, iar tu nu mai parcurgi orașul către alte centre de imagistică.</p>
        <p data-aos="fade-left">Din pasiune începe totul. Așa am apărut noi. Și suntem cât se poate de reali, de activi și dornici de cunoaștere. Pentru ca zâmbetul tău să fie oferit din plin celor din jurul tău.</p>
        <p data-aos="fade-left">Vino să ne cunoști, suntem aici pentru tine și dorim să ne ocupăm și de zâmbetul tău.</p>
        
        <div class="row gallery no-gutter">
            <div class="col-md-6" data-aos="fade-down" data-aos-delay="0">
                <div class="inner">
                    <img src="<?php echo media_url('clinica/gallery-1.png') ?>" alt="Clinica 1">
                </div>
            </div>
            <div class="col-md-6" data-aos="fade-down" data-aos-delay="50">
                <div class="inner">
                    <img src="<?php echo media_url('clinica/gallery-2.png') ?>" alt="Clinica 2">
                </div>
            </div>
            <div class="col-md-6" data-aos="fade-down" data-aos-delay="100">
                <div class="inner">
                    <img src="<?php echo media_url('clinica/gallery-3.png') ?>" alt="Clinica 3">
                </div>
            </div>
            <div class="col-md-6" data-aos="fade-down" data-aos-delay="150">
                <div class="inner">
                    <img src="<?php echo media_url('clinica/gallery-4.png') ?>" alt="Clinica 4">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <h2 data-aos="fade-left">Echipa</h2>
        <p data-aos="fade-left">Suntem o echipă multidisciplinară, fiecare fiind expert pe partea lui, pasionați de excelență în ceea ce facem. Doar împreună reușim să îți redăm starea generală de bine, oferindu-ți șansa la un zâmbet sănătos și frumos, iar asta nu poate însemna decât satisfacție și împlinire pentru noi ca medici iar pentru tine LINIȘTE, SIGURANȚĂ, CONFORT și un ZÂMBET extraordinar.</p>
    </div>
    <div class="container medics">
        <div class="row no-gutter">
            <div class="col-md-4 align-items-stretch" data-aos="fade-down" data-aos-delay="0">
                <div class="card">
                    <img src="<?php echo media_url('clinica/paul-zaharia.jpg'); ?>" alt="Paul Zaharia" class="card-img-top">
                    <div class="card-body p-0">
                        <h3 class="card-title">Paul Zaharia</h3>
                        <h4 class="card-subtitle">Medic stomatolog</h4>
                        <p class="card-text">
                            Experiența mi-a arătat că dincolo de medic este nevoie să fii în primul rând om.
                            <br>
                            Mă pun în locul pacienților mei de fiecare dată, ascult, explic, mă perfecționez pentru că am o enormă responsabilitate: sănătatea pacientului meu. Viața lui se schimbă atunci când e mulțumit de tratamentele realizate, zâmbește liber și nu e constrâns de nimic în a se exprima.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 align-items-stretch" data-aos="fade-down" data-aos-delay="50">
                <div class="card">
                    <img src="<?php echo media_url('clinica/mario-chilom.jpg'); ?>" alt="Mario Chilom" class="card-img-top">
                    <div class="card-body p-0">
                        <h3 class="card-title">Mario Chilom</h3>
                        <h4 class="card-subtitle">Medic stomatolog</h4>
                        <p class="card-text">
                            Poți să faci multe lucruri, diferența stă în cum le faci. Cu multă răbdare și precizie mă dedic fiecărei lucrări în parte. Corectitudinea, sinceritatea sunt principiile după care mă ghidez, atât personal cât și profesional. Îmi petrec weekendurile vara la pescuit și făcând scufundări.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 align-items-stretch" data-aos="fade-down" data-aos-delay="100">
                <div class="card">
                    <img src="<?php echo media_url('clinica/sergiu-buzatu.jpg'); ?>" alt="Sergiu Buzatu" class="card-img-top">
                    <div class="card-body p-0">
                        <h3 class="card-title">Sergiu Buzatu</h3>
                        <h4 class="card-subtitle">Medic stomatolog</h4>
                        <p class="card-text">
                            Privesc meseria mea ca pe o artă prin care transpun imaginația și dorința fiecărui pacient într-un lucru palpabil de care se poate bucura în orice moment. Am ales această meserie pentru a face oamenii fericiți, a le oferi o încredere mai mare în sine și un mod de viață sănătos, toate acestea printr-un zâmbet larg și o dantură sănătoasă. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutter align-items-end" data-aos="fade-down" data-aos-delay="150">
            <div class="col-md-6">
                <div class="row no-gutter align-items-end">
                    <div class="col-md-6">
                        <img class="side-image" src="<?php echo media_url('clinica/sorina-copaci.jpg'); ?>" alt="Sorina Copaci">
                    </div>
                    <div class="col-md-6">
                        <h3 class="card-title">Sorina Copaci</h3>
                        <h4 class="card-subtitle">Medic stomatolog</h4>
                        <p class="card-text">
                            Îmi place să dau din calmul și veselia mea fiecărui pacient, pentru ca atmosfera să fie una destinsă, prietenoasă.
                            <br>
                            Suntem oameni, vrem să ne simțim bine oriunde mergem, inclusiv la stomatolog.
                            <br>
                            Mă încarc cu energie din treaba bine făcută și îmi iau liniștea fugind în weekend ... oriunde.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row no-gutter align-items-end">
                    <div class="col-md-6">
                        <img class="side-image" src="<?php echo media_url('clinica/alexandra-majorosi.jpg'); ?>" alt="Alexanda Majorosi">
                    </div>
                    <div class="col-md-6">
                        <h3 class="card-title">Alexanda Majorosi</h3>
                        <h4 class="card-subtitle">Medic stomatolog</h4>
                        <p class="card-text">
                            Am muncit mult ca să pot să ajung medic stomatolog, pentru că mi-am dorit să fac bine oamenilor. Cred în blândețe, înțelegere, corectitudine. Sunt o romantică. Ador cărțile, călătoriile, și îmi place vara.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-3" id="certificari">
    <div class="container">
        <h2 data-aos="fade-left">Certificări</h2>
        <div class="row no-gutter align-items-end mb-5" data-aos="fade-down" data-aos-delay="150">
            <div class="col-md-3">
                <a href="<?php echo media_url('clinica/diplome/zoom/00.jpg'); ?>" data-fancybox="gallery">
                    <picture>
                        <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/00.jpg'); ?>">
                        <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/00.jpg'); ?>">
                        <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/00.jpg'); ?>" alt="Diploma 1">
                    </picture>
                </a>
            </div>
            <div class="col-md-9">
                <p class="card-text">Pentru desăvârșirea profesionalismului și servicii de cel mai înalt nivel, participăm constant la cursuri de perfecționare.</p>
                <p class="card-text">Iată câteva dintre ele.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row no-gutter">
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="0">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/01.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/01.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/01.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/01.jpg'); ?>" alt="Diploma 2">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="50">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/02.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/02.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/02.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/02.jpg'); ?>" alt="Diploma 3">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="100">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/03.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/03.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/03.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/03.jpg'); ?>" alt="Diploma 4">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="150">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/04.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/04.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/04.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/04.jpg'); ?>" alt="Diploma 5">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="200">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/05.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/05.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/05.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/05.jpg'); ?>" alt="Diploma 6">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="250">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/06.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/06.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/06.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/06.jpg'); ?>" alt="Diploma 7">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="0">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/07.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/07.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/07.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/07.jpg'); ?>" alt="Diploma 8">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="50">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/08.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/08.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/08.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/08.jpg'); ?>" alt="Diploma 9">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="100">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/09.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/09.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/09.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/09.jpg'); ?>" alt="Diploma 10">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="150">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/10.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/10.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/10.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/10.jpg'); ?>" alt="Diploma 11">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="200">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/11.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/11.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/11.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/11.jpg'); ?>" alt="Diploma 12">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="250">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/12.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/12.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/12.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/12.jpg'); ?>" alt="Diploma 13">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="0">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/13.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/13.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/13.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/13.jpg'); ?>" alt="Diploma 14">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="50">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/14.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/14.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/14.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/14.jpg'); ?>" alt="Diploma 15">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="100">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/15.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/15.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/15.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/15.jpg'); ?>" alt="Diploma 16">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="150">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/16.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/16.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/16.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/16.jpg'); ?>" alt="Diploma 17">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="200">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/17.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/17.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/17.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/17.jpg'); ?>" alt="Diploma 18">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="250">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/18.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/18.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/18.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/18.jpg'); ?>" alt="Diploma 19">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="0">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/19.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/19.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/19.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/19.jpg'); ?>" alt="Diploma 20">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="50">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/20.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/20.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/20.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/20.jpg'); ?>" alt="Diploma 21">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="100">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/21.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/21.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/21.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/21.jpg'); ?>" alt="Diploma 22">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="150">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/22.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/22.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/22.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/22.jpg'); ?>" alt="Diploma 23">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="200">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/23.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/23.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/23.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/23.jpg'); ?>" alt="Diploma 24">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="col-md-2" data-aos="fade-down" data-aos-delay="250">
                <div class="inner">
                    <a href="<?php echo media_url('clinica/diplome/zoom/24.jpg'); ?>" data-fancybox="gallery">
                        <picture>
                            <source media="(max-width:768px)" srcset="<?php echo media_url('clinica/diplome/zoom/24.jpg'); ?>">
                            <source media="(min-width:769px)" srcset="<?php echo media_url('clinica/diplome/thumbs/24.jpg'); ?>">
                            <img class="side-image" src="<?php echo media_url('clinica/diplome/thumbs/24.jpg'); ?>" alt="Diploma 25">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>