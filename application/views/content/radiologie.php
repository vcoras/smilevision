<section class="section-1">
    <img src="<?php echo media_url("radiologie/header.jpg"); ?>" alt="Heading image">
    <div class="text-wrapper">
        <h1 data-aos="fade-right" data-aos-anchor-placement="top-bottom">
            Radiologie digitală</h1>
        <p data-aos="fade-left">
            &nbsp;&nbsp;&nbsp;Radiografie și <br>
            &nbsp;&nbsp;&nbsp;Imagistică Dentară
        </p>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <p data-aos="fade-up">Radiografiile dentare sunt imagini ale dinților pe care le folosim pentru a evalua sănătatea orală a pacienților și pentru a putea stabili un diagnostic corect și un plan de tratament complet.</p>
        <p data-aos="fade-up">Centrul Stomatologic Smile Vision este dotat cu cea mai avansată tehnologie de imagistică dentară care reduce riscul de radiație al pacienților și oferă un rezultat clar și sigur, în cel mai scurt timp.</p>
        <p data-aos="fade-up">Este important de știut că toate investigațiile radiologice se pot realiza direct în clinica noastră, nefiind necesară deplasarea către alte centre de imagistică.</p>
        <br><br>
        <h2 data-aos="fade-up">Ce este radiologia?</h2>
        <p data-aos="fade-up">Radiologia dentară este specialitatea care folosește imagistica dentară cu scopul de a diagnostica diferite afecțiuni stomatologice. Radiografia este o tehnică imagistică a radiologiei, reprezentată sub forma unei imagini plane și tridimensionale a dinților, maxilarelor, țesuturilor moi și a oaselor în urma expunerii la radiațiile X ori a scanării digitale (radiologie digitală).</p>
        <p data-aos="fade-up">Efectuarea radiografiei este o etapă care ajută medicul stomatolog să stabilească un diagnostic precis, deoarece procedura arată detalii în funcție de care specialistul poate determina natura unei intervenții asupra dinților și chiar recomanda un plan de tratament.</p>
        <p data-aos="fade-up">Radiografia dentară are un rol dublu. Pe de-o parte, aceasta răspunde întrebărilor pe care medicii dentiști le au atunci când o simplă consultație nu scoate la iveală toate problemele pacientului. Pe de altă parte, radiografia este un factor esențial de prevenție, deoarece ea semnalează din timp afecțiuni ce necesită tratate.</p>
    </div>
</section>
<div class="title-ribbon" data-aos-delay="0" data-aos="zoom-in">
    <div class="container">
        <h2 data-aos-delay="200" data-aos="fade-right">Tipuri de radiografii pe care le realizăm</h2>
    </div>
</div>
<section class="section-3">
    <div class="container">
        <h2 data-aos="fade-up">Radiografie dentară 3D</h2>
        <p data-aos="fade-up">Radiografia dentară 3D este o metodă non-invazivă și nedureroasă de a capta mai multe detalii din zone greu accesibile ale cavității bucale și de a le exprima într-un mod concis, oferind mult mai multe informații decât o investigație obișnuită. Este o procedură de scurtă durată, ce nu va răpi mult timp, nu afectează cu nimic sănătatea, și care oferă o privire mult mai detaliată asupra danturii și a mesei osoase a pacientului.</p>
        <p data-aos="fade-up">La Smile Vision, oferim mai multe tipuri de radiografie dentară 3D:</p>
        <ul>
            <li data-aos-delay="0" data-aos="fade-left">Radiografie 3D endo</li>
            <li data-aos-delay="100" data-aos="fade-left">Radiografie parțială/arcadă</li>
            <li data-aos-delay="100" data-aos="fade-left">Radiografie 3D totală</li>
        </ul>
        <br>
        <img data-aos="fade-up" class="d-block mx-auto" src="<?php echo media_url("radiologie/radiografie_3D.jpg"); ?>" alt="Lucrari protetice">
        <br>
        <p data-aos="fade-up">Rezultatele acestor radiografii sunt generate în format 3D, informațiile fiind, prin urmare, mai detaliate și mai precise în comparație cu radiografiile clasice (2D).</p>
        <br>
        <h2 data-aos="fade-up">Radiografie retroalveolară</h2>
        <p data-aos="fade-up">Radiografia retroalveolară se numește și periapicală și este o radiografie mică, pe care se văd 2-3 dinți. Scopul său este acela ca medicul stomatolog să vadă vârful rădăcinilor dinților respectivi. În acest fel, specialistul va observa atât dinții, cât și oasele înconjurătoare.</p>
        <p data-aos="fade-up">Acest tip de radiografie poate fi folosită pentru dinții anteriori și posteriori. Cu ajutorul său, dentistul analizează detalii exacte cu privire la afecțiunile dinților sau la eventuale traume anterioare. Radiografia retroalveolară poate fi utilizată pentru a se stabili dacă este nevoie sau nu de un <a href="<?php echo base_url($this->language['url_key'] . "/servicii/endodontie"); ?>">tratament endodontic</a> sau pentru verificarea succesului acestui tratament în urma efectuării obturației de canal.</p>
        <br>
        <img data-aos="fade-up" class="d-block mx-auto" src="<?php echo media_url("radiologie/radiografie_retroalveolara.jpg"); ?>" alt="Lucrari protetice">
        <br>
        <h2 data-aos="fade-up">Radiografie panoramică</h2>
        <p data-aos="fade-up">Radiografiile panoramice arată toți dinții superiori și inferiori, precum și articulația temporo-mandibulară, oasele și sinusurile maxilare. Acestea sunt folosite pentru localizarea unor formațiuni patologice din interiorul maxilarului sau a fracturilor. Importanța lor se datorează, de asemenea, faptului că oferă medicului stomatolog o vedere de ansamblu a cazului.</p>
        <br>
        <img data-aos="fade-up" class="d-block mx-auto" src="<?php echo media_url("radiologie/radiografie_panoramica.jpg"); ?>" alt="Lucrari protetice">
    </div>
</section>
<section class="section-4">
    <div class="container">
        <h2 data-aos="fade-up">Întrebări frecvente despre radiologia dentară</h2>
        <p data-aos="fade-up"><strong>Cine interpretează o radiografie dentară?</strong></p>
        <p data-aos="fade-up">Radiografia dentară este interpretată de medicul stomatolog. Studiind-o cu atenție, dentistul poate identifica structuri ascunse (cum sunt dinții incluși), carii, dar și pierderea osoasă, afecțiuni ce nu pot fi observate cu ochiul liber. Mai mult, radiografiile pot fi de ajutor și după realizarea unui tratament pentru a analiza succesul acestuia.</p>
        <p data-aos="fade-up">Radiografiile arată ca negativele unor fotografii, structurile dense fiind redate cu alb, cavitățile cu negru, iar țesuturile moi cu diverse nuanțe de gri.</p>
        <br>
        <p data-aos="fade-up"><strong>Care este intervalul de timp dintre două radiografii dentare?</strong></p>
        <p data-aos="fade-up">O radiografie retroalveolară expune pacientul la o doză de 1-3 uSV, valoare semnificativ redusă față de doza naturală zilnică de radiații (5-8 uSV) la care suntem expuși, de 8 ori mai mică decât o călătorie cu avionul (aproximativ 25 uSV) și de 16 ori mai mică decât doza generată de locuirea într-o clădire din cărămidă sau beton timp de 1 an (aproximativ 75 uSV). O radiografie panoramică digitală expune pacientul la 4-14 uSV.</p>
        <p data-aos="fade-up">Doza anuală de risc care poate cauza cancerul este de 100.000 uSV, cea care cauzează o stare de vomă este de 2.000.000 uSV, iar cea letală este de 10.000.000 uSV.</p>
        <p data-aos="fade-up">Analizând aceste cifre, reiese că, dacă medicul stomatolog cere realizarea a 5-6 radiografii retroalveolare pe parcursul unui an, acest lucru nu implică niciun risc. La fel, dacă aveți nevoie să efectuați 3-4 radiografii panoramice anual, nu aveți motive de îngrijorare.</p>
        <br>
        <p data-aos="fade-up"><strong>Este periculoasă radiografia dentară?</strong></p>
        <p data-aos="fade-up">Radiografia dentară nu este periculoasă. Radiațiile sunt peste tot, pornind de la razele soarelui, microunde și unde radio, până la alimentele pe care le consumăm zi de zi. Atunci când sunt prezente în cantități mici, aceste tipuri de radiații nu sunt nocive, aici încadrându-se și radiografia dentară.</p>
        <p data-aos="fade-up">Mai mult, în cazul radiografiilor dentare digitale, timpul de expunere este redus, iar cantitatea de radiații considerabil mai mică în comparație cu desfășurarea unor activități precum zborul cu avionul sau prepararea mâncării pe aragaz. Radiografia dentară digitală folosește cu 80% mai puține radiații, timpul de expunere fiind mult mai mic decât în cazul celei clasice.</p>
        <br>
        <p data-aos="fade-up"><strong>Cât este valabilă o radiografie dentară?</strong></p>
        <p data-aos="fade-up">Depinde de la caz la caz, însă în cele mai multe situații, dacă radiografia este mai veche de un an, aceasta nu mai este valabilă. În unele cazuri, chiar și radiografiile mai vechi de 3 luni pot necesita să fie refăcute.</p>
    </div>
</section>
<section class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9"><p>Ai întrebări? Sună sau scrie-ne un e-mail, iar echipa noastră va fi bucuroasă să te ajute.</p></div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/contact'); ?>">Spre pagina de contact</a>
            </div>
        </div>
    </div>
</section>