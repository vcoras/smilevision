<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Cursuri</h1>
        <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Primii pași în Implantologie</h2>
        <p data-aos="fade-left">Este un curs special creat pentru cei care încep să lucreze în domeniul implantologiei sau iși doresc să inteleagă și să stăpânească modul în care orice caz medical poate fii abordat.</p>
        <ol data-aos="fade-left">
            <li><p>Obții cele mai importante informații care se gasesc la baza implantologiei (elementele de logistică, criterii de selecție a pacientului care dorește implanturi dentare, anatomia maxilarelor, tipul implanturilor, etc.).</p></li>
            <li><p>Aflii cum poti sa iei cea mai buna decizie în abordarea cazuisticii și care sunt elementele implicate (planificarea corecta, inserarea precisa, tehnica chirurgicala, tehnica de sutura si vindecare).</p></li>
            <li><p>Înțelegi care sunt factorii care influențeaza vindecarea țesuturilor periimplantare.</p></li>
            <li><p>Alegi cu ușurinta soluția protetică potrivită.</p></li>
            <li><p>PRACTICI folosing modelul de studiu adus cu tine la curs, și APLICI tehnicile învățate folosind materiale oferite de DENTIUM (mandibule, implanturi, cape de vindecare, fire, etc.).</p></li>
        </ol>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <div class="row">
            <div class="col-md-3" data-aos="fade-down" data-aos-delay="0">
                <img src="https://via.placeholder.com/200x113" />
                Video #1
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-delay="50">
                <img src="https://via.placeholder.com/200x113" />
                Video #2
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" data-aos="fade-down" data-aos-delay="100">
                <img src="https://via.placeholder.com/200x113" />
                Video #3
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-delay="150">
                <img src="https://via.placeholder.com/200x113" />
                Video #4
            </div>
        </div>
    </div>
</section>