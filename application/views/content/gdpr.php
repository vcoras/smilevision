<section class="section-1">
    <div class="container">
        <h1>Politica de confidențialitate</h1>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <p>Conform regulamentului (UE) 2016/679 al Parlamentului European și al Consiliului din 27 Aprilie 2016, cu prevederi (GDPR) aplicabile din 25 Mai 2018, privind protecția persoanelor fizice în ceea ce privește prelucrarea datelor cu caracter personal, liberă circulație a acestor date și protecția vieții private în sectorul comunicațiilor electronice, Clinica stomatologică Smile Vision își asumă obligația de a administră în condiții de siguranță și numai pentru scopurile specificate mai jos, toate datele personale furnizate de utilizatorii acestui site. </p>
        <p>Pentru a putea răspunde la solicitările trimise prin intermediul formularelor de contact, pentru a înregistra programări online, pentru personalizarea preferințelor tale, pentru îmbunătățirea experienței tale pe site și pentru a asigura functionalități adiționale este absolut necesar să colectăm câteva date personale.</p>
        <p>Ai dreptul de acces, dreptul de intervenție, dreptul de a nu fi supus unei decizii individuale, dreptul de a fi uitat și dreptul de a te adresa justiției. Totodată, ai dreptul să te opui prelucrării datelor personale care te privesc și dreptul să ne soliciți ștergerea lor din baza noastră de date, excepție făcând datele pe care suntem obligați să le păstrăm în scopuri administrative, juridice sau de securitate.</p>
        <p>În cazul în care ai lăsat comentarii pe acest site sau ai alte date cu caracter personal în cadrul clinicii Smile Vision, ne poți solicita pe bază unei cereri scrise în mod gratuit la adresa: Str. Gheorghe Lazăr nr. 9, corp H, et. 2, Timișoara următoarele: </p>
        <ul>
            <li>O dată pe an să ți se confirme dacă datele tale personale sunt sau nu prelucrate</li>
            <li>Să intervii asupra datelor transmise</li>
            <li>Să te opui prelucrării datelor pentru motive întemeiate și legitime legate de situația lor particulară</li>
            <li>Să soliciți ștergerea tuturor datelor, cu excepția situațiilor prevăzute de lege</li>
            <li>De asemenea, dacă anumite date personale sunt incorecte, te rugăm să ne informezi cât mai curând posibil.</li>
        </ul>
        <p>Orice persoană preocupată în mod special de confidențialitatea datelor, poate intra în legătură directă cu reprezentantul firmei noastre prin apel telefonic la numărul <a href="0724005832">0724 005 832</a>.</p>
        <p>Prin completarea datelor în formularul de Contact / Programare declari și accepți necondiționat ca datele personale să fie incluse în bază noastră de date și îți oferi acordul expres și neechivoc ca toate aceste date personale să fie stocate, utilizate și prelucrate în scopurile prevăzute mai sus. </p>
        <p>Datele cu caracter personal vor fi păstrate numai în perioada necesară îndeplinirii scopurilor stabilite, cu respectarea drepturilor persoanei vizate, în special a dreptului de acces, de intervenție și de opoziție. În urma verificărilor periodice, datele cu caracter personal care nu mai servesc realizării scopurilor, vor fi distruse sau transformate în date anonime, într-un interval de timp rezonabil, potrivit procedurilor stabilite de lege.</p>
        <br>
    </div>
</section>