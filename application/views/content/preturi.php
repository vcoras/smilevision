<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Prețuri</h1>
        <div class="accordion" id="listaPreturi">
            <div class="card">
                <div class="card-header" id="headingGeneral">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiGeneral" aria-expanded="true" aria-controls="collapseOne">
                            General
                        </button>
                    </h2>
                </div>

                <div id="preturiGeneral" class="collapse show" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="0">Consultatie</div>
                            <div data-aos="fade-left" data-aos-delay="0">100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="50">Plan tratament, modele studiu montate in articulator cu arc facial</div>
                            <div data-aos="fade-left" data-aos-delay="50">300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="100">Scos fire</div>
                            <div data-aos="fade-left" data-aos-delay="100">30 - 50 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="150">Igienizare profesionala 6 luni</div>
                            <div data-aos="fade-left" data-aos-delay="150">200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="200">Igienizare profesionala</div>
                            <div data-aos="fade-left" data-aos-delay="200">250 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="250">Fluorizare, remineralizare / arcada (include gutiera)</div>
                            <div data-aos="fade-left" data-aos-delay="250">300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div data-aos="fade-right" data-aos-delay="300">Anestezie locala</div>
                            <div data-aos="fade-left" data-aos-delay="300">30 lei</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTerapie">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiTerapie" aria-expanded="false" aria-controls="collapseOne">
                            Terapie
                        </button>
                    </h2>
                </div>

                <div id="preturiTerapie" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>Pansamente dentare (tratament de urgenta stomatologica)</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatie CIS</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatii compozit</div>
                            <div>300 - 350 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Reconstituire coronara</div>
                            <div>400 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Coafaj direct (MTA ProROOT), obturatie cai false, perforatii, apexificare</div>
                            <div>300 - 400 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Reconstituire dinte cu pivot endodontic</div>
                            <div>300 - 400 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Albire / dinte</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Tratament de albire chimica a dintilor / arcada</div>
                            <div>400 lei</div>
                        </div> 
                        <div class="row d-flex justify-content-between">
                            <div>Desensibilizare</div>
                            <div>100 lei</div>
                        </div>  
                        <div class="row d-flex justify-content-between">
                            <div>Imobilizare dinti</div>
                            <div>150 lei</div>
                        </div>                 
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingEndodontie">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiEndodontie" aria-expanded="false" aria-controls="collapseOne">
                            Endodonție
                        </button>
                    </h2>
                </div>

                <div id="preturiEndodontie" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>Tratament de canal</div>
                            <div>400 - 600 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Drenaj endodontic</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Dezobturare / canal</div>
                            <div>70 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie corpi straini de pe canal (DCR, Dentatus etc.)</div>
                            <div>100 - 200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Tratament canal Ca(OH)</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Ablatie coroana</div>
                            <div>70 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Rezectie apicala (cu obturatie retrograde M TA ProRoot)</div>
                            <div>600 - 800 lei</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingChirurgie">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiChirurgie" aria-expanded="false" aria-controls="collapseOne">
                            Chirurgie
                        </button>
                    </h2>
                </div>

                <div id="preturiChirurgie" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>Extractie simpla</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie resturi radiculare</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie chirurgicala</div>
                            <div>300 - 500 lei</div>
                        </div>  
                        <div class="row d-flex justify-content-between">
                            <div>Extractie dinte inclus</div>
                            <div>400 - 700 lei</div>
                        </div> 
                        <div class="row d-flex justify-content-between">
                            <div>Sutură postextractională</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aplicare PRF</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Gingivectomie / dinte</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Alungire coronara</div>
                            <div>400 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Chiuretaj parodontal / dinte</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Chiuretaj parodontal / arcade (în câmp inchis)</div>
                            <div>500 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Chiuretaj parodontal / arcada (în câmp deschis)</div>
                            <div>1.000 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Grefa gingivala libera / tesut conjunctiv</div>
                            <div>200 - 300 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Augmentare osoasa orizontala / implant</div>
                            <div>400 - 600 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Augmentare osoasa verticala / implant</div>
                            <div>600 - 800 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Sinus lift intern</div>
                            <div>250 - 500 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Sinus lift extern</div>
                            <div>800 - 1.000 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Inserare implant Dentium / Megagen</div>
                            <div>500 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Inserare implant Megagen ANYRIDGE</div>
                            <div>700 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Inserare implant Bredent – Life time warranty</div>
                            <div>800 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Descoperire implant cu repozitionare apicala</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Ghid chirurgical / implant</div>
                            <div>100 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Indepartare implant</div>
                            <div>450 lei</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingProtetica">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiProtetica" aria-expanded="false" aria-controls="collapseOne">
                            Protetică
                        </button>
                    </h2>
                </div>

                <div id="preturiProtetica" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>Model Studiu / arcada</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Wax-up  mock-up / arcada</div>
                            <div>300 - 600 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Coroana provizorie</div>
                            <div>150 - 200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Onlay, inlay</div>
                            <div>350 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Coroana ceramica (cu suport metalic)</div>
                            <div>170 - 200 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Coroana integral ceramica</div>
                            <div>350 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Coroana ceramica (cu suport de Zirconiu)</div>
                            <div>350 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Coroană Zirconiu Monolithic – Life time warranty</div>
                            <div>500 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Fateta ceramica</div>
                            <div>400 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Bont individual Zirconiu pe implant</div>
                            <div>250 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Proteza scheletata cu sisteme de ancorare speciale de precizie</div>
                            <div>3.500 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Proteza totala / partiala</div>
                            <div>1.800 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Reparatie, captusire proteza</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Gutiera bruxism Michigan</div>
                            <div>150 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Amprentă lingură individuală</div>
                            <div>100 lei</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingOrtodontie">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiOrtodontie" aria-expanded="false" aria-controls="collapseOne">
                            Ortodonție
                        </button>
                    </h2>
                </div>

                <div id="preturiOrtodontie" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>Activare arcada aparat Safir</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>CONTENTIE Retainer fix + Gutiera termoformata / arcada</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Ajustare splint repozitionare ATM</div>
                            <div>50 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Splint repozitionare ATM</div>
                            <div>900 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Activare expansor / distalizator</div>
                            <div>50 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Activare arcada Aparat Metalic Clasic</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat fix partial (in functie de caz)</div>
                            <div>250 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat fix partial (in functie de caz) 2</div>
                            <div>500 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Recolare bracket metalic clasic</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Activare aparat lingual / arcada</div>
                            <div>150 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Recolare bracket lingual</div>
                            <div>160 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Consultatie Ortodontica</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Amprenta + modele</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat ortodontic mobil realizat in laborator</div>
                            <div>800 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat ortodontic mobil realizat in laborator 2</div>
                            <div>1.200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Activare aparat mobil realizat in laborator</div>
                            <div>80 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Expansor / distalizator realizat in laborator</div>
                            <div>300 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Miniimplant ortodontic</div>
                            <div>150 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat FIX METALIC CLASIC / arcada</div>
                            <div>700 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Recolare bracket Safir</div>
                            <div>150 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat fix lingual STB / arcada</div>
                            <div>1700 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat estetic SAFIR / arcada</div>
                            <div>1.100 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Demontare aparat fix / arcada</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Inregistrare arc facial + montare in articulator</div>
                            <div>60 euro</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Aparat fix zona ant-safir zona post-metalic / arcada</div>
                            <div>900 euro</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingPedodontie">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiPedodontie" aria-expanded="false" aria-controls="collapseOne">
                            Pedodonție
                        </button>
                    </h2>
                </div>

                <div id="preturiPedodontie" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>Sigilare</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Sigilare dinte permanent</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Periaj profesional</div>
                            <div>130 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Periaj + fluorizare</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Consult consiliere de specialitate</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Tratament de urgenta</div>
                            <div>150 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie complexa</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie dinte mobil</div>
                            <div>150 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie dinte fara mobilitate</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Extractie rest radicular</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Tratament canal</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Drenaj endodontic</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Anestezie locala</div>
                            <div>30 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatie provizorie</div>
                            <div>100 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatie CIS</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatie o suprafata</div>
                            <div>300 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatie 2 suprafate</div>
                            <div>330 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Obturatie 3 suprafate</div>
                            <div>350 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Tratament canal Ca(OH)<sub>2</sub></div>
                            <div>200 lei</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingRadiologie">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#preturiRadiologie" aria-expanded="false" aria-controls="collapseOne">
                            Radiologie
                        </button>
                    </h2>
                </div>

                <div id="preturiRadiologie" class="collapse" aria-labelledby="headingOne" data-parent="#listaPreturi">
                    <div class="card-body">
                        <div class="row d-flex justify-content-between">
                            <div>3D endo</div>
                            <div>150 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>3D partial / arcada</div>
                            <div>200 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>3D total</div>
                            <div>350 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Radiografie retroalveolara</div>
                            <div>30 lei</div>
                        </div>
                        <div class="row d-flex justify-content-between">
                            <div>Radiografie panoramica</div>
                            <div>80 lei</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <h2 data-aos="fade-left">Tratament cu plata în rate</h2>
        <p data-aos="fade-left">Sănătatea TA este cea mai importantă, iar TU zâmbind, cu încredere, fără probleme, ești de neoprit în ceea ce vrei să realizezi.</p>
        <br>
        <p data-aos="fade-left">Prin parteneriatul cu Banca Transilvania, Smile Vision oferă posibilitatea achitării planurilor de tratament în 6 rate egale, fără dobândă. Instrumentele ce intermediază această ofertă sunt cardurile Star BT.</p>
        <br />
        <p data-aos="fade-left"><strong>Beneficiile incontestabile ale cardului STAR în clinica Smile Vision:</strong></p>
        <ul data-aos="fade-left" class="pl-4">
            <li>6 rate fixe: plan de plată în rate pentru tratamente dentare prin partenerul nostru financiar Banca Transilvania</li>
            <li>Dobândă 0%</li>
            <li>Fără avans</li>
            <li>Costuri zero la utilizarea cardului la comercianți în țară și în străinătate</li>
            <li>Tratamentul începe imediat</li>
            <li>Garanția tratamentelor realizate (până la 5 ani cu respectarea consultului obligatoriu la fiecare 6 luni)</li>
            <li>Plan de tratament + simulare plată GRATUITĂ</li>
            <li>Completarea dosarului chiar la clinică cu ajutorul unui Patient Care Coordinator Smile Vision și a partenerilor noștri financiari, fără alt drum la bancă.</li>
        </ul>
        <br />
    </div>
</section>