<div class="slider" data-aos="fade">
    <img src="<?php echo media_url('home/slide1.jpg'); ?>" alt="Slide1 Smile Vision" />
    <div class="container container-960">
        <div class="row">
            <div class="slider-wrapper clearfix">
                <div class="col-10 offset-5 col-md-8 offset-md-6">
                    <h1>Vino <br> să-ți redăm <br>ZÂMBETUL</h1>
                    <p>Te așteptăm cu drag :)</p>
                    <div>
                        <a class="js-open-form-modal" href="<?php echo base_url($this->language["url_key"] . "/contact"); ?>">Vreau o programare</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bt-ribbon">
    <div class="container container-960 d-flex justify-content-between">
        <p class="align-self-center mr-auto pr-2"><strong>Tratamente cu plata în rate, fără dobândă.</strong> Ne ocupăm noi de tot.</p>
        <p class="align-self-center text-right" class="images">
            Prin
            <img src="<?php echo media_url("home/StarBT.png") ?>" alt="Banca Transilvania Star BT" />
            <img src="<?php echo media_url("home/BT.png") ?>" alt="Banca Transilvania" />
        </p>
    </div>
</div>
<section class="section section-1">
    <div class="container container-960">
        <h2 data-aos="fade-up" data-aos-anchor-placement="top-bottom">De curând, <br>într-un loc nou, <br>central</h2>
        <p data-aos="fade-up" data-aos-anchor-placement="top-bottom">PIAȚA 700</p>
        <p data-aos="fade-up" data-aos-anchor-placement="top-bottom">Str. Gheorghe Lazăr nr. 9, clădirea Alcatel</p>
        <p data-aos="fade-up" data-aos-anchor-placement="top-bottom">Timișoara</p>
        <a class="js-open-form-modal" data-aos="fade-up" data-aos-anchor-placement="top-bottom" href="<?php echo base_url("/contact"); ?>">Vreau o consultație</a>
    </div>
</section>
<section class="section gallery">
    <div class="row m-0 gallery-8">
        <img data-aos-delay="0"   data-aos="fade-down" src="<?php echo media_url("home/small/1.jpg") ?>" alt="1" />
        <img data-aos-delay="100" data-aos="fade-down" src="<?php echo media_url("home/small/2.jpg") ?>" alt="2" />
        <img data-aos-delay="200" data-aos="fade-down" src="<?php echo media_url("home/small/3.jpg") ?>" alt="3" />
        <img data-aos-delay="300" data-aos="fade-down" src="<?php echo media_url("home/small/4.jpg") ?>" alt="4" />
        <img data-aos-delay="400" data-aos="fade-down" src="<?php echo media_url("home/small/5.jpg") ?>" alt="5" />
        <img data-aos-delay="500" data-aos="fade-down" src="<?php echo media_url("home/small/6.jpg") ?>" alt="6" />
        <img data-aos-delay="600" data-aos="fade-down" src="<?php echo media_url("home/small/7.jpg") ?>" alt="7" />
        <img data-aos-delay="700" data-aos="fade-down" src="<?php echo media_url("home/small/8.jpg") ?>" alt="8" />
    </div>
    <div data-aos-delay="800" data-aos="fade-down" class="row m-0 video">
        <video controls poster="<?php echo media_url("home/video.jpg") ?>">
            <source src="<?php echo media_url("home/prezentare-smile-vision.mp4") ?>" type="video/mp4">
            Nu putem reda acest video in browser-ul dumneavoastra.
        </video>
    </div>
</section>
<section class="section top-services">
    <h2>Servicii de top</h2>
    <div class="services-wrapper">
        <div data-aos-delay="0" data-aos="fade-down" class="row">
            <div class="col-md-6">
                <img src="<?php echo media_url('home/servicii/implantologie.jpg') ?>" alt="Implantologie" />
            </div>
            <div class="col-md-6 text">
                <h3>Implantologie</h3>
                <p>Implantul dentar este cea mai bună soluție pentru înlocuirea unuia sau mai multor dinți lipsă. Actualele tehnici ne permit să realizăm implantarea unui dinte artificial indiferent de situația care a condus la pierderea celui natural.</p>
                <p>Cu ajutorul tehnicilor pe care le utilizăm inserarea unui implant este ușor de suportat, iar recuperarea este rapidă.</p>
                <p><strong>Puteți</strong> pleca după intervenția de inserare a implanturilor cu dinți provizorii, dinți cu care puteți zâmbi, mânca și vorbi.</p>
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/implantologie") ?>">Află mai multe</a>
            </div>
        </div>
        <div data-aos-delay="100" data-aos="fade-down" class="row">
            <div class="col-md-6 text">
                <h3>Stomatologie digitală</h3>
                <p>Fiecare arie în domeniul stomatologiei este impactată de digitalizarea tratamentelor dentare. Utilizarea tehnologiilor sau dispozitivelor dentare care încorporează componente digitale sau controlate de calculator oferă avantaje semnificative față de medicina dentară convențională.</p>
                <p>De la radiografii cu un nivel mult mai scăzut de radiații, zâmbet perfect integrat în fizionomia feței, configurat pe calculator, până la reconstrucții dentare realizate în timp record.</p>
                <p>În clinica noastră aveți parte de un “digital workflow” complet.</p>
                <a href="<?php echo base_url($this->language['url_key'] . "/stomatologie-digitala-timisoara") ?>">Află mai multe</a>
            </div>
            <div class="col-md-6">
                <img src="<?php echo media_url('home/servicii/stomatologie.jpg') ?>" alt="Stomatologie digitală" />
            </div>
        </div>
        <div data-aos-delay="200" data-aos="fade-down" class="row">
            <div class="col-md-6">
                <img src="<?php echo media_url('home/servicii/laborator.jpg') ?>" alt="Laborator dentar digital" />
            </div>
            <div class="col-md-6 text">
                <h3>Laborator dentar digital</h3>
                <p>Schimbările tehnologice revoluționează cu adevărat în modul în care laboratoarele dentare fabrică restaurări protetice.</p>
                <p>Apariția CAD / CAM a permis medicilor stomatologi și laboratoarelor să valorifice puterea computerelor pentru a proiecta și fabrica restaurări protetice, estetice și durabile.</p>
                <p>Tehnica digitală din cadrul laboratorului dentar grăbește proiectarea și producerea restaurărilor, combinând scanarea 3D, proiectarea CAD și frezarea cu cameră sau imprimarea 3D.</p>
                <a href="<?php echo base_url($this->language['url_key'] . "/laborator-tehnica-dentara-timisoara") ?>">Află mai multe</a>
            </div>
        </div>
        <div data-aos-delay="300" data-aos="fade-down" class="row">
            <div class="col-md-6 text">
                <h3>Tratament endodontic asistat microscopic</h3>
                <p>Tratamentul endodontic se efectuează în locuri întunecate și restrânse, iar fracțiunile de milimetri pot decide rezultatul tratamentului.</p>
                <p>Microscopul dentar a devenit indispensabil atât pentru diagnostic cât și pentru terapie în toate procedurile dentare, dar îndeosebi în endodonție (tratamentul de canal)</p>
                <p>Rolul acestui tratament este de a menține dinții funcționali pe arcadă, în ciuda faptului că pulpa a fost afectată.</p>
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/endodontie") ?>">Află mai multe</a>
            </div>
            <div class="col-md-6">
                <img src="<?php echo media_url('home/servicii/microscop.jpg') ?>" alt="Tratament endodontic asistat microscopic" />
            </div>
        </div>
        <div data-aos-delay="400" data-aos="fade-down" class="row">
            <div class="col-md-6">
                <img src="<?php echo media_url('home/servicii/radiologie.jpg') ?>" alt="Radiologie" />
            </div>
            <div class="col-md-6 text">
                <h3>Radiologie</h3>
                <p>Radiografia dentară răspunde întrebărilor pe care medicii dentiști le au atunci când o simplă consultație nu scoate la iveală toate problemele pacientului, fiind un factor esențial de prevenție, deoarece ea semnalează din timp afecțiuni ce necesită tratate.</p>
                <p>În funcție de scopul pentru care sunt efectuate, există mai multe feluri de radiografii dentare.</p>
                <a href="<?php echo base_url($this->language['url_key'] . "/radiologie-dentara-digitala") ?>">Află mai multe</a>
            </div>
        </div>
        <div class="row">
            <a href="<?php echo base_url($this->language["url_key"] . "/servicii"); ?>" class="see-all">Vezi toate serviciile</a>
        </div>
    </div>
</section>
<section class="section section-2 testimonials">
    <div class="container" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm-12">
                <img src="<?php echo media_url('home/testimoniale/9.jpg'); ?>">
            </div>
            <div class="column-text col-md-9 col-sm-12">
                <div class="wrapper">
                    <div class="name">Ramona</div>
                    <div class="function">Inginer chimist, 38 ani</div>
                    <div class="content">
                        <p>
                            Felicitări tuturor angajaților pentru profesionalismul de care dau dovadă! Atunci când îți pasă de pacienți, ei simt asta, iar eu o spun din tot sufletul că alegerea făcută este una din cele mai bune din viața mea! Și acesta este un lucru măreț!!! 💎💎💎
                            <br><br>
                            Excelența își lasă amprenta în orice serviciu oferit de Smile Vision!
                            <br><br>
                            Servicii impecabile, m-am simțit ca acasă! Abia aștept să revin 😊
                            <br><br>
                            Mulțumesc! Mulțumesc! Mulțumesc!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm-12">
                <img src="<?php echo media_url('home/testimoniale/10.jpg'); ?>">
            </div>
            <div class="column-text col-md-9 col-sm-12">
                <div class="wrapper">
                    <div class="name">Florentina</div>
                    <div class="function">Farmacist, 36 ani</div>
                    <div class="content">
                        <p>
                            Recomand cu mare drag Clinica Smile Vision. Colaborăm din 2016 și nu am fost niciodată dezamăgită. Am avut parte de un plan de lucrări complet și foarte bine efectuat, inclusiv implant și aparat dentar.
                            <br><br>
                            La Smile Vision veți fi întâmpinați de o echipă de profesioniști care se completează perfect. Sunt în permanență orientați spre client și dornici să se dezvolte pentru a oferi servicii de cea mai înaltă calitate.
                            <br><br>
                            Felicitări, Smile Vision! Sunteți parte din viața mea și mă bucur că vă cunosc! Succes în continuare!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm-12">
                <img src="<?php echo media_url('home/testimoniale/11.jpg'); ?>">
            </div>
            <div class="column-text col-md-9 col-sm-12">
                <div class="wrapper">
                    <div class="name">Mădălina</div>
                    <div class="function">Tehnician radiolog, 28 ani</div>
                    <div class="content">
                        <p>
                            Înainte de a veni la clinică îmi era foarte frică de tot ce înseamnă tratamente stomatologice, mai ales că trebuia să îmi extrag 3 măsele de minte și amânam de foarte mult timp. M-am simțit în siguranță de la prima interacțiune cu medicii, cum au vorbit cu mine, cum mi-au explicat. Mi-am extras 3 masele de minte la Dr. Mario Chilom și nu m-a durut deloc. Nici acum nu îmi vine să cred ce frică aveam înainte și ce simplu a putut să fie.
                            <br><br>
                            Recomand cu încredere! O echipă cu adevărat profesionistă și dedicată!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm-12">
                <img src="<?php echo media_url('home/testimoniale/12.jpg'); ?>">
            </div>
            <div class="column-text col-md-9 col-sm-12">
                <div class="wrapper">
                    <div class="name">Dorotheea</div>
                    <div class="function">Jurist, 30 de ani</div>
                    <div class="content">
                        <p>
                            În 2005 eram în căutarea unui medic stomatolog care să îmi pună la punct dantura, cu pretenția ca rezultatul să fie "de reclamă TV". Am ajuns la Dr. Paul Zaharia iar ceea ce a reușit să facă mi-a întrecut toate așteptările.
                            <br><br>
                            Paul are în jurul lui o echipă care împreună cu el asigură pacienților servicii de o calitate excepțională, indiferent de gradul de complexitate al lucrărilor dentare. Toți sunt profesionisti de excepție, serioși și foarte amabili. Mulțumesc, întregii echipe!
                            <br><br>
                            Vă recomand cu drag și cu cea mai mare încredere!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm-12">
                <img src="<?php echo media_url('home/testimoniale/1.jpg'); ?>">
            </div>
            <div class="column-text col-md-9 col-sm-12">
                <div class="wrapper">
                    <div class="name">Paula</div>
                    <div class="function">Blogger, 35 de ani</div>
                    <div class="content">
                        <p>
                            Țin nespus de mult la dinții mei, mai ales că râd cu gura până la urechi. Pentru asta îmi spăl dinții zilnic și am un doctor extraordinar care mă păzește de infecții.
                            <br><br>
                            E liniștitor să ai pe cineva competent să lupte pentru păstrarea dinților.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/2.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Puiu</div>
                    <div class="function">Inginer, 43 de ani</div>
                    <div class="content">
                        <p>În sfârșit un cabinet stomatologic în care ești tratat corect și la care te reîntorci cu drag. Oameni calmi, profesioniști și deschiși să-ți răspundă la orice întrebare, astfel încât să înțelegi diagnosticul și toți pașii tratamentului.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/3.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Sanda</div>
                    <div class="function">Nail artist, 33 de ani</div>
                    <div class="content">
                        <p>O echipă extrem de profesionistă. Pentru prima dată când am simțit că am parte de tratamente de cea mai bună calitate la stomatolog. Cu explicații clare pe înțelesul meu și cu cele mai noi tehnici și aparate. Un ambient extrem de plăcut, modern, impecabil.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/4.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Andreea</div>
                    <div class="function">Quality engineer, 29 de ani</div>
                    <div class="content">
                        <p>
                            În urma unor dureri la un molar de minte am mers la Smile Vision unde am dat peste un personal foarte prietenos, de la recepție până la domnișoara doctor Sorina Copaci care s-a ocupat de mine cu foarte multă răbdare și profesionalism. Îi mulțumesc pentru competența, seriozitatea, meticulozitatea, respectul, comunicarea prietenoasă, dar și pentru îngrijirea pe care mi-a acordat-o în toată perioada de tratament. Recomand cu multă încredere și drag!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/5.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Daniela</div>
                    <div class="function">Director Resurse Umane, 37 de ani</div>
                    <div class="content">
                        <p>
                            Cea mai tare și faină echipă! Apreciez foarte tare amabilitatea și căldura cu care vă primiți pacienții and I feel like home :) Și pentru asta meritați și o poezie, poate se motivează mai mulți oameni să facă vizite mai dese la dentist 'cause is fun :).
                            <br><br>
                            ”Ursulețului Axinte <br>
                            I se clatină un dinte. <br>
                            Să-l văd trist eu nu rezist, <br>
                            Mergem iute, la dentist.<br>
                            Gurița nici n-a căscat<br>
                            Că pe loc l-a rezolvat.<br>
                            Demn fiind de laudă,<br>
                            Toată lumea-aplaudă!<br>
                            Ursulețul curajos<br>
                            Acu-i tare bucuros.<br>
                            Cum mă doare-un dințișor,<br>
                            Pacientu-s următor.<br>
                            Ca să nu mă fac de râs<br>
                            Nici eu nu am zis nici pâs.”<br>
                            <br><br>
                            de Valeria Tamaș, not me but still cute
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/6.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Călin</div>
                    <div class="function">Manager IT, 38 de ani</div>
                    <div class="content">
                        <p>O echipă de profesioniști, oameni faini care reușesc să facă din mersul la stomatolog o experiență plăcută, începând cu programările, comunicarea cu clientul și terminând cu partea de intervenții complexe unde profesionalismul își spune din nou cuvântul. Sunt perfectioniști, cu o grijă deosebită față de pacient. Recomand serviciile Smile Vision, cu maximă încredere.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/7.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Maurizio</div>
                    <div class="function">Real Estate Businessman, 36 years</div>
                    <div class="content">
                        <p>A great experience! Dr. Zaharia is very professional and kind, his office is clean modern and stylish. My treatment went fine and I am completely satisfied with the result.<br><br>Very recommended!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="quotes">
                <img src="<?php echo images_url('quotes.png') ?>" alt="Quotes">
            </div>
            <div class="column-image col-md-3 col-sm">
                <img src="<?php echo media_url('home/testimoniale/8.jpg'); ?>">
            </div>
            <div class="column-text col-md-9">
                <div class="wrapper">
                    <div class="name">Tina</div>
                    <div class="function">Expert contabil, 37 de ani</div>
                    <div class="content">
                        <p>
                            Tocmai azi am finalizat o lucrare de implant în clinica SMILE VISION, totul a decurs foarte bine. Servicii impecabile, personal foarte amabil, toată lumea zâmbește. Cred că de aici și numele SMILE VISION :)))
                            <br><br>
                            Mulțumesc Paul, mulțumesc echipei.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section smilevision">
    <h2 data-aos="fade-down">Noi suntem Smile Vision</h2>
</section>
<section class="section diplomas">
    <div class="container container-960">
        <div class="row no-gutter">
            <div class="col-md-9">
                <h2 data-aos="fade-down">Diplome și certificări</h2>
                <p data-aos="fade-down">Pentru desăvârșirea profesionalismului și pentru a-ți oferi servicii de cel mai înalt nivel, participăm constant la cursuri de perfecționare.</p>
                <a data-aos="fade-down" href="<?php echo base_url($this->language["url_key"] . "/clinica#certificari") ?>">Află mai multe</a>
            </div>
            <div data-aos="fade-down" class="col-md-3">
                <img src="<?php echo media_url("home/diploma.jpg"); ?>" alt="Diploma Paul Zaharia" />
            </div>
        </div>
    </div>
</section>
<section class="section contact">
    <div class="container container-960">
        <h2 data-aos="fade-down">Fă-ți o programare</h2>
        <div class="row justify-content-md-center no-gutter">
            <div class="col-md-4">
                <form action="<?php echo current_url(); ?>" method="post">
                    <div data-aos-delay="0" data-aos="fade-right" class="form-group">
                        <label for="full_name">Nume</label>
                        <input type="text" class="form-control" id="full_name" name="full_name">
                    </div>
                    <div data-aos-delay="100" data-aos="fade-right" class="form-group">
                        <label for="phone">Telefon</label>
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>

                    <div data-aos-delay="200" data-aos="fade-right" class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>

                    <div data-aos-delay="300" data-aos="fade-right" class="form-group">
                        <label for="message">Mesaj</label>
                        <textarea class="form-control" id="message" name="message"></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" id="submit" name="submit" value="Trimite">Trimite</button>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div data-aos-delay="0" data-aos="fade-left" class="row phone">
                    <h3 class="col-md-12">Telefon</h3>
                    <p class="col-md-12"><a href="tel:0724005832">0724 005 832</a></p>
                </div>
                <div data-aos-delay="100" data-aos="fade-left" class="row email">
                    <h3 class="col-md-12">E-mail</h3>
                    <p class="col-md-12"><?php echo safe_mailto('office@smilevision.ro'); ?></p>
                </div>
                <div data-aos-delay="200" data-aos="fade-left" class="row address">
                    <h3 class="col-md-12">Adresă</h3>
                    <p class="col-md-12">Str. Gheorghe Lazăr nr. 9, corp H, et. 2,<br>Timișoara</p>
                </div>
            </div>
        </div>
    </div>
</section>
