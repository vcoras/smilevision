<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Ortodonție</h2>
                <p data-aos="fade-left">Ortodonția este partea stomatologiei care se ocupă cu prevenirea și tratamentul pozițiilor defectuoase ale dinților, prin așezarea în poziție normală a dinților pe arcadele dentare.</p>
                <p data-aos="fade-left">Altfel spus, prin procedurile de ortodonție, dinţii strâmbi sau aliniați incorect sunt așezați în poziție corespunzătoare.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url("servicii/ortodontie/0.jpg") ?>" alt="Ortodontie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Încă din perioada copilăriei, prin vizitele regulate la medicul stomatolog, este indicat să se intercepteze anomaliile dentare. În caz contrar, odată ajunși la maturitate, procedurile de ortodonție devin mai complexe și mai costisitoare.</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de ortodonție pe care le utilizăm</h3>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Tratamentul ortodontic se realizează prin diverse tipuri de aparate. În urma unei analize amănunțite a tipului de anomalie, recomandăm tipul de aparat corespunzător: fix sau mobil, pentru realinierea dinților, reantrenarea mușchilor și influențarea creșterii maxilarelor.</p>
            <p data-aos="fade-down">Durata tratamentelor ortodontice depinde de gravitatea anomaliei și de vârsta pacientului. În funcție de aceste două criterii principale, stabilim și personalizăm procedurile care trebuie urmate, plus tipul de aparat potrivit pacientului.</p>
            <p data-aos="fade-down">Plecând de la recomandările noastre, ajungem la preferințele pacientului, care poate alege tipul de aparat dorit, precum și personalizarea acestuia.</p>
        </div>
    </div>
</div>
<div class="section-types">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Tipuri de aparate dentare</h3>
        </div>
        <div class="row no-gutter mb-4">
            <div data-aos="fade-left" class="col-md-9">
                <p><strong>Aparat dentar transparent fix Safir</strong></p>
                <p>Aparatul dentar cu brackeți din safir este o alegere modernă de îndreptare a dinților, ajutând la corectarea poziției și la obținerea unei danturi funcționale, fără ca ceilalți să observe că porți aparat dentar.</p>
            </div>
            <div data-aos="fade-right" class="col-md-3 pl-0 pr-0">
                <img src="<?php echo media_url("servicii/ortodontie/1.jpg") ?>" alt="Ortodontie">
            </div>
        </div>
        <div class="row no-gutter">
            <div data-aos="fade-left" class="col-md-9">
                <p><strong>Aparat dentar fix metalic</strong></p>
                <p>Aparatul dentar metalic asigură un tratament eficient, este accesibil din punctul de vedere al costului, iar brackeții sunt rezistenți. Este recomandat în cazul oricărei anomalii dentare.</p>
            </div>
            <div data-aos="fade-right" class="col-md-3 pl-0 pr-0">
                <img src="<?php echo media_url("servicii/ortodontie/2.jpg") ?>" alt="Ortodontie">
            </div>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">La fel ca în cazul tuturor serviciilor pe care le oferim, și în ceea ce privește tratamentele de ortodonție folosim tehnologiile și materialele de ultimă generație. Și nu ne rezumăm la asta.</p>
                <p data-aos="fade-down">Pentru că medicina dentară este într-o continuă evoluție, medicul nostru specializat în ortodonție se află într-o continuă perfecționare. Pentru că zâmbetul tău merită cele mai bune și performante tehnici și aparaturi.</p>
                <p data-aos="fade-down">La fel cum fiecare persoană are trăsături proprii, fiecare zâmbet este diferit. Pentru noi, ortodonția nu înseamnă a aduce zâmbetul la un anumit standard de frumusețe, ci a oferi fiecărei persoane zâmbetul care să îi dea încredere în sine. Prin tratamentele de ortodonție pe care le efectuăm, nu ne rezumăm doar la alinierea dinților, ci investigăm amănunțit tipul de anomalie, pentru care recomandăm aparatul potrivit.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url("servicii/medici/flavia.jpg"); ?>" alt="Flavia">
                        <p>Flavia</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>