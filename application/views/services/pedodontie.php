<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Pedodonție</h2>
                <p data-aos="fade-left">Pedodonția înseamnă stomatologie pediatrică, adică acea parte a medicinei dentare care se adresează copiilor.</p>
                <p data-aos="fade-left">Oriunde am fi, un zâmbet sănătos, frumos, armonios ne face ușor remarcați... este cartea noastră de vizită.</p>
                <p data-aos="fade-left">Tratamentele pedodontice sunt proceduri importante în viața orcărui copil, începând de la cele mai mici vârste. deoarece pierderea precoce a dinților "de lapte" poate provoca diverse complicații, care vor influența viața adultă a danturii micilor pacienți.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/pedodontie/0.jpg') ?>" alt="Pedodontie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Stomatologia copilului este diferită de stomatologia adultului. Încă din această perioadă se pot preveni anumite modificări dentare, motiv pentru care monitorizarea sănătății orale a copilului este esențială. Vizitele regulate la stomatolog, încă de la apariția primilor dinți, influențează în bine zâmbetul, sănătatea orală a viitorului adult și educația lui.</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de pedodonție utilizate în cabinetul nostru</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/pedodontie/1.jpg') ?>" alt="Pedodontie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/pedodontie/2.jpg') ?>" alt="Pedodontie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/pedodontie/3.jpg') ?>" alt="Pedodontie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Considerăm că tratamentul adresat copiilor necesită o atenție deosebită în privința comunicării dintre pacient și medicul stomatolog, deoarece primele vizite ale micuților la cabinet le influențează încrederea în sistemul stomatologic. De aceea, înainte de a porni orice tratament pedodontic, încercăm să găsim, alături de părinți, calea de comunicare potrivită fiecărui mic pacient în parte.</p>
            <p data-aos="fade-down"><strong>Principalele tratamente de pedodonție pe care le efectuăm:</strong></p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">Fluorizări realizate în cabinet</li>
                <li data-aos="fade-left" data-aos-delay="50">Sigilarea dinților permanenți</li>
                <li data-aos="fade-left" data-aos-delay="100">Interceptarea anomaliilor dentare și rezolvarea acestora încă de la primele semne, pentru a împiedica malpoziționarea dinților</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Știm că rolul nostru nu constă doar în elaborarea și aplicarea tratamentelor pedodontice, ci se extinde în educarea copiilor legat de îngrijirea corectă a dinților.</p>
                <p data-aos="fade-down">Treptat, copiii trebuie să dobândească tehnicile necesare pentru a-și îngriji singuri și într-un mod corect dinții, iar în acest proces, alături de părinți, noi, medicii dentiști, avem un rol esențial. De aceea, echipa noastră nu se rezumă la efectuarea tratamentelor de pedodonție, ci se străduiește să găsească întotdeauna calea pentru a-i educa legat de:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">Prevenirea cariilor și afecțiunilor gingivale</li>
                    <li data-aos="fade-left" data-aos-delay="50">Controlul plăcii bacteriene</li>
                    <li data-aos="fade-left" data-aos-delay="100">Indicații pentru periajul dentar și igiena bucală corectă</li>
                </ul>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/alexandra_majorosi.jpg'); ?>" alt="Alexandra Majorosi">
                        <p>Alexandra Majorosi</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>