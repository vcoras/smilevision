<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Endodonție</h2>
                <p data-aos="fade-left">Endodonția este ramura stomatologiei care se ocupă cu tratamentul rădăcinii dintelui, partea care nu este vizibilă în cavitatea bucală. </p>
                <p data-aos="fade-left">Tratamentul endodontic al unui dinte este o etapă din cadrul unui proces complex. Salvarea și păstrarea dintelui în cavitatea bucală este misiunea noastră prin această procedură stomatologică.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/endodontie/0.jpg') ?>" alt="endodontie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Fiecare dinte are o anatomie specifică. Majoritatea se încadrează statistic în anumite tipare. Molarii de minte sunt mai atipici și nu se știe câte canale pot avea sau dacă sunt operabile în totalitate. Rar se poate aplica tratamentul endodontic molarilor de minte iar teoretic ei sunt ”neendodontici”.</p>
                <p data-aos="fade-left">Tratamentul endodontic are numeroase indicații, printre care:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">îndepărtarea unui nerv mort sau infectat</li>
                    <li data-aos="fade-left" data-aos-delay="50">îndepărtarea nervului în scop protetic</li>
                    <li data-aos="fade-left" data-aos-delay="100">vindecarea unui abces de la vârful dintelui</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de endodonție pe care le utilizăm</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/endodontie/1.jpg') ?>" alt="endodontie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/endodontie/2.jpg') ?>" alt="endodontie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/endodontie/3.jpg') ?>" alt="endodontie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">În tratamentul endodontic folosim:</p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0"><strong>sistemul de izolare cu diga</strong>, sistem care împiedică trecerea substanțelor utilizate pentru dezinfectarea rădăcinii dintelui în cavitatea bucală și care păstrează un câmp de lucru curat. Infectarea cu alte bacterii devine practic imposibilă.</li>
                <li data-aos="fade-left" data-aos-delay="50">Diga este singura bariera care protejează, în timpul tratamentului de endodonție, infectarea dintelui cu microorganismele din cavitatea bucală. În acest fel ești protejat de contactul cu substanțele folosite pentru curățarea canalelor, substanțe care pe lângă că au un gust neplăcut, mai pot produce și iritații.</li>
                <li data-aos="fade-left" data-aos-delay="100"><strong>lupe Zeiss</strong> care măresc și luminează zona care va fi tratată de până la 5 ori, oferind medicului gradul de precizie necesar pentru a identifica și trata canalele dintelui. Nu tot timpul este necesar un grad de mărire de până la 40 de ori, precum se poate realiza cu microscopul Leica pe care îl utilizăm când situația o cere.</li>
                <li data-aos="fade-left" data-aos-delay="50"><strong>microscopul Leica</strong> mărește de până la 40 de ori și luminează zona care va fi tratată, vizualizând orice complicație, oferind pacientului un tratament fără riscuri și o intervenție 100% precisă.</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Succesul unui tratament endodontic constă în dezinfecția foarte riguroasă a canalelor dintelui și obturarea acestora în mod tridimensional iar echipa noastră este specializată în atenție, răbdare, folosind o tehnologie de ultimă oră (lupe, microscop, radiologie pentru verificarea tratamentelor după execuție).</p>
                <p data-aos="fade-down">Un alt aspect căruia noi îi acordăm un interes deosebit, este tipul de materiale utilizate. În acest fel tratamentul endodontic are cele mai bune rezultate vizibile în timp. Folosim materiale 100% biologice, care nu dau colorații în timp dintelui și pe care le combinăm cu cele mai bune tehnici moderne în prepararea canalului.</p>
                <p data-aos="fade-down">Alături de acestea, sistemul pe care îl utilizăm în obturarea canalelor este sistemul de obturare tridimensional, recunoscut pentru sigilarea mult mai eficientă în comparație cu sistemele clasice.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/mario_chilom.jpg'); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sorina_copaci.jpg'); ?>" alt="Sorina Copaci">
                        <p>Sorina Copaci</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sergiu_buzatu.jpg'); ?>" alt="Sergiu Buzatu">
                        <p>Sergiu Buzatu</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>