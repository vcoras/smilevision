<div class="page-menu">
    <div class="container">
        <div class="row no-gutter">
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="0">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/chirurgie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'chirurgie' ? 'current' : ''; ?>">Chirurgie</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="50">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/fatete-dentare"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'fatete-dentare' ? 'current' : ''; ?>">Fațete dentare</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="100">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/endodontie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'endodontie' ? 'current' : ''; ?>">Endodonție</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="150">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/estetica-dentara"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'estetica-dentara' ? 'current' : ''; ?>">Estetică dentară</span>
                </a>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="200">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/implantologie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'implantologie' ? 'current' : ''; ?>">Implantologie</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="250">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/odontologie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'odontologie' ? 'current' : ''; ?>">Odontologie</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="300">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/ortodontie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'ortodontie' ? 'current' : ''; ?>">Ortodonție</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="350">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/parodontologie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'parodontologie' ? 'current' : ''; ?>">Parodontologie</span>
                </a>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="400">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/pedodontie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'pedodontie' ? 'current' : ''; ?>">Pedodonție</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="450">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/profilaxie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'profilaxie' ? 'current' : ''; ?>">Profilaxie</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="500">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/protetica"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'protetica' ? 'current' : ''; ?>">Protetică</span>
                </a>
            </div>
            <div class="col-md-3" data-aos="fade-down" data-aos-duration="700" data-aos-delay="550">
                <a href="<?php echo base_url($this->language['url_key'] . "/servicii/radiologie"); ?>">
                    <span class="<?php echo $this->uri->rsegment(3) == 'radiologie' ? 'current' : ''; ?>">Radiologie</span>
                </a>
            </div>
        </div>
    </div>
</div>