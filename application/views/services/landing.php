<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
        <p>Clinica Smile Vision îți oferă întreg spectrul de servicii stomatologice, folosing cele mai noi tehnologii, într-o locație centrală. Investigațiile radiologice se realizează direct în clinică și nu mai este nevoie să parcurgi orașul către alte centre de imagistică.</p>
        <p>Servicii complete, ai totul într-un singur loc!</p>
    </div>
</section>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter">
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <a class="inner" href="<?php echo base_url('ro/servicii/chirurgie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/1-chirurgie.jpg') ?>" alt="Chirurgie">
                    <h3>Chirurgie</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <a class="inner" href="<?php echo base_url('ro/servicii/fatete-dentare'); ?>">
                    <img src="<?php echo media_url('servicii/landing/2-fatete.jpg') ?>" alt="Fațete dentare">
                    <h3>Fațete dentare</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="150">
                <a class="inner" href="<?php echo base_url('ro/servicii/endodontie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/3-endodontie.jpg') ?>" alt="Endodonție">
                    <h3>Endodonție</h3>
                </a>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="200">
                <a class="inner" href="<?php echo base_url('ro/servicii/estetica-dentara'); ?>">
                    <img src="<?php echo media_url('servicii/landing/4-estetica.jpg') ?>" alt="Estetică dentară">
                    <h3>Estetică dentară</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="250">
                <a class="inner" href="<?php echo base_url('ro/servicii/implantologie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/5-implantologie.jpg') ?>" alt="Implantologie">
                    <h3>Implantologie</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="300">
                <a class="inner" href="<?php echo base_url('ro/servicii/odontologie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/6-odontologie.jpg') ?>" alt="Odontologie">
                    <h3>Odontologie</h3>
                </a>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="350">
                <a class="inner" href="<?php echo base_url('ro/servicii/ortodontie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/7-ortodontie.jpg') ?>" alt="Ortodonție">
                    <h3>Ortodonție</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="400">
                <a class="inner" href="<?php echo base_url('ro/servicii/parodontologie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/8-parodontologie.jpg') ?>" alt="Parodontologie">
                    <h3>Parodontologie</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="450">
                <a class="inner" href="<?php echo base_url('ro/servicii/pedodontie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/9-pedodontie.jpg') ?>" alt="Pedodonție">
                    <h3>Pedodonție</h3>
                </a>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="500">
                <a class="inner" href="<?php echo base_url('ro/servicii/profilaxie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/10-profilaxie.jpg') ?>" alt="Profilaxie">
                    <h3>Profilaxie</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="550">
                <a class="inner" href="<?php echo base_url('ro/servicii/protetica'); ?>">
                    <img src="<?php echo media_url('servicii/landing/11-protetica.jpg') ?>" alt="Protetică">
                    <h3>Protetică</h3>
                </a>
            </div>
            <div class="services-links col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="600">
                <a class="inner" href="<?php echo base_url('ro/servicii/radiologie'); ?>">
                    <img src="<?php echo media_url('servicii/landing/12-radiologie.jpg') ?>" alt="Radiologie">
                    <h3>Radiologie</h3>
                </a>
            </div>
        </div>
    </div>
</section>