<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Estetică dentară</h2>
                <p data-aos="fade-left">Estetica dentară are un impact puternic în relaționarea pe care o avem cu ceilalți dar și cu noi înșine.</p>
                <p data-aos="fade-left">Oriunde am fi, un zâmbet sănătos, frumos, armonios ne face ușor remarcați... este cartea noastră de vizită.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/estetica-dentara/0.jpg') ?>" alt="Estetică dentară">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Printr-un zâmbet îngrijit, estetic, transmiți faptul că îți pasă de tine și de ceilalți, te îngrijești și îți acorzi atenție. Estetica dentară s-a născut tocmai din această dorință a oamenilor de a transmite frumosul. Această specializare se adresează pacienților care își doresc dinți sănătoși și albi, armonizați cu fizionomia lor. Pentru aceasta trebuie acordat un interes sporit în principal: igienei dentare, simetriei naturale a zâmbetului, feței și culorii dinților.</p>
                <p data-aos="fade-left">Alături de estetică dentară, estetică facială contribuie la modul în care ești perceput. Aceasta presupune diverse tehnici prin care se creează un zâmbet frumos, definit de buze pline, o piele fină, îngrijită și lipsită de imperfecțiuni (diminuarea ridurilor, corecția conturului facial, remodelarea buzelor).</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Tehnicile folosite de noi în contribuția la estetică dentară și facială</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/estetica-dentara/1.jpg') ?>" alt="Estetică dentară">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/estetica-dentara/2.jpg') ?>" alt="Estetică dentară">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/estetica-dentara/3.jpg') ?>" alt="Estetică dentară">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down"><strong>Albirea dentară</strong> realizată prin intermediul a două tehnici: <strong>albirea endodontică</strong> și <strong>albirea tuturor dinților</strong>, în funcție de necesitățile pacientului.</p>
            <p data-aos="fade-down"><strong>Albirea tuturor dinților</strong> se efectuează în două etape principale:</p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">realizarea gutierelor necesare albirii</li>
                <li data-aos="fade-left" data-aos-delay="50">albirea propriu-zisă cu substanța destinată acestui scop.</li>
            </ul>
            <p data-aos="fade-down">Pacientul primește gutierele și substanța de albit, pentru a căror utilizare corespunzătoare la domiciliu i se face un instructaj. Datorită faptului că substanța va fi dozată în funcție de necesitatea și preferințele pacientului, nu vor apărea sensibilități dentare.</p>
            <p data-aos="fade-down"><strong>Modificarea zâmbetului</strong> prin transformarea celebrului “gummy smile”, care presupune descoperirea completă a dinților și a unei părți considerabile din gingie. Pentru această procedură a esteticii dentare utilizăm toxina botulinică, prin care construim, fără a fi invazivi, un zâmbet cu aspect natural fără descoperirea completă a dinților când zâmbești.</p>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Considerăm că estetica dentară este disciplina medicinii dentare dedicată artei de a reda zâmbetul natural, disciplină prin care un medic dentist își demonstreză în cel mai vizibil mod măiestria.</p>
                <p data-aos="fade-down">La fel ca în cazul tuturor procedurilor utilizate în cadrul clinicii noastre, atât în ceea ce privește estetica dentară dar cât și cea facială acordăm o atenție sporită materialelor și aparaturii folosite.</p>
                <p data-aos="fade-down">Cu ajutorul pozelor realizate în clinică putem realiza o simulare a viitorului zâmbet, înainte de realizarea procedurilor propriu zise.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/paul_zaharia.jpg'); ?>" alt="Paul Zaharia">
                        <p>Paul Zaharia</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/mario_chilom.jpg'); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sorina_copaci.jpg'); ?>" alt="Sorina Copaci">
                        <p>Sorina Copaci</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sergiu_buzatu.jpg'); ?>" alt="Sergiu Buzatu">
                        <p>Sergiu Buzatu</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/alexandra_majorosi.jpg'); ?>" alt="Alexandra Majorosi">
                        <p>Alexandra Majorosi</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>