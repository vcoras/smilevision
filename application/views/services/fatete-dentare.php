<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Fațete dentare</h2>
                <p data-aos="fade-left">Vrei un zâmbet mai strălucitor, mai armonios, sau pur și simplu diferit?</p>
                <p data-aos="fade-left">Fațetele dentare sunt soluția ideală. Sunt minim-invazive, extrem de rezistente și longevive. De asemenea pot schimba atât culoarea dintelui, dar mai ales forma și poziția acestuia, reprezentând, în unele cazuri clinice, o alternativă la aparatele dentare. Culoarea fațetelor este permanentă și nu se schimbă în timp.</p>
                <p data-aos="fade-left">Sunt perfect compatibile cu smalțul dintelui natural și pot fi realizate din ceramică sau compozit.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/fatete-dentare/0.jpg') ?>" alt="fatete dentare">
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de fațetare dentară pe care le utilizăm</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/fatete-dentare/1.jpg') ?>" alt="fatete dentare">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/fatete-dentare/2.jpg') ?>" alt="fatete dentare">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/fatete-dentare/3.jpg') ?>" alt="fatete dentare">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Realizarea fațetelor este o procedură complexă.</p>
            <p data-aos="fade-down">Fațetele dentare pot fi realizate <strong>direct</strong>, din compozit, respectiv <strong>indirect</strong>, în laboratorul de tehnică dentară.</p>
            <p data-aos="fade-down">Fațetarea directă se face folosind o rășină sintetică (<strong>compozit</strong>) care este modelată și lipită pe dinte pentru a-i îmbunătăți aspectul și este realizată de către medic în clinică.</p>
            <p data-aos="fade-down">Fațetarea indirectă se face folosind materiale ce imită cel mai fidel naturalețea dintelui (<strong>ceramică, compozit</strong>), iar fațetele sunt realizate în laboratorul de tehnică dentară. Aceste fațete se pot modela, colora, astfel încât să primiți fațetele “ideale”.</p>
            <p style="display:block;" data-aos="fade-down">Procedura de fațetare dentară poate fi de două tipuri:</p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">cu șlefuirea dinților - tehnică prin care se reduce dintele (se șlefuiește)</li>
                <li data-aos="fade-left" data-aos-delay="50">fără șlefuirea dinților (no-prep) - tehnică prin care dintele nu se reduce (nu se șlefuiește)</li>
            </ul>
            <p data-aos="fade-down">Procedura este adaptată în funcție de dantura fiecărui pacient.</p>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Planificăm în detaliu înainte de a începe orice manoperă pentru ca tu să știi dinainte care este rezultatul final și să fii întru totul mulțumit și satisfăcut cu noul tău zâmbet.</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">Realizăm o serie de fotografii pentru studiul facial și dentar.</li>
                    <li data-aos="fade-left" data-aos-delay="50">Utilizăm tehnologia digitală (Digital Smile Design) și îți arătăm care va fi dimensiunea, poziția ideală, forma, textura și culoarea fiecărui nou dinte.</li>
                    <li data-aos="fade-left" data-aos-delay="100">Realizăm în clinică un MOCK-up (test), care se aplică direct pe dinții naturali neșlefuiți. Pentru câteva zile poți testa să vezi cum te simți, dacă este ce îți dorești și ce ajustări sunt necesare urmând apoi să aplicăm lucrarea finală.</li>
                </ul>
                <p data-aos="fade-down">Pentru că dispunem de propriul laborator dentar digital, putem să obținem fațetele în numai câteva ore, totul cu ajutorul designului și a frezării asistate de computer.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/paul_zaharia.jpg'); ?>" alt="Paul Zaharia">
                        <p>Paul Zaharia</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/mario_chilom.jpg'); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sorina_copaci.jpg'); ?>" alt="Sorina Copaci">
                        <p>Sorina Copaci</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>