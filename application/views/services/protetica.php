<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Protetică</h2>
                <p data-aos="fade-left">Protetica dentară este ramura stomatologiei care se ocupă de înlocuirea dinților lipsă.</p>
                <p data-aos="fade-left">Intervențiile de protetică dentară restabilesc integritatea dentară afectată de leziuni sau degradare, folosind lucrări protetice (proteze, coroane, încrustații dentare, fațete, punți dentare).</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/protetica/0.jpg') ?>" alt="Protetica">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Cu ajutorul aparaturii digitale de ultimă generație (scanner intraoral, mașini de frezat CAD-CAM, imprimantă 3D, software pentru design-ul lucrărilor protetice etc.) obținem toată gama de lucrări protetice perfect adaptate, precise și într-un timp foarte scurt.</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de protetică utilizate în cabinetul nostru</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/protetica/1.jpg') ?>" alt="Protetica">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/protetica/2.jpg') ?>" alt="Protetica">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/protetica/3.jpg') ?>" alt="Protetica">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down"><strong>În urma tratamentului endodontic</strong> aplicat unui dinte, rezistența lui este scăzută. Din această cauză, este recomandată aplicarea unei coroane protetice peste coroană clinică a dintelui.</p>
            <p data-aos="fade-down"><strong>Încrustații dentare Inlay / Onlay</strong> sunt o alternativă mai estetică (datorită culorii translucide, similară dintelui natural) și mai rezistentă (datorită materialului din care sunt confecționate) pentru obturațiile dentare. Încrustațiile dentare îmbracă perfect dintele deoarece sunt create în laborator, fiind 100% personalizate.</p>
            <p data-aos="fade-down"><strong>În cazul dinților lipsă</strong> se realizează punți dentare.</p>
            <p data-aos="fade-down">Întotdeauna preferăm protetica pe implanturi în detrimentul proteticii clasice, în care se șlefuiesc dinții naturali. Șlefuirea dinților naturali nu este singurul argument contra proteticii clasice, ci și faptul că dinții absenți înlocuiți în protezarea clasică nu au sprijin pe gingie astfel încât forțele se distribuie doar dinților naturali și aceștia se suprasolicită în timp.</p>
            <p data-aos="fade-down"><strong>Protetica pe implanturi</strong> este reprezentată de realizarea coroanelor pe implanturi. După vindecarea completă a implantului propriu-zis, se realizează coroana definitivă. Aceasta coroană se realizează în așa fel încât să fie asemănătoare dintelui natural, atât din punct de vedere estetic, cât și funcțional.</p>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Tehnicile de protetică utilizate reflectă iscusința, imaginația medicului stomatolog, experiența și tehnologiile  pe care o folosim.</p>
                <p data-aos="fade-down">Cu ajutorul tehnicilor și tehnologiilor de actualitate  realizăm soluții personalizate folosind:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0"><strong>Digital Smile Design</strong> care ne permite ca, în urma analizei preferințelor și necesităților pacientului, să realizăm simularea viitoarei lucrări protetice.</li>
                    <li data-aos="fade-left" data-aos-delay="50"><strong>3Shape Trios</strong>, cel mai performant scanner intraoral din lume, care ne permite efectuarea amprentelor digitale de înaltă precizie într-un timp foarte scurt</li>
                    <li data-aos="fade-left" data-aos-delay="100"><strong>propriul laborator dentar digital</strong>, care ne ajută să obținem unele lucrări protetice în termen de numai câteva ore, totul cu ajutorul designului și a frezării asistate de computer.</li>
                </ul>
                <p data-aos="fade-down">Pe lângă acestea, în realizarea lucrărilor de protetică, recomandăm:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">realizarea lucrărilor protetice provizorii în cabinet (pacientul nu va pleca niciodată cu dinții neacoperiți, neprotejați)</li>
                    <li data-aos="fade-left" data-aos-delay="50">realizarea unor coroane care imită în detaliu anatomia și morfologia dintelui natural, în colaborare cu tehnicianul și cu ajutorul materialelor de ultimă generație pe care le utilizăm</li>
                </ul>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/paul_zaharia.jpg'); ?>" alt="Paul Zaharia">
                        <p>Paul Zaharia</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/mario_chilom.jpg'); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sorina_copaci.jpg'); ?>" alt="Sorina Copaci">
                        <p>Sorina Copaci</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sergiu_buzatu.jpg'); ?>" alt="Sergiu Buzatu">
                        <p>Sergiu Buzatu</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/alexandra_majorosi.jpg'); ?>" alt="Alexandra Majorosi">
                        <p>Alexandra Majorosi</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>