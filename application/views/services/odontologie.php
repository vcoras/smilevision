<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Odontologie</h2>
                <p data-aos="fade-left">Odontologia este specialitatea stomatologică care tratează leziunile carioase și cele necarioase (fracturi, distrofii, leziuni de uzură dentară). </p>
                <p data-aos="fade-left">În mod frecvent, dinții sunt distruși de carii. Cu toții am învățat de mici cum acestea penetrează structura dintelui și duc la modificări anatomice la suprafața lui.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url("servicii/odontologie/0.jpg") ?>" alt="Odontologie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Modelarea suprafețelor dinților este importantă, atât din punct de vedere funcțional cât și estetic. Dacă nu se realizează reconstituirea anatomică a dinților, apar deficiențe în ocluzia și masticația pacientului ceea ce poate duce la o digestie.</p>
                <p data-aos="fade-left">Reconstituirile dentare pot fi directe (obturațiile) sau indirecte (incrustațiile - inlay/onlay/overlay).</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de restaurare odontologică pe care le utilizăm</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/odontologie/1.jpg') ?>" alt="Odontologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/odontologie/2.jpg') ?>" alt="Odontologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/odontologie/3.jpg') ?>" alt="Odontologie">
            </div>
        </div>
        <div class="row content">
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">Obturații fizionomice</li>
                <li data-aos="fade-left" data-aos-delay="50">Incrustații Inlay / Onlay / Overlay</li>
                <li data-aos="fade-left" data-aos-delay="100">Fațetări directe</li>
            </ul>
            <p data-aos="fade-down">Cu cât leziunea dintelui este descoperită mai devreme, cu atât cresc șansele ca dintele să rămână cât mai aproape de forma inițială, fără intervenții de odontologie majore. Nu degeaba se recomandă controalele periodice pentru depistarea schimbărilor, prevenirea și tratarea cariilor în faza incipientă.</p>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <div class="row mini-gallery no-gutter mb-4">
                    <div class="col-md-6" data-aos="fade-down" data-aos-delay="0">
                        <img src="<?php echo media_url('servicii/odontologie/4.jpg') ?>" alt="Odontologie">
                    </div>
                    <div class="col-md-6" data-aos="fade-down" data-aos-delay="50">
                        <img src="<?php echo media_url('servicii/odontologie/5.jpg') ?>" alt="Odontologie">
                    </div>
                </div>
                <p data-aos="fade-down">„Modelarea unui dinte frontal este teza de doctorat a fiecărui stomatolog”.</p>
                <p data-aos="fade-down">Reușita intervenției se reflectă în obturarea dinților frontali și aceasta poate fi considerată o carte de vizită pentru noi. Acest proces trebuie realizat în așa fel încât să nu se observe faptul că pacientul este posesorul unei obturații pe dintele respectiv. De aceea, în cadrul intervenției de odontologie folosim materiale de reconstituire de ultimă generație, cu o rezistență crescută în timp.</p>
                <p data-aos="fade-down">Rezultatul nu întârzie să apară: prin folosirea acestor materiale și datorită tehnicii pe care o utilizăm, estetica dintelui, în urma intervenției, rămâne identică cu cea a dintelui natural.</p>
                <p data-aos="fade-down">Trebuie menționat faptul că în rezultatele pe care le avem, o contribuție majoră o au și cursurile de perfecționare a tehnicilor de realizare a obturațiilor, la care echipa noastră participă constant.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url("servicii/medici/sorina_copaci.jpg"); ?>" alt="Sorina Copaci">
                        <p>Sorina Copaci</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url("servicii/medici/mario_chilom.jpg"); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url("servicii/medici/sergiu_buzatu.jpg"); ?>" alt="Sergiu Buzatu">
                        <p>Sergiu Buzatu</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>