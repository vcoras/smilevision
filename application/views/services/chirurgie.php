<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Chirurgie</h2>
                <p data-aos="fade-left">Tratamentele de chirurgie dento-alveolară sunt necesare atunci când trebuie salvați sau îndepărtați dinții afectați de complicații severe. Aceste tipuri de tratamente pot de asemenea pregăti terenul pentru refacerile protetice ulterioare sau pentru inserția implanturilor.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/chirurgie/0.jpg') ?>" alt="chirurgie">
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de chirurgie orală pe care le utilizăm</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/chirurgie/1.jpg') ?>" alt="chirurgie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/chirurgie/2.jpg') ?>" alt="chirurgie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/chirurgie/3.jpg') ?>" alt="chirurgie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Chirurgia dento-alveolară implică proceduri complexe, fiind un domeniu vast al stomatologiei. În clinica noastră vei putea beneficia de toate intervențiile de chirurgie orală, printre care:</p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">extracții dentare</li>
                <li data-aos="fade-left" data-aos-delay="50">descoperiri chirurgicale ale dinților incluși</li>
                <li data-aos="fade-left" data-aos-delay="100">chirurgie parodontală – gingivectomii: chiuretajul subgingival în câmp închis / deschis, alungiri coronare etc.</li>
                <li data-aos="fade-left" data-aos-delay="150">chirurgie endodontică – rezecții apicale: îndepărtarea vârfului rădăcinii și a țesutului afectat din jurul acestuia</li>
                <li data-aos="fade-left" data-aos-delay="200">chistectomii – extirparea chisturilor rămase intra-osos</li>
                <li data-aos="fade-left" data-aos-delay="250">chirurgie preprotetică – regularizarea crestelor edentate în vederea protezării</li>
                <li data-aos="fade-left" data-aos-delay="300">implantologie - tehnici de augmentare osoasa orizontală și verticală</li>
            </ul>
            <p data-aos="fade-down">Toate tratamentele sunt efectuate cu ajutorul lupelor / microscopului, pentru a garanta o mare precizie.</p>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Când spunem chirurgie dento-alveolară, probabil că ne gândim la procese complexe și dureroase. Din fericire, în practica noastră chirurgicală durerea este aproape inexistentă și ne place să nu complicăm lucrurile când nu este cazul. De aceea folosim materiale și tehnici care ajută la vindecarea rapidă și sănătoasă a plăgilor obținute în urma manoperelor chirurgicale orale și anumite substanțe care împiedică apariția durerii, umflarea și colorarea tegumentelor postchirurgical.</p>
                <p data-aos="fade-down">Noi vom face tot posibilul ca pentru tine să pară simplu, indiferent de gradul de dificultate al cazului:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">îți vom explica întotdeauna care este tratamentul indicat și de ce îl recomandăm</li>
                    <li data-aos="fade-left" data-aos-delay="50">îți vom prezenta toate argumentele pro și contra, iar tu vei avea cuvântul decisiv.</li>
                </ul>
                <p data-aos="fade-down">La noi nu se aplică sintagma “no pain, no gain”, de aceea te așteptăm în clinica noastră pentru a-ți dovedi asta. Vei simți că lucrăm, dar fără urme de durere.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/paul_zaharia.jpg'); ?>" alt="Paul Zaharia">
                        <p>Paul Zaharia</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>