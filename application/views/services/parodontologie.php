<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Parodontologie</h2>
                <p data-aos="fade-left">Odată cu modernizarea umanității, se complică și afecțiunile omului.</p>
                <p data-aos="fade-left">S-a observat faptul că din ce în ce mai mulți pacienți prezintă afecțiuni ale parodonțiului marginal, implicit ale dinților.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/parodontologie/0.jpg') ?>" alt="Parodontologie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left"><strong>Cele mai frecvente cauze ale bolii parodontale sunt reprezentate de:</strong></p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">tartrul dentar</li>
                    <li data-aos="fade-left" data-aos-delay="50">absența dinților de pe arcade</li>
                    <li data-aos="fade-left" data-aos-delay="100">restaurările protetice maladaptate</li>
                    <li data-aos="fade-left" data-aos-delay="150">obiceiuri vicioase (fumatul)</li>
                    <li data-aos="fade-left" data-aos-delay="200">afecțiuni generale și chiar unele medicamente</li>
                </ul>
                <p data-aos="fade-left">Boala parodontală este sistematizată în mai multe etape, evoluția ei fiind de la simplă la complicată. Cu cât este mai devreme descoperită, cu atât este mai simplu de tratat. Principalele semne clinice care ar trebui să vă dea de gândit sunt reprezentate de:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">sângerarea gingiilor la periaj</li>
                    <li data-aos="fade-left" data-aos-delay="50">schimbarea culorii gingiilor de la roz spre roșu violaceu</li>
                    <li data-aos="fade-left" data-aos-delay="100">mărirea volumului gingiilor</li>
                    <li data-aos="fade-left" data-aos-delay="150">apariția aspectului de coajă de portocală</li>
                    <li data-aos="fade-left" data-aos-delay="200">retragerea gingiilor și mobilitatea dinților</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile pe care le utilizăm în tratamentul parodontologic</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/parodontologie/1.jpg') ?>" alt="Parodontologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/parodontologie/2.jpg') ?>" alt="Parodontologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/parodontologie/3.jpg') ?>" alt="Parodontologie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Tratamentul parodontopatiilor marginale este relativ simplu, dar necesită răbdare și consecvență din partea dumneavoastră. Prima etapă constă în eliminarea factorilor cauzali, urmată de etapa întreținerii și apoi vindecarea propriu-zisă cu refacerea structurilor afectate.</p>
            <p data-aos="fade-down">Pe parcursul tratamentului parodontal, monitorizăm evoluția germenilor din pungile parodontale. Acest lucru ne poate confirma/ infirma evoluția procesului de vindecare. La fiecare vizită stomatologică în cadrul terapiei bolii parodontale, monitorizăm schimbările sănătății orale, pentru a verifica în permanență evoluția vindecării.</p>
            <p data-aos="fade-down">Alături de fișele în care se notează evoluția bolii dumneavoastră, se vor găsi fotografii cu situația inițială, pe parcursul tratamentului și cea finală. Astfel, datorită evoluției stomatologiei digitale, oricând vă putem pune la dispoziție, cu exactitate, istoricul bolii dumneavoastră.</p>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Considerăm că tratamentele parodontale trebuie adaptate fiecărui pacient în parte. De aceea, analizăm fiecare caz în parte în privința depistării germenilor (există mai multe specii de bacterii prezente în boala parodontală) și aplicăm un tratament corespunzător (nu toate bacteriile sunt sensibile la un anumit tratament, motiv pentru care tratamentul aplicat trebuie să fie specific).</p>
                <p data-aos="fade-down">Pentru eficientizarea tratamentului și o experiență cât mai ușoară în clinica noastră, am elaborat și menținem un program sistematizat în etape pentru tratarea bolii parodontale. Pornim de la analizele speciale pentru detectarea parodontozei, făcute în Germania, continuăm cu elaborarea schemei personalizate de tratament și urmărim în timp parametrii prin controale și analize periodice.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/paul_zaharia.jpg'); ?>" alt="Paul Zaharia">
                        <p>Paul Zaharia</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/mario_chilom.jpg'); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/sorina_copaci.jpg'); ?>" alt="Sorina Copaci">
                        <p>Sorina Copaci</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>