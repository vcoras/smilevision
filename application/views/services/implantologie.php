<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Implantologie</h2>
                <p data-aos="fade-left">Edentația sau pierderea dinților reprezintă una dintre problemele frecvente cu care se confruntă pacienții vârstnici dar și persoanele tinere. Dacă în trecut era mai dificil să înlocuiești un dinte, mai mulți, sau toți dinții lipsă, în prezent, stomatologia modernă oferă pacienților posibilitatea de a înlocui orice dinte pierdut.</p>
                <p data-aos="fade-left"><strong>Implantul dentar este cea mai bună soluție pentru înlocuirea unuia sau mai multor dinți lipsă. Actualele tehnici ne permit să realizăm implantarea unui dinte artificial indiferent de situația care a condus la pierderea celui natural.</strong></p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/implantologie/0.jpg') ?>" alt="Implantologie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Înaintea de realizarea unei intervenții ce presupune inserarea unuia sau mai multor implanturi, are loc o consultație amănunțită în cadrul căreia se va realiza o radiografie tridimensională, fotografii ale situației inițiale și o scanare a cavității bucale. Pe baza acestora are loc o discuție unde ți se va explica care este particularitatea cazului tău, ce soluții există, care sunt etapele și ce presupune intervenția. De asemenea, vom discuta care sunt recomandările înainte și după, ce alte alternative sunt, care sunt costurile și cât durează. Împreună vom creea un plan de tratament detaliat.</p>
                <p data-aos="fade-left">Astfel tu vei ști în permanență care sunt etapele pe care va trebui să le parcurgi și ce presupun acestea.</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de implantologie pe care le utilizăm</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/implantologie/1.jpg') ?>" alt="Implantologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/implantologie/2.jpg') ?>" alt="Implantologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/implantologie/3.jpg') ?>" alt="Implantologie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Implantul dentar este un dispozitiv artificial din titan confecționat pentru a înlocui rădăcina dintelui care lipsește și se folosește ca suport pentru coroane, proteze fixe și mobile. Un implant dentar are aceeași funcție ca rădăcina unui dinte natural, oferind sprijin solid pentru un dinte nou. Astfel poți redobândi un zâmbet funcțional și sănătos.</p>
            <p data-aos="fade-down">Recomandăm implanturile ca cele mai bune opțiuni pentru înlocuirea dinților pierduți, deoarece:</p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">oferă o estetică și o funcționalitate naturală</li>
                <li data-aos="fade-left" data-aos-delay="50">nu necesită prepararea dinților vecini, ca în cazul protezării clasice</li>
                <li data-aos="fade-left" data-aos-delay="100">prezența implantului nu-i permite osului și gingiei să se resoarbă</li>
            </ul>
            <p data-aos="fade-down">Soluțiile protetice de tipul implantului dentar pot veni în multe variante, iar pe baza consultației și a radiografiei tridimensionale vom stabili împreună care este cea mai bună alegere. Dacă osul în care urmează să fie integrat implantul nu este suficient se va apela la tehnici de reconstituire osoasă.</p>
        </div>
    </div>
</div>
<div class="section-implants">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Tipuri de implanturi folosite în clinică</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-4 d-flex align-items-stretch">
                <div class="card" data-aos="fade-down" data-aos-delay="0">
                    <img class="card-img-top" src="<?php echo media_url('servicii/implantologie/megagen.jpg') ?>" alt="Megagen">
                    <div class="card-body">
                        <p class="card-text">Sistemul de implant MegaGen îndeplinește cele mai înalte standarde de calitate. Implanturile MegaGen implică un tratament minim invaziv. Cu implanturile MegaGen sunt obținute rezultate bune într-o perioadă mai scurtă de timp.</p>
                        <p class="card-text">Ele sunt realizate dintr-un aliaj din titan, un metal biocompatibil cu organismul uman și care nu provoacă reacții alergice. Fuziunea cu osul se realizează rapid datorită suprafeței implantului acoperită cu ioni de calciu. </p>
                        <p class="card-text">De asemenea, designul asigură o stabilitate excepțional de bună implantului.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex align-items-stretch">
                <div class="card" data-aos="fade-down" data-aos-delay="50">
                    <img class="card-img-top" src="<?php echo media_url('servicii/implantologie/dentium.jpg') ?>" alt="Dentium">
                    <div class="card-body">
                        <p class="card-text">Implantul dentar Dentium este produs în Coreea, fiind distribuit în peste 70 țări și acreditat ISO. Îndeplinește cele mai înalte standarde de calitate impuse în materie de dispozitive medicale.</p>
                        <p class="card-text">Se remarcă atât prin design, cât și prin funcționalitate, oferind pacienților osteointegrare superioară, demonstrată în numeroasele studii clinice realizate până în prezent. </p>
                        <p class="card-text">Suprafața S.L.A asigură o conexiune perfectă între implant și os, contribuind la prezervarea țesuturilor.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex align-items-stretch">
                <div class="card" data-aos="fade-down" data-aos-delay="100">
                    <img class="card-img-top" src="<?php echo media_url('servicii/implantologie/bredent.jpg') ?>" alt="Bredent">
                    <div class="card-body">
                        <p class="card-text">Sistemul Bredent este lider mondial în ceea ce privește implanturile dentare din clasa premium, fiind fabricat în Germania. Acesta oferă garanția unor materiale de calitate, ce sunt ușor acceptate de către organism, dar în același timp ne ajută să oferim soluții de tratament în cazuri complicate sau ușoare. Tipurile de implant oferite de Bredent se pretează tuturor cazurilor posibile.</p>
                        <p class="card-text">Sistemul de implanturi dentare de la Bredent oferă mai multe tipuri de tratament, inclusiv renumitul sistem Fast and Fixed, folosit pentru a înlocui proteza mobilă cu dinți ficși în 24 de ore.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-implant-systems" id="implanturi">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Sisteme de implanturi pentru toți dinții lipsă</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-8">
                <p data-aos="fade-down"><strong>Sky fast & fixed de la Bredent</strong></p>
                <p data-aos="fade-down">Soluția de protezare de tip Sky fast&fixed de la Bredent este o metodă de reconstrucție totală a danturii ce se adresează pacienților fără dinți sau cu dinți bolnavi ce nu mai pot fi salvați. </p>
                <p data-aos="fade-down">Procedura presupune inserarea implanturilor dentare și fixarea lucrării provizorii în 24 de ore. Avantajul folosirii soluției de protezare tip SKY fast & fixed, de la Bredent, este că avem posibilitatea de a oferi pacienţilor, într-o singură zi, dinţi ficşi cu care pot mânca imediat după operaţie. </p>
                <p data-aos="fade-down">Osteointegrarea implanturilor dentare se face cu ușurință, iar tehnica PRF pe care o folosim asigură o vindecare mai rapidă decât în mod obișnuit.</p>
            </div>
            <div class="col-md-4">
                <img data-aos="fade-down" style="width:100%; height:auto;" src="<?php echo media_url('servicii/implantologie/4.jpg') ?>" alt="Implantologie">
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-12">
                <img class="youtube-video" data-video="https://www.youtube.com/embed/pv3OqA__Osc?autoplay=1" data-aos="fade-down" src="<?php echo media_url('servicii/implantologie/5.jpg') ?>" alt="Implantologie">
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-md-12">
                <br>
                <p data-aos="fade-down"><strong>All-on-4 / All-on-6</strong></p>
                <p data-aos="fade-down">Tehnica All-on-4/6 oferă pacienților o lucrare protetică complet fixă în ziua intervenției, sau în cel mai scurt timp.</p>
                <p data-aos="fade-down">All-on-4/6 sunt soluții absolut stabile și cu un foarte bun prognostic pe termen lung.</p>
                <p data-aos="fade-down">Conform protocolului original All-on-4/6, simultan cu inserarea implanturilor dentare are loc și fixarea protezei provizorii. După osteointegrarea implanturilor se va aplica lucrarea finală iar în tot acest timp pacientul poate mânca, zâmbi și avea o viață absolut normală.</p>

                <img class="youtube-video" data-video="https://www.youtube.com/embed/IW4VFtb7OHU?autoplay=1"  data-aos="fade-down" src="<?php echo media_url('servicii/implantologie/6.jpg') ?>" alt="Implantologie">
            </div>
        </div>
    </div>
</div>
<div class="section-banner">
    <div class="container" data-aos="flip-up">
        <p>Sistemul de implanturi dentare potrivit depinde de problemele dentare cu care se confruntă pacientul. Recomandarea se va face în funcție de volumul țesutului osos observat în urma investigațiilor radiologice, de restul danturii, dar și de nevoile pacientului.</p>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Cu ajutorul tehnicilor pe care le utilizăm în toate procedurile de implantologie dentară, inserarea unui implant este ușor de suportat, iar recuperarea este rapidă. În funcție de situația clinică inițială și duritatea osului, în unele cazuri veți putea pleca după intervenția de inserare a implanturilor cu dinți provizorii, dinți cu care puteți zâmbi, mânca și vorbi.</p>
                <p data-aos="fade-down">Modalitățile prin care realizăm inserarea implantului ne ajută să redăm aspectul natural al dinților, nu doar la nivel estetic, ci și legat de funcționalitatea acestora. Și asta datorită pregătirii temeinice și a experienței de peste 10 ani în acest domeniu.</p>
                <p data-aos="fade-down">În clinica Smile Vision, intervențiile chirurgicale de inserare a implanturilor Megagen, Bredent, Dentium, MIS sunt efectuate cu succes de către Dr. Mario Chilom și Dr. Paul Zaharia.</p>
                <p data-aos="fade-down">Garanția pentru orice lucrare pe implant este de 5 ani cu respectarea recomandărilor și consultului regulat la fiecare 6 luni.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/paul_zaharia.jpg'); ?>" alt="Paul Zaharia">
                        <p>Paul Zaharia</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/mario_chilom.jpg'); ?>" alt="Mario Chilom">
                        <p>Mario Chilom</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>