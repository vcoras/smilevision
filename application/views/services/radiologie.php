<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Radiologie</h2>
                <p data-aos="fade-left">Radiografiile dentare sunt imagini ale dinților pe care le folosim pentru a vă evalua sănătatea orală. </p>
                <p data-aos="fade-left">În funcție de scopul pentru care sunt efectuate, există mai multe feluri de radiografii dentare. Cea mai utilizată este <strong>radiografia retro-alveolară</strong>. Această investigație se folosește pentru a fi identificate cariile dentare, chisturi, abcese dentare, dar și fracturi.</p>
                <p data-aos="fade-left">Și unele infecții pot fi bine vizualizate în urma unei astfel de investigații imagistice.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url("servicii/radiologie/0.jpg") ?>" alt="Radiologie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Un alt tip de radiografie dentară frecvent utilizată este cea panoramică, care oferă o vedere de ansamblu a dinților, a sinusurilor, a articulațiilor temporo-mandibulare. Cu ajutorul ei pot fi descoperite și tumori situate la nivelul cavității bucale.</p>
                <p data-aos="fade-left"><strong>Tomografia dentară computerizată</strong>, sau CBCT-ul, reprezintă o reproducere tridimensională a dinților, oaselor maxilare și a structurilor din cavitatea bucală, care ne ajută la stabilirea unui diagnostic precis și un plan de tratament optim.</p>
                <p data-aos="fade-left">Medicul specialist poate cere o astfel de investigație:</p>
                <ul>
                    <li data-aos="fade-left" data-aos-delay="0">în cazul unui tratament endodontic complicat, pentru a vizualiza canalele accesorii, procese patologice la nivelul rădăcinilor sau chiar fracturi</li>
                    <li data-aos="fade-left" data-aos-delay="50">înaintea unui implant dentar, pentru a măsura cu exactitate dimensiunile țesuturilor osoase și/sau a sinusurilor maxilare</li>
                    <li data-aos="fade-left" data-aos-delay="100">în cazul unor complicații ale molarilor de minte sau anomalii de erupție</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de radiologie utilizate în clinica noastră</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/radiologie/1.jpg') ?>" alt="Radiologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/radiologie/2.jpg') ?>" alt="Radiologie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/radiologie/3.jpg') ?>" alt="Radiologie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down"><strong>Planmeca ProX</strong> și <strong>Satelec</strong>, aparatele noastre pentru radiografiile retro-alveolare ne oferă o poziționare ușoară și precisă, un proces imagistic simplu și imagini de înaltă calitate în rezoluție înaltă.</p>
            <p data-aos="fade-down">Având o doză de radiație extrem de mică datorită senzorilor digitali care reduc timpul de expunere, aparatele de radiologie ne sunt indispensabile în stabilirea diagnosticelor dar și la verificarea și în monitorizarea rezultatelor tratamentelor efectuate pentru 1-3 dinți.</p>
        </div>
        <div class="row no-gutter mb-4 mt-4">
            <div class="col-md-12" data-aos="fade-down">
                <img style="width:100%; height:auto;" src="<?php echo media_url('servicii/radiologie/4.jpg') ?>" alt="Radiologie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Radiologie digitală panoramică <strong>Morita Veraviewepocs 3D R100</strong> ce ne permite o calitate strălucitoare a imaginii și reducerea eficientă a dozei. Câmpul de vedere complet al arcului 3D Reuleaux al acestei unități se potrivește strâns cu forma arcului dentar natural. Reduce doza prin excluderea zonelor din afara regiunii de interes și permite o scanare completă a maxilarului și / sau a mandibulei.</p>
        </div>
        <div class="row no-gutter mt-4" data-aos="fade-down">
            <div class="col-md-12">
                <img style="width:100%; height:auto;" src="<?php echo media_url('servicii/radiologie/5.jpg') ?>" alt="Radiologie">
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row">
            <div class="col-md-12">Toate investigațiile radiologice se pot realiza direct în clinică nefiind necesară deplasarea către alte centre de imagistică.</div>
        </div>
    </div>
</div>