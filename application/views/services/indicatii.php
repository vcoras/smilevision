<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Indicații</h1>
        <div class="accordion" id="listaIndicatii">
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#aparatDentarFix" aria-expanded="true" aria-controls="collapseOne">
                            Purtarea unui aparat dentar fix
                        </button>
                    </h2>
                </div>

                <div id="aparatDentarFix" class="collapse show" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p data-aos="fade-down"><strong>Purtarea unui aparat dentar fix – ce trebuie să faci după aplicarea lui</strong></p>
                        <p data-aos="fade-down">1. Primele zile după aplicarea aparatului fix sunt cele mai dificile. După aplicarea aparatului în cavitatea bucala, apare senzația de tensiune la nivelul dinților și în primele zile pot apărea chiar dureri. Durerea e de intensitate redusă și e un fenomen normal. Daca intensitatea e mai mare, se pot lua analgezice ușoare. Pentru ca adaptarea sa fie cat mai ușoară va recomandăm următoarele:</p>
                        <ul>
                            <li data-aos="fade-left" data-aos-delay="0">Folosiți ceara de protecție pe toate zonele aparatului care jenează buzele, obrajii sau limba.</li>
                            <li data-aos="fade-left" data-aos-delay="50">Dacă tensiunea resimțita în primele zile este mare, puteți lua un antiinflamator pentru a o reduce durerea (Nurofen, Algocalmin, Paracetamol).<br><strong>ATENȚIE! SUB NICIO FORMA NU DEPĂȘIȚI DOZA ZILNICĂ INDICATĂ!</strong></li>
                            <li data-aos="fade-left" data-aos-delay="100">Folosiți o alimentatie de consistență mai redusă, în special în primele zile.</li>
                            <li data-aos="fade-left" data-aos-delay="150">Respectați indicațiile privind igiena aparatului și reîncercati ca ele sa devină în scurt timp o rutină.</li>
                        </ul>
                        <p data-aos="fade-down">2. Evitați alimentele dure sau lipicioase ce pot deteriora aparatul (caramele, alune, fistic, bake rolls, covrigi, fructe tari). Consecințele nerespectării acestor indicații fiind prelungirea în timp a tratamentului și costuri financiare mai mari.</p>
                        <p data-aos="fade-down">3. Incizia alimentelor cu dinții frontali trebuie evitată- acestea se rup sau se taie în bucăți și se mesteca pe dinții posteriori.</p>
                        <p data-aos="fade-down">4. În unele etape de tratament este posibil ca medicul sa va indice purtarea unor elastice – respectați indicațiile privind modul de aplicare și numărul de ore de purtare zilnică. Nu mancati cu elasticele aplicate.</p>
                        <p data-aos="fade-down">5. În cazul în care se desprinde un bracket, prima masura e sa incercam în fața oglinzii sa îl localizâm. În cele mai multe cazuri el ramane atașat pe arc și tot ce poate face e sa se plimbe între cei doi brackets vecini. Nu trebuie sa ne ingrijorăm. Dacă e prins cu o ligatura de sarma nu are rost sa incercati sa il îndepărtați pentru ca va fi foarte greu. Cel mai bine il fixâm pe arc cu ceara de protecție. Dacă legătura sau modulul elastic s-au rupt, puteți încerca să le indepărtati. Pentru asta trebuie deplasat între doi dinți vecini, acolo spațiul va fi mai mare și va fi mai ușor de scos de pe arc. Totusi, dacă aceasta manevră pare dificilă, rămăne varianta de a-l fixa cu ceara.</p>
                        <p data-aos="fade-down">Dacă s-a desprins ultimul bracket din spate, s-ar putea ca acul sa jeneze la capătul lui. Dacă se intampla asta si nu puteți ajunge prea curând la cabinet sau este perioada de vacanta, în cazul în care este absolut necesar, se poate taia arcul cu o forfecuta sau mai bine cu o unghiera imediat în spatele ultimului bracket nedesprins.</p>
                        <p data-aos="fade-down">Atenție!!! Ceara de protecție primită la inceputul tratamentului nu se găseste in farmacii sau magazine. Cel mai bine vă asigurați la fiecare control că aveți suficientă , în caz de urgență, o puteți înlocui cu gumă de mestecat sau cu ceara de la o lumanare moale.</p>
                        <p data-aos="fade-down">6. Capătul ligaturilor metalice este de obicei răsucit sub arc. Dacă în cursul periajului acesta se deplaseaza si devine iritant pentru obraz, încercați sa impingeti la loc cu o pensetă sau scobitoare.</p>
                        <p data-aos="fade-down">7. In special la inceputul tratamentului, arcurile flexibile si subtiri se pot deplasa, iar capatul acestora va poate jena. Încercați sa il misca-ti înapoi sau, dacă nu este posibil și se intampla acest lucru în weekend sau vacanta, cu o forfecuta sau unghiera îl puteți gestiona și indeparta.</p>
                        <br>
                        <p data-aos="fade-down"><strong>Sfaturi privind igiena aparatelor dentare fixe</strong></p>
                        <p data-aos="fade-down">Rezultatele, de multe ori spectaculoase ale tratamentului ortodontic pot fi însă umbrite de o igiena precara pe perioada purtarii bracketilor. Astfel, depunerea de placa bacteriana poate duce la apariția cariilor sau zonelor albe de demineralizare. Aceste pete albe sunt foarte greu de indepartat, prin remineralizari succesive, nu intotdeauna insotite de succes. Singurele zone rămase afectate sunt cele protejate chiar de prezenta bracketilor.</p>
                        <p data-aos="fade-down">1. Periajul dentar este foarte important și trebuie făcut corect și meticulos (minimum 3 minute).</p>
                        <p data-aos="fade-down">2. Folosiți o periuta cu perii moi (SOFT sau ULTRA SOFT). Este indicat periajul după fiecare masa si, bineinteles, dimineața și seara. Cand nu aveti periuta la indemana dupa ce mancati, puteti clati folosind apa de gura sau măcar apa obișnuită. Periati suprafetele dentare fără aparat în mod obișnuit. Fata externă a dintilor unde sunt și bracketii trebuie periata cu mișcări orizontale, insistand în zona dintre bracket și gingie.</p>
                        <p data-aos="fade-down">3. Folosiți periute interdentare pentru zonele mai greu accesibile (suprafețele dentare de sub arc). Alegeți periute cu partea activa mai mare pentru a putea fi eficiente.</p>
                        <p data-aos="fade-down">4. Chiar dacă este mai greu, ata dentara poate fi folosită cu succes și atunci când purtați aparat. Rugați medicul sau asistenta sa va ajute la început.</p>
                        <br>
                        <p data-aos="fade-down">Pentru orice întrebare sau nelamurire, va rugăm să ne <a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">contactați</a> din timp. Echipa noastra este pregatită și dispusă să vă ofere consultanță atât în timpul, cât și în urma oricărui tratament stomatologic.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#lucrariDentare" aria-expanded="true" aria-controls="collapseOne">
                            Indicații în timpul lucrărilor dentare
                        </button>
                    </h2>
                </div>

                <div id="lucrariDentare" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>Lucrări dentare – cimentarea provizorie</strong></p>
                        <p data-aos="fade-down">Până la finalizarea lucrării dentare definitive, vă recomandăm sa purtați lucrări dentare provizorii. Perioada de probă cu dinți provizorii, are un rol important în acomodarea cu noile lucrări dentare.</p>
                        <p data-aos="fade-down"><strong>Masticația</strong>: Evitați masticația cel puțin o ora după tratament, pentru a permite priza (întărirea) completă a cimentului provizoriu. În cazul în care în ședința de tratament s-a făcut anestezie locală, evitați masticația pana cand semnalele anesteziei dispar complet. Evitați alimentele dure sau lipicioase și gumă de mestecat.</p>
                        
                        <p data-aos="fade-down"><strong>Igienizarea</strong>: Periajul dinților se face normal, inclusiv la nivelul lucrării cimentate provizoriu. Ață interdentară trebuie utilizată cu prudență la nivelul lucrărilor cimentate provizoriu, îndepărtând firul din spațiile interdentare prin mișcări orizontale. În funcție de situația dumneavoastră specifică, va vom recomandă perii sau ațe interdentare speciale, precum și alte mijloace de igienizare. Dacă dinții sunt sensibili la temperaturi extreme, folosiți o pastă de dinți desensibilizantă.</p>
                        <p data-aos="fade-down"><strong>Medicația</strong>: Este normal ca dinții sau unele zone ale cavității bucale să fie sensibile câteva zile după intervenție. Totuși, nu luați nici un medicament fără recomandarea medicului.</p>
                        <p data-aos="fade-down">Este normal ca zone mici din lucrarea provizorie sa se uzeze, să se fractureze sau să se descimenteze. În cazul acesta, va rugăm sa ne contactați.</p>
                        <br>
                        <p data-aos="fade-down"><strong>Lucrări dentare – cimentarea definitivă</strong></p>
                        <p data-aos="fade-down">Pentru ca procesul de vindecare după efectuarea unei cimentări definitive sa fie cât mai rapid, este indicat sa respectați următoarele:</p>
                        <ul>
                            <li data-aos="fade-left" data-aos-delay="0">În cazul în care aveți anestezie locală așteptați până la remiterea completă a acesteia.</li>
                            <li data-aos="fade-left" data-aos-delay="50">După cimentarea definitiva a lucrărilor protetice, este normal sa aveți o senzatie de tensiune la nivelul dintilor sau chiar o jena la nivelul gingiilor. S-ar putea sa aveți nevoie de un analgezic. Acest disconfort va ceda în perioada de acomodare cu noile lucrări dentare. Dacă senzațiile neplăcute persistă mai multe zile, va rugam sa ne contactați.</li>
                            <li data-aos="fade-left" data-aos-delay="100">Igienizarea se realizeaza normal și la nivelul lucrărilor dentare, trebuie sa folositi în continuare apa de gura și ata dentara.</li>
                            <li data-aos="fade-left" data-aos-delay="150">Odată ce s-a cimentat definitiv o lucrare, veți beneficia de toate avantajele ceramicii, adică estetică și rezistență. Încercați să evitați socurile termice si mecanice protejandu-va în acest fel atat lucrările protetice, cat și dinții naturali.</li>
                            <li data-aos="fade-left" data-aos-delay="200">După cimentarea definitiva veți fi chemat la cabinet pentru controale periodice ale lucrării și pentru igienizarea acesteia. Este necesar să respectați aceste controale.</li>
                        </ul>
                        <p data-aos="fade-down">Pentru orice întrebare sau nelămurire, va rugam sa ne <a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">contactați</a> din timp. Echipa noastră este pregatită si dispusă sa va ofere consultanta atat în timpul cât și în urma lucrărilor dentare.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#sinusLift" aria-expanded="true" aria-controls="collapseOne">
                            Indicații post – sinus lift
                        </button>
                    </h2>
                </div>

                <div id="sinusLift" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p>Sinus lift este o procedurâ care recâștigă înălțimea osului pierdut în regiunea molară și premolară. Este o manoperă importantă deoarece permite inserarea implantelor într-o zona cu suport osos insuficient. Având în vedere invazivitatea acestei manopere asupra unor formațiuni anatomice (sinusul nazal) este foarte important să respectați următoarele instrucțiuni:</p>
                        <ul>
                            <li>Nu va suflați nasul timp de 1-2 săptămâni!</li>
                            <li>Nu strănutați ținăndu-vă de nas; strănutați cu gura deschisă pentru a nu crea presiune in sinus!</li>
                            <li>Nu beți cu paiul, nu scuipați!</li>
                            <li>Nu dormiți pe partea operată cate zile!</li>
                            <li>Scufundarile și zborul cu avionul trebuie evitate deoarece creeaza modificări ale presiunii în sinus posibil cauzatoare de complicatii.</li>
                            <li>Evitați sa ridicați greutăți mari aplecandu-va, evitați sa umflati baloane, nu cantati la instrumente muzicale ce necesita suflatul.</li>
                            <li>Nu deranjati locul intervenției. În caz contrar, puteți facilita aparitia iritatiei, infectiei sau sangerarii. Firele de sutura se pot indeparta în 10-14 zile.</li>
                            <li>Nu fumati 10-14 zile după efectuarea interventiei de sinus lift. Fumatul poate interfera cu procesul de vindecare (incetineste vindecarea), poate cauza săngerare si chiar o infecție a locului.</li>
                        </ul>
                        <p>Orice activitate care creste presiunea nazala si orala este contraindicată!</p>
                        <br>
                        <p><strong>Ce trebuie sa faci în privința edemului</strong></p>
                        <p>Este normal sa va umflați după operație. Pentru a preveni edemul postoperator, aplicați gheață sau un prosop rece pe fata în primele 12–24 de ore. Aplicații alternativ cu 10-20 de minute pauză, timp de o ora sau chiar mai mult dacă este necesar.</p>
                        <br>
                        <p><strong>Ce trebuie sa faci în caz de sângerare după intervenția de sinus lift</strong></p>
                        <ul>
                            <li>Nu vă alarmați dacă vă curge sânge pe nas, intindeți-va pe spate și aplicați o pungă cu gheață (10 minute cu 10 minute pauza) pe nara care sângerează. Nu faceți efort fizic și încercați sa stați cât mai relaxați posibil.</li>
                            <li>Prezența unei cantități mici de sânge în salivă este normală pentru primele 24 de ore dupa operatia de sinus lift.</li>
                            <li>Dacă sangerare din cavitatea bucală continua după câteva ore de la intervenție, adresati-va medicului stomatolog. Dacă este nevoie să folosiți compresele acasă nu uitați sa le rulați sub forma unui “ghemotoc” suficient de mare pentru a acoperi rana si a oferi o usoara compresie. Mentine-ti compresa pe locul intervenției prin presare (muscand pe ea) timp de 30 minute. Aceasta presiune ajuta la reducerea săngerarii si la formarea cheagului de sânge. Un înlocuitor foarte eficient al comprese din tifon este un plic de ceai (de preferat negru). Pliați plicul în doua și mușcați pe el timp de 15-30 de minute. Ceaiul conține acid tanic, un hemostatic natural, care ajuta la oprirea sangerarii.</li>
                        </ul>
                        <br>
                        <p><strong>Ce dieta este indicat sa urmezi</strong></p>
                        <ul>
                            <li>Este de evitat sa măncati imediat după intervenție de sinus lift. Cănd dispare efectul anesteziei puteți mânca, dar este recomandat sa mestecati pe partea opusă cel puțin 24-48 ore, pentru a evita ca mancarea sa ajungă pe locul respectiv.</li>
                            <li>Pentru primele 24-48 ore menține-ți o dieta cu mancaruri moi. Evitați mâncărurile fierbinti, picante, care pot provoca iritații si arsuri la locul intervenției. De asemenea, evitați băuturile carbogazoase pentru 3-4 zile.</li>
                            <li>Este, de asemenea, important sa beți multe lichide între mese.</li>
                            <li>Va puteți întoarce la regimul normal de alimentație atunci când considerați necesar.</li>
                            <li>Trebuie sa evitați sa deranjați locul operației cu obiecte ascuțite (tacâmuri,scobitori, degete sau alte obiecte).</li>
                        </ul>
                        <br>
                        <p><strong>Cum poți administra durerea</strong></p>
                        <ul>
                            <li>Este normal sa simțiți un mic disconfort în urma unei intervenții de sinus lift. Puteți lua analgezice (Ketonal, Ketorol, Nurofen, sau non aspirin) la fiecare 4-6 ore sau la recomandarea medicului stomatolog.</li>
                            <li>Medicamentele prescrise se vor lua doua – trei zile după operație sau la recomandarea medicului. Dacă sunt prescrise antibiotice urmați cu strictețe instrucțiunile.</li>
                            <li>În cazul în care răciti sau vă curge nasul, folosiți decongestionante nazale (ex. Bixtonim). În orice caz, nu suflati nasul!</li>
                        </ul>
                        <br>
                        <p><strong>Igiena orală în perioada imediat următoare intervenție de sinus lift</strong></p>
                        <ul>
                            <li>O igienă orala buna este esențială pentru vindecare. În ziua următoare intervenție de sinus lift, soluția recomandată de medicul dentist trebuie folosită de doua ori, după micul dejun și înainte de culcare. Clătiți 30 de secunde înainte sa scuipati.</li>
                            <li>Periajul dinților nu reprezinta o problemă, cu mențiunea sa fiti inițial mai blând/ă cu zona operată.</li>
                            <li>Folosiți periuța de dinți cu grijă. Câteva zile după extracție este foarte important să mențineți locul cât mai curat pentru a preveni infecția și a ajuta la vindecarea. Nu periați direct locul operației, cel puțin 3-4 zile, pentru a preveni dislocarea cheagului de sânge. După aceasta perioadă puteți curăța cu grijă zona intervenției și dinții din vecinătate.</li>
                            <li>Evitați folosirea apelor de gura în primele 24 de ore de la intervenție. Aceasta măsura este necesara pentru a asigura formarea cheagului de sânge. Distrugerea cheagului poate duce la sângerare. După primele 24 de ore ar trebui să clătiți în zona respectivă cu apa caldă sărata sau cu apa de gură pe baza de clorhexidină (ex.Parodontax). Clatiti ușor după fiecare masa sau gustare, asigurându-vă că apa va ajunge în zona extractiei. Clătirea îndepărtează resturile alimentare și debriurile din zona intervenției accelerând vindecarea.</li>
                        </ul>
                        <br>
                        <p><strong>Activitatea fizica după intervenția de sinus lift</strong></p>
                        <ul>
                            <li>Activitatea fizică trebuie sa fie redusă la minimum în ziua operației. În caz contrar, apare riscul palpitațiilor și al hemoragiei.</li>
                            <li>Nu vă aplecați sau nu ridicați greutăți.</li>
                            <li>Este recomandat ca pe o perioada de aproximativ două săptămâni să evitați activitățile sportive sau alte activități solicitante fizic.</li>
                            <li>Țineți cont de restricțiile alimentare care vă limitează capacitatea de efort.</li>
                        </ul>
                        <p>Pentru orice întrebare sau nelamurire, vă rugăm sa ne <a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">contactați</a> din timp. Cu cat se intervine mai rapid, cu atat complicațiile sunt mai mici.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#postImplant" aria-expanded="true" aria-controls="collapseOne">
                            Ce trebuie sa faci după inserarea implantului
                        </button>
                    </h2>
                </div>

                <div id="postImplant" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>Indicații post implant: ce trebuie sa faci după inserarea implantului</strong></p>
                        <p>Nimeni nu-și dorește complicații sau eșec după tratamentul de inserare al unui implant dentar. Ca pacient, ar trebui să faci tot ce-ți stă în putință pentru a evita aceasta situație. Să urmezi instrucțiunile medicului dentist post operației de implant dar și îngrijirea lor pe termen lung sunt condiții absolut necesare pentru a păstra implanturile cat mai mult.</p>
                        <br>
                        <p><strong>Gestionarea hemoragiei post implant</strong></p>
                        <p>O sângerare ușoară este normală în primele 24 de ore. Nu luati aspirină, poate produce sângerare, deoarece are rol anticoagulant. Sângerarea excesivă (gura se umple rapid cu sânge) poate fi controlată prin mușcarea unei comprese sterile (sau a unui pliculeț de ceai negru) timp de 30 de minute. Dacă sângerarea persistă, contactați medicul stomatolog.</p>
                        <br>
                        <p><strong>Edemul post implant</strong></p>
                        <p>După operație este normal sa va umflați. Pentru a minimaliza acest lucru, aplicați o punga cu gheață pe obraz la nivelul zonei operate. Aplicați gheață în mod repetat (10 minute aplicați, 15 minute pauza), pe cat posibil, pentru primele 24 de ore. Edemul și modificările tegumentare pot varia ca întindere, în funcție de cât de extinsă a fost interventia chirurgicala. Aceste manifestări pot fi minimalizate prin menținerea unei poziții mai ridicate a capului în timpul somnului. Nu vă sprijiniți și nu dormiți pe partea operată. Nu va speriați dacă apare o vanataie (hematom). Hematoamele se resorb de la sine în aproximativ 7-10 zile. Apariția lor este posibilă mai ales dacă intervenția a fost mai dificila sau a durat mai mult, cu traumatizarea părților moi sau lambouri largi.</p>
                        <br>
                        <p><strong>Dieta post implant</strong></p>
                        <p>Evitați alcoolul și fumatul pe toata perioada vindecarii. Pentru primele 4-5 zile mențineți o dieta cu alimente moi, ușor de mestecat și înghițit. Evitați mâncărurile fierbinti, picante, care pot provoca iritatii si arsuri la locul intervenției. De asemenea, evitați băuturile carbogazoase pentru 3-4 zile. Este de evitat să mâncați imediat după intervenție. Cand dispare efectul anesteziei puteți mânca, dar este recomandat sa mestecati pe partea opusă cel puțin 24-48 ore, pentru a evita ca mancarea sa ajungă pe locul respectiv. Este, de asemenea, important sa beți multe lichide între mese. Va puteți întoarce la regimul normal de alimentatie atunci cand considerati necesar. Trebuie sa evitati sa deranjați locul operației cu obiecte ascuțite (tacămuri,scobitori, degete sau alte obiecte).</p>
                        <br>
                        <p><strong>Gestionarea durerii post implant</strong></p>
                        <p>Postoperator, pentru controlarea durerii, se recomandă administrarea de Ibuprofen 400 mg/12 h, timp de 3 zile. Pentru confortul dumneavoastra, este recomandat sa luati analgezice imediat ce simțiți ca efectul anesteziei dispare. În privința administrării antibioticelor, este esențial să respectați programul prescris pe reteta de antibiotice.</p>
                        <br>
                        <p><strong>Igiena orala post implant</strong></p>
                        <p>O igiena orala buna este esențială pentru vindecare. În ziua intervenției nu se recomandă folosirea apei de gura. În ziua următoare, apa de gura trebuie folosită de cel puțin 2 ori, după micul dejun și înainte de culcare. Clatiti UȘOR! 60 de secunde înainte sa scuipați. Periajul dinților si a zonei operate nu reprezinta o problema, cu mențiunea sa fiti inițial mai blănd(a) în zona respectivă. De asemenea, se recomanda pentru o vindecare căt mai rapidă și sigură aplicarea gelului GINGIVAL pe zona unde s-a efectuat intervenția, timp de 3-4 zile și de 3-4 ori pe zi. Aplicarea se face printr-o atingere usoara a zonei timp de 30 – 60 de secunde.</p>
                        <br>
                        <p><strong>Activitatea fizica</strong></p>
                        <p>Activitatea fizică sa fie redusă la minimum în ziua operației, în caz contrar apare riscul palpitațiilor și al hemoragiei. Nu va aplecați si nu ridicați greutati.Tineti cont de restricțiile alimentare care va limitează capacitatea de efort.</p>
                        <p>Pentru orice întrebare sau nelamurire, vă rugăm sa ne <a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">contactați</a> din timp. Cu cat se intervine mai rapid, cu atat complicațiile sunt mai mici.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#postAlbire" aria-expanded="true" aria-controls="collapseOne">
                            Indicații post-albire
                        </button>
                    </h2>
                </div>

                <div id="postAlbire" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>Este sigur?</strong></p>
                        <p>Acest tip de albire este cel mai recomandat pentru o albire totală pentru ca  structură prismatică a smalțului dentar nu este afectată în urma albirii (fapt demonstrat cu studii de microscopie electronică).</p>
                        <p>Albirea dinților sub supravegherea  medicului  stomatolog s-a dovedit sigură în studii clinice de-a lungul unei perioade îndelungate de timp. Substanța activa, peroxidul de carbamida, a fost utilizată pentru a readuce zâmbetul luminos pe buzele a sute de milioane de oameni din întreaga lume.</p>
                        <br>
                        <p><strong>Cum funcționeaza?</strong></p>
                        <p>Stomatologul va lua amprentele dinților tăi și va face gutiere în care  se va pune gelul de albire. Aceste gutiere cu gel vor fi fixate pe dinții tăi și le vei purta în timpul somnului între 2-8 ore în funcție de concentrația de substanță pe care o primești. În timpul procesului de albire structura dintelui nu este afectată iar culoarea dintelui se luminează.</p>
                        <br>
                        <p><strong>Care este durata tratamentului?</strong></p>
                        <p>Rezultatele se pot observa adesea după prima ședință de aplicare. Culoarea finală a dinților se stabilizeaza după ce procesul are loc timp de o săptămână până la 10 zile. Stomatologul va aprecia în funcție de fiecare situație în parte.</p>
                        <br>
                        <p><strong>Cât durează?</strong></p>
                        <p>Durata rezultatelor depinde in mare măsură de dumneavoastră. Practicând o buna igiena orala și un periaj corect, în special după consumarea de alimente colorate sau băuturi, rezultatele pot dura cativa ani (3-5 ani). Dacă consumați cafea, tutun, vin roșu, vor fi necesare tratamente de întreținere a albirii de cate o zi, două la fiecare șase luni sau se reia procedură după o perioadă mai scurtă (2-3 ani).</p>
                        <br>
                        <p><strong>Mod de utilizare</strong></p>
                        <p>Periați-va dinții. Umpleți gutiera cu gelul de albire.</p>
                        <p>Fixați gutiera pe dinți, apasa-l ușor, pentru a presa puțin gelul și a-l distribui uniform.</p>
                        <p>Atenție însă: dacă presați prea puternic, gelul poate ieși din gutiera.</p>
                        <p>Indepartați usor excesul de gel de pe gingie cu o periuță de dinți sau chiar cu degetul.</p>
                        <p>După ce ați poziționat gutierele, este de recomandat sa nu mancati, sa nu beți, sa nu vorbiți prea mult.</p>
                        <br>
                        <p><strong>Contraindicații:</strong></p>
                        <p>Nu folosiți substanțe de albire dentara în cazul în care sunteți însărcinată sau alăptați.Nu mancati in timp ce purtati gutierele.</p>
                        <p>Nu folosiți produse provenite din tutun în timp ce purtați gutierele.  Nu fumați în timpul tratamentului.</p>
                        <p>Nu expuneti seringile la căldură și/sau soare.</p>
                        <p>Nu congelați seringile, depozitați-le la rece în schimb (în frigider).</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#postExtractie" aria-expanded="true" aria-controls="collapseOne">
                            Indicații post extracție pentru accelerarea vindecării
                        </button>
                    </h2>
                </div>

                <div id="postExtractie" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p>Unul dintre obiectivele principale ale stomatologiei este prevenirea pierderii dinților. Se iau toate măsurile pentru a conserva și a menține dinții, deoarece un singur dinte pierdut poate avea un impact major asupra sanatatii dentare, vorbirii, mușcaturii, masticației și a aspectului. Cu toate acestea, din diverse motive, uneori pacientul ajunge în situația în care este necesara extracția unui dinte.</p>
                        <p>Vă puteți aștepta la disconfort, sângerare și/sau inflamație în următoarele 24 de ore. Vindecare inițială durează aproximativ de la o sâptamâna la două săptămâni. Gingiile se vindeca în trei – patru săptămâni și vindecarea completă a osului se face în aproximativ șase luni. Pe măsură ce locul extracției se vindecă, urmând aceste indicații post-extracție, puteți facilita o vindecare mai rapidă și puteți evita eventualele complicații.</p>
                        <br>
                        <p>Ce trebuie să faceți pentru accelerarea vindecării și evitarea de complicații post-extracție:</p>
                        <p><strong>Nu deranjați locul extracției.</strong> În caz contrar, puteți facilita aparitia iritatiei, infecției sau sângerării. Simpla apasare cu o compresa este suficientă pentru a controla sangerarea si pentru a ajuta formarea unui cheag de sânge în alveola. Firele de sutura se pot îndepărta în 5-6 zile. Dacă sângerarea persistă, adresați-vă medicului stomatolog.</p>
                        <p><strong>Nu fumati 48 – 72 de ore după efectuarea extracției.</strong> Fumatul poate interfera cu procesul de vindecare, poate cauza sângerare si chiar infecția alveolei. Continuând să fumați după primele zile, incetiniți viteza de vindecare și sângele nu va mai putea sa umple alveola și să formeze cheagul necesar unei vindecări sănătoase. Fumatul poate afecta osul din jurul alveolei, îngreunând și mai mult vindecarea.</p>
                        <p><strong>Folosiți periuța de dinți cu grija.</strong> Câteva zile după extracție este foarte important sa mentineti locul cat mai curat pentru a preveni infectia si a ajuta la vindecare. Nu periati direct locul extractiei, cel puțin 3-4 zile, pentru a preveni dislocarea cheagului de sânge. În aceasta perioada puteți curăța cu grija dinții din vecinătatea locului extractiei.</p>
                        <p><strong>Evitați folosirea apelor de gura.</strong> Evitați să clatiti cu apa de gura în primele 24 de ore de la extracție. Aceasta este una dintre principalele indicații post extracție necesară a fi respectată pentru a asigura formarea cheagului de sange. Distrugerea cheagului poate duce la sângerare, la o vindecare mai de durata și chiar la infecție. După primele 24 de ore ar trebui să clătiți în zona respectivă cu apă caldă sărata sau cu apă de gură pe baza de clorhexidina (ex:Parodontax). Clătiți ușor după fiecare masa sau gustare, asigurându-vă că apa va ajunge în zona extractiei. Clătirea îndepărtează resturile alimentare și debriurile din alveola, ajutand vindecarea.</p>
                        <p><strong>Nu scuipati și nu beți cu paiul.</strong> În caz contrar, acestea vor duce la sângerare și la dislocarea cheagului de sânge.</p>
                        <p><strong>Fiți atenți la sangerare.</strong> Dupa extractie se plasează pe locul respectiv o compresa pe care trebuie sa o schimbati la un interval de aproximativ 10-20 de minute, în funcție de gravitatea sangerarii. Este normal existe o mica sangerare în primele 8-12 ore de la extracție. Dacă sangerare continua, folosiți comprese din tifon pentru a opri sangerarea. Dacă este nevoie sa folositi compresele acasă, nu uitați să le rulați sub forma unui “ghemotoc” suficient de mare pentru a acoperi rana. Mențineți compresa pe locul extracției, prin presare (mușcând pe ea), timp de 30 de minute. Această presiune ajută la reducerea sângerării și la formarea cheagului de sange. Dacă sângerarea continuă, umeziți un plic de ceai (este recomandat ceaiul negru), pliați plicul în două și mușcat pe el timp de 30 de minute. Ceaiul contine acid tanic, un hemostatic, care ajută la oprirea sângerării.</p>
                        <p><strong>Folosiți numai anumite medicamente.</strong> În perioada post-extracție este normal să simțiți un mic disconfort. Puteți lua analgezice (Ketonal, Ketorol, Nurofen, sau non aspirin) la fiecare patru ore sau la recomandarea medicului stomatolog. Medicamentele prescrise se vor lua în primele doua – trei zile post extracție sau la recomandarea medicului. Dacă sunt prescrise antibiotice, urmați cu strictețe instrucțiunile.</p>
                        <p><strong>Fiți atenți la edem.</strong> Pentru a preveni edemul post-operator, aplicați gheață sau un prosop rece pe fața în primele 12–24 de ore postextractie. Aplicați alternativ cu 10-20 de minute pauza, timp de ora sau mai mult, dacă este necesar.</p>
                        <p><strong>Aveți grijă ce mâncați.</strong> Pentru primele 24-48 de ore post-extracție, mențineți o dieta cu mancaruri moi. Evitați mâncărurile FIERBINTI, picante, care pot provoca iritații și arsuri în locul extracției. De asemenea, evitați băuturile carbogazoase pentru 2-3 zile post-extracție. Este de evitat sa mancati imediat dupa extractie. Când dispare efectul anesteziei puteți mânca, dar este recomandat să mestecați pe partea opusă cel puțin 24-48 de ore, pentru a evita ca mâncarea să ajungă pe locul extractiei. Este la fel de important sa beți multe lichide între mese. Trebuie sa evitați să deranjați locul extractiei cu obiecte ascuțite (tacâmuri, scobitori, degete sau alte obiecte). Vă puteți întoarce la regimul normal de alimentație atunci când considerați necesar.</p>
                        <p><strong>Reduceti activitățile.</strong> Pentru primele 24-48 de ore post-extracție trebuie să limitați activitățile, deoarece o activitate intensă poate duce la o intensificare a sângerării. Nu va aplecati sau nu ridicati greutati in primele 2-3 zile postextracție.</p>
                        <p><strong>Cunoașteți efectele anesteziei.</strong> Cand se foloseste anestezia locală, obrazul, limba și buza vor fi amortite cateva ore dupa extractie. În timp ce sunt anesteziate veți avea o senzație “ciudată”. În timpul acestei perioade aveți grijă să nu mușcați, să nu mestecați și/sau să vă scărpinat în acea zonă, pentru că puteți cauza o traumă severă țesutului conjunctiv.</p>
                        <p>Chiar dacă respectați toate aceste indicații post-extracție, nu uitați că pot apărea probleme pe termen lung. Lipsa unui dinte poate duce la probleme severe, ca de exemplu: mobilitatea dinților, dificultăți în masticație sau malocluzie, care poate duce la probleme ale ATM (articulatia temporo-mandibulara). În aceasta zona va fi nevoie de o lucrare protetica fixa, implant, proteza parțială sau totala pentru a asigura o sanatate dentara pe termen lung.</p>
                        <p>Pentru orice întrebare sau nelamurire, vă rugăm sa ne <a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">contactați</a> din timp. Cu cat se intervine mai rapid, cu atat complicațiile sunt mai mici.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#tratamentEndodontic" aria-expanded="true" aria-controls="collapseOne">
                            Tratament endodontic
                        </button>
                    </h2>
                </div>

                <div id="tratamentEndodontic" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>Tratament canal</strong></p>
                        <p>Tratamentul de canal al unui dinte este o etapă din cadrul unui întreg proces, al cărui scop este păstrarea dintelui în cavitatea bucală. Se apelează la acest tratament  în momentul în care vasele de sânge sau nervul aferent dintelui sunt infectate.</p>
                        <p>Este normal sa simțiți disconfort câteva zile după tratament, mai ales în timpul masticației. Experiența arată că, dacă a existat durere înainte de tratament, foarte posibil ca ea să persiste într-un grad mai scăzut câteva zile după tratament.</p>
                        <p>Un dinte tratat endodontic și apoi restaurat poate ține o viață întreagă dacă ai grijă de el. Respectă cu rigurozitate indicațiile medicului,  regulile de igienă orală și vino la consultul periodic o dată la 6 luni sau atunci când simți cel mai mic disconfort dentar.</p>
                        <br>
                        <p><strong>Tratament canal – ce trebuie să faci după tratament</strong></p>
                        <ul>
                            <li>Până la dispariția completă a anesteziei va rugăm sa nu măncati, existând riscul să vă mușcați de părțile moi ale cavitatii bucale.</li>
                            <li>Dacă dintele nu a fost reconstituit cu o obturație finală nu mâncați o ora.</li>
                            <li>În primele zile după tratament evitați alimentele fierbinți sau foarte reci, încercați să consumați alimente moi și folosiți la masticație partea opusă.</li>
                            <li>Nu mestecați puternic pe dinte pana cand acesta nu va fi reconstituit definitiv, existand riscul de a fractura dintele.</li>
                            <li>Evitați fumatul, alcoolul și efortul fizic.</li>
                            <li>Pentru combaterea durerii luați antialgicele recomandate de către medic.</li>
                            <li>Dacă v-a fost prescrisă o reteta cu antibiotice urmați cu strictețe chiar dacă au dispărut simptomele și semnele de infecție. În caz de reacții alergice opriți imediat tratamentul.</li>
                        </ul>
                        <br>
                        <p><strong>Tratament canal – ce trebuie să faci în timpul obturatiei provizorii</strong></p>
                        <p>Obturatia provizorie vă protejează dintele pana la aplicarea restaurării definitive. Uneori se desprind bucăți din ea. Acest lucru nu reprezintă o problema, dar este bine sa fie prevenit prin respectarea indicațiilor:</p>
                        <ul>
                            <li>evitați gumă de mestecat, alimentele lipicioase sau tari;</li>
                            <li>pe căt posibil, mestecati pe partea opusă;</li>
                            <li>continuați periajul normal;</li>
                            <li>folosiți cu grijă ața dentară.</li>
                        </ul>
                        <p>După finalizarea tratamentului endodontic (tratament canal) și a reconstituirii coronare finale, odată cu dispariția disconfortului postoperator, dintele trebuie acoperit cat mai repede posibil (în maximum o lună de la finalizarea tratamentului endodontic).</p>
                        
                        <br>
                        <p><strong>Indicații în timpul tratamentului de endodonție</strong></p>
                        <br>
                        <p><strong>Tratament de endodonție – ce înseamnă</strong></p>
                        <p>Endodonția este ramura stomatologiei care se ocupă cu tratamentul părții dintelui (rădăcina) care nu este vizibilă în cavitatea bucală. Tratamentul de endodonție al unui dinte este o etapa din cadrul unui întreg proces, al cărui scop este salvarea și păstrarea dintelui în cavitatea bucală.</p>
                        <p>Am folosit un anestezic în cursul tratamentului. Amorțeala dinților, a buzelor și a limbii este posibil să mai dureze câteva ore. Nu vă muscați buzele! Nu vă mușcați limba! Evitați mestecatul până la dispariția completă a anesteziei.</p>
                        <p>Canalul (canalele) din interiorul rădăcinii (rădăcinilor) a (au) fost curățat(e), irigate și obturate definitiv. Orificiul de deschidere prin care s-a făcut tratamentul a fost obturat. Amânarea obturării finale/ aplicării coroanei definitive poate duce la fractură și posibil la pierderea dintelui.</p>
                        <br>
                        <p><strong>Tratament de endodonție – ce recomandări trebuie respectate</strong></p>
                        <p>Obturația provizorie vă protejează dintele pana la aplicarea restaurării definitive. Uneori se desprind bucăti din ea, acest lucru nu reprezinta o problema, dar este bine sa fie prevenit prin respectarea indicațiilor:</p>
                        <ul>
                            <li>evitați gumă de mestecat, alimentele lipicioase sau țări</li>
                            <li>pe căt posibil, mestecati pe partea opusă;</li>
                            <li>continuați periajul normal;</li>
                            <li>folosiți cu grijă ața dentară.</li>
                        </ul>
                        <p>Este normal sa simțiți disconfort cateva zile după tratament, mai ales în timpul masticației. Experiența arată că, dacă a existat durere înainte de tratament, foarte posibil ca ea să persiste într-un grad mai scăzut căteva zile după tratament. Pentru combaterea durerii luați antialgicele recomandate de către medic. Dacă v-a fost prescrisă o rețetă cu antibiotice urmați cu strictețe chiar dacă au dispărut simptomele și semnele de infecție. În caz de reacții alergice opriți imediat tratamentul.</p>
                        <p>Pentru combaterea durerii și a edemului clătiți de 3 ori pe zi cu soluție de sare dizolvată în apă călduță (dizolvati o linguriță de sare într-un pahar cu apă călduță). Pungile cu gheață aplicate 10-15min la fiecare ora, în ziua tratamentului, ajuta la reducerea inflamației.</p>
                        <p>În primele doua zile după tratament evitați alimentele fierbinți sau foarte reci, încercați să consumați alimente moi. De asemenea, evitați fumatul, alcoolul și efortul fizic.</p>

                        <p>Pentru orice întrebare sau nelamurire, vă rugăm sa ne <a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">contactați</a> din timp. Cu cat se intervine mai rapid, cu atat complicațiile sunt mai mici.</p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#albireDentara" aria-expanded="true" aria-controls="collapseOne">
                            Albirea dentară la domiciliu
                        </button>
                    </h2>
                </div>

                <div id="albireDentara" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>Instrucțiuni pentru albirea dentară la domiciliu</strong></p>
                        <br>
                        <p><strong>Pentru cine și când este indicată albirea dentară la domiciliu?</strong></p>
                        <p>Albirea dentară la domiciliu este indicată tuturor pacienților care doresc să aibă un zambet luminos și atragator. În general, rezultatele cele mai bune se obțin în cazuri de coloratii ușoare și medii ale dinților, gradul de albire fiind dependent de structura dinților și de tipul de coloranți la care sunt expuși dinții în mod frecvent. Colorațiile accentuate beneficiază de un tratament mai îndelungat de albire și cu rezultate diferite de la caz la caz.</p>
                        <p>Procesul de albire acționează numai pe dinții naturali, nu și pe obturații, fațete, coroane sau punți dentare, astfel ca acestea necesită, de obicei, înlocuirea după terminarea tratamentului.</p>
                        <p>Albirea dinților sub supravegherea medicului stomatolog s-a dovedit sigură în studii clinice de-a lungul unei perioade îndelungate de timp. Substanța activa, peroxidul de carbamida, a fost utilizată pentru a readuce zâmbetul luminos pe buzele a sute de milioane de oameni din întreaga lume.</p>
                        <p>Albirea dentară efectuată acasă, dar sub îndrumarea medicului stomatolog, este metoda cea mai recomandată de noi pentru o albire totala. Prin albirea dentară la domiciliu, structura smalțului dentar este cea mai puțin afectată (comparativ cu cea efectuată în cabinetul stomatologic). Acest fapt este demonstrat de numeroase studii de microscopie electronică.</p>
                        <br>
                        <p><strong>Cum se realizeaza albirea dentară la domiciliu?</strong></p>
                        <p>Albirea dentară la domiciliu trebuie sa se realizeze cu atenție, urmand cativa pași simpli:</p>
                        <ul>
                            <li>Periați-vă dinții.</li>
                            <li>Umpleți gutierele cu gelul de albire, după cum v-a explicat medicul stomatolog.</li>
                            <li>Fixați gutiera pe dinți, apasa-l ușor, pentru a presa puțin gelul și a-l distribui uniform. Atenție însă: dacă presați prea puternic gelul poate ieși din gutiera!</li>
                            <li>Indepartati usor excesul de gel de pe gingie cu o periuta de dinti, o compresa sau cu degetul.</li>
                            <li>Datorita unei concentratii mai scazute a substantei de albire, gutierele se mențin în cavitatea bucala timp de 4-6 ore (pentru o concentratie de 15%).</li>
                            <li>După îndepărtarea gutierelor spalati-va pe dinți. Aveți grija sa nu inghititi.</li>
                            <li>Gutierele se curata cu apa rece și o periuta de dinți moale. Este recomandat sa se depoziteze în recipientele speciale pe care le-ați primit de la medicul stomatolog.</li>
                        </ul>
                        <p><strong>Atentie:</strong> in cazul in care apare sensibilitate dentara spontana sau provocata se recomanda oprirea temporara a tratamentului pana la disparitia completă a simptomelor. Menționăm faptul că beneficiile albirii dentare nu se “pierd” pe durata acestei perioade, iar reluarea ulterioară a tratamentului nu modifică deloc eficienta și rezultatul albirii dentare.</p>
                        <br>
                        <p><strong>Care sunt rezultatele și cât durează acestea?</strong></p>
                        <p>Rezultatele se pot observa adesea după prima ședința de aplicare. Culoarea finală a dinților se stabilizeaza după ce procesul are loc timp de o saptamana pana la 10 zile. Stomatologul va aprecia timpul necesar albirii în funcție de fiecare situație în parte.</p>
                        <p>Durata rezultatelor depinde in mare masura de dumneavoastra. Practicând o buna igiena orala și un periaj corect, în special după consumarea de alimente colorate sau băuturi, rezultatele pot dura cativa ani (3-5 ani). În cazul în care consumați cafea, tutun, vin roșu, vor fi necesare tratamente de întreținere a albiri de cate o zi doua la fiecare șase luni , sau se reia procedura după o perioada mai scurta (2-3 ani).</p>
                        <br>
                        <p><strong>Contraindicații</strong></p>
                        <p>Bineînțeles, albirea dentară la domiciliu, ca orice alt tratament aplicat asupra danturii, trebuie efectuată la recomandarea și după indicațiile medicului stomatolog. Pentru rezultate de durată, este recomandat să:</p>
                        <ul>
                            <li>Nu folosiți substanțe de albire dentară în cazul în care sunteți însărcinată sau alăptați.</li>
                            <li>Nu mâncați în timp ce purtați gutierele. Pe perioada albirii băuturile acidulate pot cauza sensibilitatea dinților, de aceea ar trebui evitate.</li>
                            <li>Nu folosiți produse provenite din tutun în timp ce purtați gutierele. Nu fumați în timpul tratamentului.</li>
                            <li>Nu expuneți seringile la căldură și/sau soare.</li>
                            <li>Nu congelați seringile, depozitațile la rece (in frigider).</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#intretinereProteza" aria-expanded="true" aria-controls="collapseOne">
                            Indicații pentru purtătorul de proteze mobile
                        </button>
                    </h2>
                </div>

                <div id="intretinereProteza" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>În perioada inițială de adaptare:</strong></p>
                        <ul>
                            <li>nu scoateți protezele din gura în primele 24 ore, deoarece medicul stomatolog va va programa în ziua imediat următoare celei în care au fost gata protezele;</li>
                            <li>senzatia de “gura plina” si cresterea salivatiei sunt normale și vor trece cu timpul;</li>
                            <li>senzatia de voma poate disparea daca sugeti o bomboana (gen drops);</li>
                            <li>în timpul acestei perioade pot apărea zone dureroase în cavitatea bucală;</li>
                            <li>dacă aveți senzația ca mancarea nu are gust consumați mancaruri sau bauturi calde, cu aroma si miros mai usor sesizabile (mai condimentate).</li>
                        </ul>
                        <br>
                        <p><strong>Cum trebuie sa măncati cu proteza:</strong></p>
                        <ul>
                            <li>tăiați mâncarea în bucăți mici, cu ajutorul cutitului;</li>
                            <li>încercați sa mestecati în ambele parti în același timp;</li>
                            <li>dacă mușcați cu dinții din față, protezele se vor mișca și vor fi afectate și gingiile;</li>
                            <li>evitați mâncărurile delicioase;</li>
                            <li>durează un timp să învățați să mâncați cu protezele. Niciodată nu va fi la fel de bine ca atunci cand aveți dinți naturali, de aceea trebuie sa aveți răbdare;</li>
                            <li>mușchii trebuie reeducați, astfel ca în final vor ajuta și ei la ținerea protezei;</li>
                            <li>limba își va găsi până la urmă locul ei.</li>
                        </ul>
                        <br>
                        <p><strong>Vorbirea cu noile proteze:</strong></p>
                        <ul>
                            <li>durează până să vă obișnuiți – citiți cu voce tare pentru a scurta aceasta perioada;</li>
                            <li>repetati cuvintele pe care le pronunțați cu dificultate.</li>
                        </ul>
                        <br>
                        <p><strong>Curățarea gurii și a protezelor:</strong></p>
                        <ul>
                            <li>curățați și masați zilnic gingiile cu o periuță moale;</li>
                            <li>frecați protezele cu o perie mai dura și cu șampon obișnuit;</li>
                            <li>nu folosiți niciodată pasta de dinti pentru a curata proteza, deoarece este prea abraziva;</li>
                            <li>este bine ca în timpul igienizarii proteza să fie ținută deasupra unui recipient cu apa. Dacă proteza aluneca, apa va amortiza căderea;</li>
                            <li>lasa-ti protezele peste noapte (de 2-3 ori pe saptamana) în una din următoarele soluții: o soluție de curățat protezele din comerț sau o soluție făcută dintr-o linguriță de oțet la un pahar cu apa.</li>
                        </ul>
                        <p>Introducerea protezei într-o soluție, obținută prin dizolvarea unei tablete efervescente într-un pahar cu apa, ce are o acțiune chimică și mecanică, poate îndepărta de pe proteza ușoare colorații, resturile de mâncare și microbii cantonați pe suprafața protezei.</p>
                        <br>
                        <p><strong>Ce puteți face și ce nu puteți face:</strong></p>
                        <ul>
                            <li>nu purtați protezele în timpul nopții dacă medicul va recomanda expres acest lucru;</li>
                            <li>folosiți substanțe adezive (Corega® – Block Drug Company, Fixodent® – Procter & Gamble) numai la sfatul medicului stomatolog;</li>
                            <li>cand tusiti sau stranutati protezele se pot desprinde, ceea ce poate crea o situație jenantă, care poate fi evitată prin acoperirea gurii cu o batistă sau cu mana;</li>
                            <li>nu încercați niciodată să adaptați sau sa reparați singuri protezele;</li>
                            <li>prezentați-vă regulat la medic pentru un control de rutina (CEL PUȚIN O DATA PE AN).</li>
                        </ul>
                        <br>
                        <p><strong>Protezele nu sunt eterne. La nivelul oaselor și gingiilor continuă să se producă modificări. Dacă aveți una din următoarele probleme:</strong></p>
                        <ul>
                            <li>masticație dificilă a mâncării;</li>
                            <li>mișcare repetată a obrazului;</li>
                            <li>dificultăți în vorbire;</li>
                            <li>gingii roșii și inflamate;</li>
                            <li>disconfort produs de purtarea protezelor;</li>
                            <li>fisuri la colțul buzelor.</li>
                        </ul>
                        <p><a href="<?php echo base_url($this->language['url_key'] . "/contact"); ?>">ADRESAȚI-VĂ MEDICULUI STOMATOLOG!</a></p>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#sfaturiIgienaOrala" aria-expanded="true" aria-controls="collapseOne">
                            Sfaturi pentru o igienă orală corespunzătoare
                        </button>
                    </h2>
                </div>

                <div id="sfaturiIgienaOrala" class="collapse" aria-labelledby="headingOne" data-parent="#listaIndicatii">
                    <div class="card-body">
                        <p><strong>Suntem cu toții convinși că știm să ne spălăm pe dinți corect, dar este chiar așa?</strong></p>
                        <p>Datele statistice arată clar ca afecțiunile stomatologice ocupă primul loc dintre toate îmbolnăvirile organismului. Astfel, găsim o prevalență a cariei dentare în randul populatiei – peste 90%, peste 60% din populatie prezinta anomalii dento-maxilare și peste 60% din populația de peste 35 de ani prezinta afecțiuni ale gingiei si parodontului.</p>
                        <p>Este important ca fiecare dintre noi să afle importanta ingrijirii dinților, să cunoască simptomele afecțiunilor buco-dentare și a modului în care acestea determină alte afecțiuni ale organismului. Prin înțelegerea acestor informații, putem trece de la faza de “a afla”, la “a înțelege și a conștientiza” cum putem preveni.</p>
                        <p>Periajul dentar corect și efectuat cu regularitate este cea mai eficientă metodă de îndepărtare a plăcii bacteriene și de a reduce semnificativ riscul de apariție a cariei dentare.</p>
                        <br>
                        <p><strong>Reguli simple pentru o igienă orală corespunzătoare:</strong></p>
                        <ul>
                            <li>Periați-vă dinții corect, cel puțin de două ori pe zi zi timp de cel puțin 3 minute.</li>
                            <li>Acordați cel puțin 10 secunde, odată cu fiecare periaj, curățării limbii și feței inferioare a obrajilor.</li>
                            <li>Schimba-ti periuța de dinți după cel mult 3 luni de utilizare.</li>
                            <li>Utilizați mijloace suplimentare de igenizare orala(apa de gura, ața dentară, duș bucal), conform indicațiilor medicului dentist. Dușul bucal este un dispozitiv ce îndepărtează placa bacteriană cu ajutorul unui jet sub presiune de apă sau apa de gura. Acesta se recomanda celor cu afecțiuni gingivale, parodontale , celor cu aparate ortodontice, pacienților cu lucrări protetice (pe dinți sau pe implanturi), dar și celor ce doresc o igienă completă.</li>
                            <li>Mergeți la controlul stomatologic în mod periodic (cel puțin o data la 6 luni), pentru a preveni sau trata afectiunile orale.</li>
                            <li>Cereți sfatul medicului dentist cu privire la cele mai potrivite produse de ingrijire orala, în funcție de particularitățile afectiunilor orale pe care trebuie sa le preveniti sau sa le tratați.</li>
                            <li>Folosiți produse de igienă orala care conțin fluor, care contribuie eficient la apariția cariilor dentare.</li>
                        </ul>
                        <p>Oricât ar părea de incredibil, exista statistici care susțin că persoanele care obișnuiesc sa își clateasca gura cu apa, fără nici un adaos, în mod regulat (de mai multe ori pe zi, în special după masa sau după consumul de dulciuri sau băuturi dulci), au mai puține calorii. Explicația este firească: apa nu permite creșterea nivelului de aciditate și pastreaza în gura un echilibru cu adevărat natural.</p>
                        <br>
                        <p><strong>Importanța igienei orale în timpul sarcinii</strong></p>
                        <p>Perioada sarcinii este o perioada foarte emoțională și foarte intensă. Corpul vostru trece prin multe schimbari si gura voastra nu face exceptie. O igiena orala corespunzatoare este foarte importantă în timpul sarcinii deoarece creșterea nivelurilor de hormoni poate intensifica problemele dentare. Una dintre cele mai obișnuite probleme asociate sarcinii este o afectiune numita gingivita de sarcina care apare de obicei în primul trimestru. Simptomele obișnuite ale acestei afectiuni sunt de obicei sangerari ale gingiilor și gingii umflate, roșii și sensibile.</p>
                        <p>O igiena orala corespunzatoare în timpul sarcinii este importantă și pentru fetus. Cativa cercetătorii au ajuns la concluzia ca stadiile târzii ale gingivitei, precum parodontopatia, pot contribui la riscul unei nasteri premature, o greutate mica a bebelusului la naștere, declansarea contractiilor premature sau chiar o infecție a nou nascutului. Toate acestea pot apărea din cauza faptului ca bacteriile din gura mamei pot fi transmise prin sange și apoi prin lichidul amniotic fetusului din uter.</p>
                        <p>Exista dovezi ca o igiena orala corespunzatoare, bazată pe cateva reguli simple, poate avea implicații majore asupra sanatatii oamenilor si a copiilor lor, ajutand la scaderea incidentei complicatiilor necunoscute din timpul sarcinii și a copiilor nou nascuti.</p>
                        <p>La Universitatea din Londra s-a realizat o cercetare în timpul careia s-a făcut analiza conținutului stomacal a 57 de nou nascuti, in mostrele recoltate descoperindu-se 46 de specii diferite de bacterii. Cele mai multe bacterii găsite în mostre provin din vagin, dar 2 dintre aceste specii au fost recunoscute a proveni din gura omului și nu se găsesc în mod obișnuit în alta parte a corpului. Aceste doua tipuri de bacterii, Granulicatella elegans și Streptococcus sinensis, sunt cunoscute a fi capabile de a se infiltra în circuitul sangvin și au fost asociate în trecut cu infectii departe de gura precum endocardita infectioasa.</p>
                        <p>Pentru a menține o igiena orala corespunzatoare în timpul sarcinii este indicat sa:</p>
                        <ul>
                            <li>Mergeți la dentist pentru controalele de rutina și pentru a va curata dantura. Aceasta este una dintre cele mai bune metode pentru o igiena orala de calitate.</li>
                            <li>Spalati-va pe dinți de cel puțin doua ori pe zi pentru a elimina placa dentara.</li>
                            <li>Folosiți ata dentara în fiecare zi. Aceasta va elimina resturile de mancare între dinți unde periuta n-ajunge.</li>
                            <li>Folosiți o apa de gura antimicrobiana pentru ca acestea ajuta la prevenirea gingivitei.</li>
                            <li>Periati și limba zilnic pentru a mai elimina o parte din bacterii.</li>
                            <li>Și, nu în cele din urma, mancati alimente hranitoare si gustări sănătoase.</li>
                        </ul>
                        <p>În concluzie, după cum reiese și din aceste paragrafe, o igienă orală corespunzătoare se realizează prin urmarea unor reguli simple, care odată cunoscute și aplicate împiedica apariția afecțiunilor buco-dentare, indiferent de vârsta și de etapele prin care trecem. Și, bineînțeles, nu uitați: controlul stomatologic periodic este de asemenea esențial în prevenirea sau tratarea afecțiunilor orale.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>