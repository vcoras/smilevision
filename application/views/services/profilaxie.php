<section class="section-1">
    <div class="container">
        <h1 data-aos="fade-up" data-aos-anchor-placement="top-bottom">Servicii</h1>
    </div>
</section>
<?php $this->load->view('services/menu.php'); ?>
<section class="section-2">
    <div class="container">
        <div class="row no-gutter align-items-end">
            <div class="col-md-5">
                <h2 data-aos="fade-left">Profilaxie</h2>
                <p data-aos="fade-left">Cu toate că dinții sunt unele dintre cele mai puternice componente ale organismului, ei pot fi atacați și distruși de bacterii. </p>
                <p data-aos="fade-left">Profilaxia dentară este modalitatea de prevenire a îmbolnăvirii dentare și a complicațiilor, în cazul în care afecțiunile există deja.</p>
            </div>
            <div class="col-md-7">
                <img data-aos="fade-right" src="<?php echo media_url('servicii/profilaxie/0.jpg') ?>" alt="profilaxie">
            </div>
        </div>
        <div class="row no-gutter additional-info">
            <div class="col-md-12">
                <p data-aos="fade-left">Majoritatea problemelor dinților pot fi prevenite, iar prin prevenție poți evita multe complicații stomatologice. Sănătatea ta primează, așa că cea mai importantă măsura luată în acest sens este realizarea unei igienizări riguroase.</p>
                <p data-aos="fade-left">Respectând vizitele regulate la stomatolog reduceți semnificativ costurile alocate recăpătării sănătății dinților.</p>
            </div>
        </div>
    </div>
</section>
<div class="section-3">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">Procedurile de profilaxie utilizate în cabinetul nostru</h3>
        </div>
        <div class="row no-gutter gallery">
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="0">
                <img src="<?php echo media_url('servicii/profilaxie/1.jpg') ?>" alt="profilaxie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="50">
                <img src="<?php echo media_url('servicii/profilaxie/2.jpg') ?>" alt="profilaxie">
            </div>
            <div class="col-md-4 justify-content-center align-self-center" data-aos="fade-down" data-aos-delay="100">
                <img src="<?php echo media_url('servicii/profilaxie/3.jpg') ?>" alt="profilaxie">
            </div>
        </div>
        <div class="row content">
            <p data-aos="fade-down">Profilaxia este un proces sistematic, pe parcursul căruia vom urmări și identifica starea sănătății orale. Prin această procedură pot fi depistate problemele și dezvoltarea unui tratament personalizat de igienizare și profilaxie.</p>
            <p data-aos="fade-down"><strong>Procedurile pe care le efectuăm sunt:</strong></p>
            <ul>
                <li data-aos="fade-left" data-aos-delay="0">Identificarea și îndepărtarea plăcii bacteriene</li>
                <li data-aos="fade-left" data-aos-delay="50">Fluorizări</li>
                <li data-aos="fade-left" data-aos-delay="100">Sigilarea șanțurilor și fosetelor</li>
                <li data-aos="fade-left" data-aos-delay="100">Detartraj și periaj profesional</li>
                <li data-aos="fade-left" data-aos-delay="100">Control periodic</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <h3 data-aos="fade-left">De ce la noi?</h3>
        </div>
        <div class="row no-gutter">
            <div class="col-md-9">
                <p data-aos="fade-down">Considerăm că, pentru sănătatea orală a fiecărui pacient, este necesară acordarea unei atenții deosebite comunicării dintre pacient și echipa medicală. De aceea, echipa noastră nu se rezuma la tratarea afecțiunilor deja existente, ci este pregătită să acorde sfaturile necesare pentru menținerea sănătății orale.</p>
                <p data-aos="fade-down">La finalul ședințelor de igienizare (procedura principală în profilaxie), tratăm dinții cu pastă care remineralizează structura dentară, lucru care va avea efecte pozitive vizibile în timp asupra danturii.</p>
                <p data-aos="fade-down">Prin intermediul programului de recall, nu trebuie să țineți evidența ultimei vizite la stomatolog, fiindcă, datorită acestui program, veți fi anunțat când au trecut 6 luni de la data ultimei vizite la noi. Considerăm acest program foarte util, atât pentru dumneavoastră cât și pentru noi. Pentru dumneavoastră, deoarece vă scutește de încă o grijă pe care trebuie să o purtați, iar pe noi ne ajută în asumarea răspunderii referitoare la sănătatea orală a fiecărui pacient în parte.</p>
            </div>
            <div class="col-md-3 doctors-container" data-aos="zoom-out">
                <h4>Cunoaște-ți medicul</h4>
                <div class="doctors-list">
                    <div>
                        <img src="<?php echo media_url('servicii/medici/alexandra_majorosi.jpg'); ?>" alt="Alexandra Majorosi">
                        <p>Alexandra Majorosi</p>
                    </div>
                    <div>
                        <img src="<?php echo media_url('servicii/medici/carina.jpg'); ?>" alt="Carina">
                        <p>Carina??</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container" data-aos="flip-up">
        <div class="row no-gutter">
            <div class="col-md-9">Pentru mai multe detalii și sfaturi legate de modalitățile în care este recomandat să-ți îngrijești dantura înaintea, în timpul sau în urma fiecărei proceduri stomatologice, te rugăm să accesezi secțiunea indicații utile pentru pacienți.</div>
            <div class="col-md-3">
                <a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Accesează</a>
            </div>
        </div>
    </div>
</div>