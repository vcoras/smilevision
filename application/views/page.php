<?php
    $update = uniqid();
    $development = true;
    $slickPages = ['home', 'services'];
?>
<!DOCTYPE html>
<html lang="ro">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=yes">
        <meta name="theme-color" content="#096b88">
        <base href="<?php echo site_url(); ?>">
        <title><?php echo !empty($page_title) ? $page_title : 'Clinică Stomatologică Timișoara | Smile Vision'; ?></title>
        <?php if (!empty($meta_data)): ?>
            <?php foreach ($meta_data as $name => $content): ?>
                <meta name="<?php echo $name; ?>" content="<?php echo character_limiter(strip_tags($content), 160); ?>">
            <?php endforeach; ?>
        <?php endif; ?>
        <link rel="canonical" href="<?php echo current_url(); ?>">
		<link 
			rel="stylesheet" 
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
			integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
			crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Raleway:300,600,700&display=swap&subset=latin-ext" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo css_url(($development ? 'global.css?' : 'global.min.css?') . $update); ?>">
		<link rel="shortcut icon" href="<?php echo images_url('favicon.ico'); ?>">
		<?php if (in_array($this->uri->rsegment(1), $slickPages)): ?>
			<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
		<?php endif; ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168418398-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-168418398-1');
        </script>

    </head>
    <body class="<?php echo $this->language['url_key']; ?> <?php echo $this->uri->rsegment(1); ?> <?php echo safe_string($this->uri->rsegment(1) . '-' . $this->uri->rsegment(2)); ?> <?php echo !empty($this->uri->rsegment(3)) ? $this->uri->rsegment(3) : ''; ?> <?php echo !empty($body_class) ? $body_class : ''; ?>">
		<div class="header-wrapper">
			<div class="banner">
				<div class="container-960 container d-flex justify-content-end">
					<a class="phone" href="tel:0724005832">0724 005 832</a>
					<p>Luni - Vineri&nbsp;&nbsp;9 - 20</p>
					<a class="contact d-none d-sm-inline-block js-open-form-modal" href="<?php echo base_url($this->language['url_key'] . '/contact'); ?>">Vreau o programare</a>
				</div>
			</div>
			<nav class="header container container-960">
				<div class="language-switcher d-none d-lg-block">
					<?php $this->load->view('language'); ?>
				</div>
				<div class="row justify-content-between">
					<div class="col-md-2 pl-xs-0 logo">
						<div>
							<a href="<?php echo base_url($this->language['url_key']); ?>">
								<img src="<?php echo media_url('logo-smile-vision.svg'); ?>" alt="Logo Smile Vision" />
							</a>
						</div>
					</div>
					<div class="d-lg-none burger-menu">
						<div class="js-open-menu">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
					<div class="col-md-10 d-none d-lg-block">
						<ul class="nav d-flex justify-content-end">
							<li class="list-inline-item <?php echo($this->uri->rsegment(1) == 'home' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key']); ?>">Acasă</a>
								<span class="spot">&nbsp;</span>
							</li>
							<li class="list-inline-item <?php echo($this->uri->rsegment(3) == 'clinica' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key'] . '/clinica'); ?>">Clinica</a>
								<span class="spot">&nbsp;</span>
							</li>
							<li class="list-inline-item has-submenu <?php echo($this->uri->rsegment(1) == 'services' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key'] . '/servicii'); ?>">Servicii</a>
								<span class="spot">&nbsp;</span>
								<ul class="submenu">
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/chirurgie'); ?>">Chirurgie</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/fatete-dentare'); ?>">Fațete dentare</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/endodontie'); ?>">Endodonție</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/estetica-dentara'); ?>">Estetică dentară</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/implantologie'); ?>">Implantologie</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/odontologie'); ?>">Odontologie</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/ortodontie'); ?>">Ortodonție</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/parodontologie'); ?>">Parodontologie</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/pedodontie'); ?>">Pedodonție</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/profilaxie'); ?>">Profilaxie</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/protetica'); ?>">Protetică</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/radiologie'); ?>">Radiologie</a></li>
									<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Indicații</a></li>
								</ul>
							</li>
							<li class="list-inline-item <?php echo($this->uri->rsegment(3) == 'preturi' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key'] . '/preturi'); ?>">Prețuri</a>
								<span class="spot">&nbsp;</span>
							</li>
							<li class="list-inline-item <?php echo($this->uri->rsegment(3) == 'cazuri' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key'] . '/cazuri'); ?>">Cazuri</a>
								<span class="spot">&nbsp;</span>
							</li>
							<!-- <li class="list-inline-item <?php echo($this->uri->rsegment(3) == 'cursuri' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key'] . '/cursuri'); ?>">Cursuri</a>
								<span class="spot">&nbsp;</span>
							</li> -->
							<li class="list-inline-item <?php echo($this->uri->rsegment(1) == 'contact' ? 'current' : ''); ?>">
								<a href="<?php echo base_url($this->language['url_key'] . '/contact'); ?>">Contact</a>
								<span class="spot">&nbsp;</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="nav-mobile">
					<ul>
						<li class="text-center d-block <?php echo($this->uri->rsegment(1) == 'home' ? 'current' : ''); ?>">
							<a href="<?php echo base_url($this->language['url_key']); ?>">Acasă</a>
						</li>
						<li class="text-center d-block <?php echo($this->uri->rsegment(3) == 'clinica' ? 'current' : ''); ?>">
							<a href="<?php echo base_url($this->language['url_key'] . '/clinica'); ?>">Clinica</a>
						</li>
						<li class="text-center d-block <?php echo($this->uri->rsegment(3) == 'servicii' ? 'current' : ''); ?>">
							<a class="js-open-submenu" href="<?php echo base_url($this->language['url_key'] . '/servicii'); ?>">Servicii <i class="fas fa-chevron-down">&nbsp;</i></a>
							<ul class="submenu">
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/chirurgie'); ?>">Chirurgie</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/fatete-dentare'); ?>">Fațete dentare</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/endodontie'); ?>">Endodonție</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/estetica-dentara'); ?>">Estetică dentară</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/implantologie'); ?>">Implantologie</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/odontologie'); ?>">Odontologie</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/ortodontie'); ?>">Ortodonție</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/parodontologie'); ?>">Parodontologie</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/pedodontie'); ?>">Pedodonție</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/profilaxie'); ?>">Profilaxie</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/protetica'); ?>">Protetică</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/radiologie'); ?>">Radiologie</a></li>
								<li><a href="<?php echo base_url($this->language['url_key'] . '/servicii/indicatii'); ?>">Indicații</a></li>
							</ul>
						</li>
						<li class="text-center d-block <?php echo($this->uri->rsegment(3) == 'preturi' ? 'current' : ''); ?>">
							<a href="<?php echo base_url($this->language['url_key'] . '/preturi'); ?>">Prețuri</a>
						</li>
						<li class="text-center d-block <?php echo($this->uri->rsegment(3) == 'cazuri' ? 'current' : ''); ?>">
							<a href="<?php echo base_url($this->language['url_key'] . '/cazuri'); ?>">Cazuri</a>
						</li>
						<!-- <li class="text-center d-block <?php echo($this->uri->rsegment(3) == 'cursuri' ? 'current' : ''); ?>">
							<a href="<?php echo base_url($this->language['url_key'] . '/cursuri'); ?>">Cursuri</a>
						</li> -->
						<li class="text-center d-block <?php echo($this->uri->rsegment(1) == 'contact' ? 'current' : ''); ?>">
							<a href="<?php echo base_url($this->language['url_key'] . '/contact'); ?>">Contact</a>
						</li>
					</ul>
					<?php $this->load->view('language', ['mobile' => true]); ?>
				</div>
			</nav>
			<div class="for-partners">
				<div class="partner-wrapper">
					<p data-aos-delay="100" data-aos="fade-left">Pentru parteneri</p>
					<a data-aos-delay="200" data-aos="fade-left" href="<?php echo base_url($this->language['url_key'] . "/stomatologie-digitala-timisoara"); ?>">Stomatologie digitală</a>
					<a data-aos-delay="300" data-aos="fade-left" href="<?php echo base_url($this->language['url_key'] . "/laborator-tehnica-dentara-timisoara"); ?>">Laborator</a>
					<a data-aos-delay="400" data-aos="fade-left" href="<?php echo base_url($this->language['url_key'] . "/radiologie-dentara-digitala"); ?>">Radiologie</a>
				</div>
			</div>
		</div>
        <div  class="main">
            <?php echo !empty($main) ? $main : ''; ?>
        </div>
		<div class="upper-footer">
			<div class="container container-960">
				<div class="row">
					<div class="col-md-6 offset-md-6 col-xs-12">
						<div class="row no-gutter">
							<div class="col-md-4">
								<div class="title">
									<h3>Adresa</h3>
								</div>
								<div>
									<p>Str. Gh. Lazăr nr. 9</p>
									<p>Corp H, Etaj 2</p>
									<p>Timișoara</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="title">
									<h3>Orar</h3>
								</div>
								<div>
									<p>Luni - Vineri</p>
									<p>9 - 20</p>
								</div>
							</div>
							<div class="col-md-4 icons">
								<div class="row justify-content-md-end justify-content-center">
									<div class="col-md-5 col-6 facebook">
										<a target="_blank" rel=”noopener” href="https://www.facebook.com/DrZaharia/"><i class="fab fa-facebook-f"></i></a>
									</div>
									<div class="col-md-4 col-6 instagram">
										<a target="_blank" rel=”noopener” href="https://www.instagram.com/smilevision_romania/"><i class="fab fa-instagram"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lower-footer">
			<div class="container container-960">
				<div class="row">
					<div class="col-md-10">
						<p class="align-middle">&copy; Copyright 2019 Smile Vision. <span>Toate drepturile rezervate.</span></p>
					</div>
					<div class="col-md-2" style="text-align:right;">
						<a href="<?php echo base_url($this->language['url_key'] . '/gdpr'); ?>" class="gdpr">GDPR</a>
					</div>
				</div>
			</div>
		</div>
		<div class="contact-form-ajax modal">
			<div class="form-wrapper">
				<span class="js-close-form-modal">
					<img src="<?php echo images_url("x.svg"); ?>" alt="Inchidere fereastra" />
				</span>
				<div class="row">
					<div class="col-md-6">
						<h2>Salut!</h2>
						<p>Te felicit că alegi să faci primul pas pentru sănătatea danturii tale.</p>
						<p>Contactează-ne telefonic sau completează formularul alăturat și trimite-l, iar noi vom reveni la tine în maxim 48 de ore pentru a programa vizita la clinică.</p>
						<div class="phone">
							<h3 class="col-md-12">Telefon programări</h3>
							<p class="col-md-12"><a href="tel:0724005832">0724 005 832</a></p>
						</div>
					</div>
					<div class="col-md-6">
						<form action="<?php echo current_url(); ?>" method="post">
							<div class="form-group">
								<label for="full_name">Nume</label>
								<input type="text" class="form-control" id="full_name_modal" name="full_name">
							</div>
							<div class="form-group">
								<label for="phone">Telefon</label>
								<input type="text" class="form-control" id="phone_modal" name="phone">
							</div>

							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" class="form-control" id="email_modal" name="email">
							</div>

							<div class="form-group">
								<label for="message">Mesaj</label>
								<textarea class="form-control" id="message_modal" name="message"></textarea>
							</div>

							<div class="form-group">
								<button type="submit" id="submit" name="submit" value="Trimite">Trimite</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
		<?php if ($development): ?>
			<script type="text/javascript" src="<?php echo js_url('jquery-3.4.1.min.js?' . $update); ?>"></script>
			<script type="text/javascript" src="<?php echo js_url('functions.js?' . $update); ?>"></script>
			<script type="text/javascript" src="<?php echo js_url('global.js?' . $update); ?>"></script>
			<script type="text/javascript" src="<?php echo js_url('jquery.lazyload.min.js'); ?>"></script>
		<?php else: ?>
			<script src="<?php echo js_url('global.min.js'); ?>"></script>
		<?php endif; ?>
		
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
			<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

			<?php if (in_array($this->uri->rsegment(1), $slickPages)): ?>
			<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
		<?php endif; ?>

		<?php if ($this->uri->rsegment(1) == 'contact'): ?>
			<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8-imnNbNw08Km-dTtOACn3wGW_32KtsI&callback=initMap"
			type="text/javascript"></script>
		<?php endif; ?>
		<script defer src="https://kit.fontawesome.com/fd97b5d954.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
		<script>
			window.addEventListener("load", function(){
			window.cookieconsent.initialise({
				"palette": {
				"popup": {
					"background": "#fff",
					"text": "#4B4B4B"
				},
				"button": {
					"background": "#3CA0AA",
					"text": "#FFF"
				}
			},
			"theme": "edgeless",
			"position": "bottom-right",
			"content": {
				"message": "Website-ul nostru folosește cookie-uri pentru a se asigrura că aveți parte de cea mai bună experiență.",
				"dismiss": "Sunt de acord",
				"link": "Află mai multe",
				"href": "<?php echo base_url($this->language['url_key'] . '/gdpr'); ?>"
			}
		})});
		</script>

    </body>
</html>
