<?php if (isset($mobile) && $mobile === TRUE): ?>
    <ul class="language-list-mobile row">
        <?php if ($this->languages): ?>
            <?php foreach($this->languages as $language): ?>
                <?php if ($language['active'] == "1"): ?>
                    <li class="text-center col-md-6 list-inline-item <?php echo ($this->language['id_language'] == $language['id_language'] ? "current" : ""); ?>">
                        <a href="<?php echo base_url($language['url_key']); ?>">
                            <?php echo $language['title']; ?>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
<?php else: ?>
    <ul class="language-list">
        <?php if ($this->languages): ?>
            <?php foreach($this->languages as $language): ?>
                <?php if ($language['active'] == "1"): ?>
                    <li class="<?php echo ($this->language['id_language'] == $language['id_language'] ? "current" : ""); ?>">
                        <a href="<?php echo base_url($language['url_key']); ?>">
                            <?php echo $language['alias']; ?>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
<?php endif; ?>
