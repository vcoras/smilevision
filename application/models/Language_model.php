<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language_model extends CI_Model {

    public function get_languages($active = NULL)
    {
        $this->db->from('language');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row)
            {
                $result[$row['url_key']] = $row;
            }
            return $result;
        }
    }

    public function get_current_language($languages = array(), $url_key = 'ro')
    {
        $default_language = array();
        $current_language = array();

        if ($languages)
        {
            foreach ($languages as $language)
            {
                if ($language['default'])
                {
                    $default_language = $language;
                }

                if ($language['url_key'] == $url_key)
                {
                   $current_language = $language;
                }
            }
        }
        return $current_language ? $current_language : $default_language;
    }

}

/* End of file Language_model.php */
/* Location: ./application/models/Language_model.php */