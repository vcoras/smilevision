<?php

function safe_string($string)
{
    return trim(preg_replace(array("/ă/", "/â/", "/î/", "/ș/", "/ş/", "/ț/", "/ţ/", "/_/", "/\s+/", "/[^A-Za-z0-9\-]/", "/\-+/"), array("a", "a", "i", "s", "s", "t", "t", "-", "-", "", "-"), mb_strtolower($string)), '-');
}

function get_video_html($url, $autoplay = 0)
{
    if (preg_match('![?&]{1}v=([^&]+)!', $url . '&', $video))
    {
        return '<div class="video"><iframe width="640" height="360" src="https://www.youtube.com/embed/' . $video[1] . '?showinfo=0&autoplay=' . $autoplay . '" frameborder="0" allowfullscreen></iframe></div>';
    }
}

function concatenate_date($date1, $date2, $dateFormat = "d.m.Y")
{
    $date1 = strtotime($date1);
    $date2 = strtotime($date2);

    // Same month, same year
    if ((date("m", $date1) == date("m", $date2)) && (date("Y", $date1) == date("Y", $date2)))
    {
        return (date("d", $date1) . " - " . date($dateFormat, $date2));
    }
    elseif ((date("Y", $date1) == date("Y", $date2)))
    {
        return (date("d.m", $date1) . " - " . date("d.m", $date2) . " (" . date("Y", $date2) . ")");
    }

    return (date($dateFormat, $date1) . " - " . date($dateFormat, $date2));
}
